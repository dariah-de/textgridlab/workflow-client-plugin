<xsl:transform version="1.0"
xmlns="http://www.gridworkflow.org/gworkflowdl"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:oc="http://www.gridworkflow.org/gworkflowdl/operationclass" >

<!--
    input: TextGrid Workflow <tgwf> Language Document
    output: GridWorkflowDL for the Fraunhofer GWES
-->

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<!--****** Main ******-->
<xsl:template match="/">
<workflow xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.gridworkflow.org/gworkflowdl" xsi:schemaLocation="http://www.gridworkflow.org/gworkflowdl http://www.gridworkflow.org/kwfgrid/src/xsd/gworkflowdl_1_1.xsd" ID="No_ID">

<xsl:call-template name="headers"/>
<xsl:call-template name="batchinput"/>
<xsl:call-template name="inputconstants"/>
<xsl:call-template name="defaultplaces"/>
<xsl:call-template name="datalinks"/>

<xsl:call-template name="CRUDread"/>
<xsl:call-template name="Services"/>
<xsl:call-template name="CRUDcreate"/>

</workflow>
</xsl:template>

<!--***** Functions ********-->
<xsl:template name="headers">
  <description><xsl:value-of select="/tgwf/description"/></description>
  <property name="redistributionOfFailedActivities">false</property>
</xsl:template>

<!-- the documents to be processed -->
<xsl:template name="batchinput">
<place ID="batchinput">
  <xsl:for-each select="/tgwf/batchinput/URI">
    <token>
      <property name="data.group">URInumber<xsl:number count="/tgwf/batchinput/URI"/>
      </property>
      <data>
	<param xsi:type="xsd:anyURI">
	  <xsl:value-of select="."/>
	</param>
      </data>
    </token>
  </xsl:for-each>
</place>  
</xsl:template>

<!-- constant inputs to services -->
<xsl:template name="inputconstants">
  <xsl:for-each select="/tgwf/inputconstants/activity/const">
    <xsl:element name="place">
      <xsl:attribute name="ID">
	<xsl:value-of select="../@serviceID"/>-<xsl:value-of select="./@name"/>
      </xsl:attribute>
      <token>
	<data>
	  <param xsi:type="xsd:string">
	    <xsl:value-of select="."/>
	  </param>
	</data>
      </token>
    </xsl:element>
  </xsl:for-each>
</xsl:template>

<!-- places between services -->
<xsl:template name="datalinks">
  <xsl:for-each select="/tgwf/datalinks/link">
    <xsl:element name="place">
      <xsl:attribute name="ID">
	<!-- intermediate places will be named after the service they serve as input to --> 
	<xsl:value-of select="./@toServiceID"/>-<xsl:value-of select="./@toParam"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:for-each>
</xsl:template>

<!-- places occuring in every Workflow -->
<xsl:template name="defaultplaces">
  <place ID="intermediateTGOMD"/>
  <place ID="resultingTGOMD"/>
</xsl:template>

<!-- CRUD read -->
<xsl:template name="CRUDread">
  <transition ID="CRUDread">

    <description>CRUD read</description>
    <property name="xmlns:tgc">http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService</property>
    <xsl:call-template name="printReadInputs">
      <xsl:with-param name="thisServiceID">crud</xsl:with-param>
    </xsl:call-template>

    <inputPlace placeID="batchinput" edgeExpression="uri"/>

    <xsl:element name="outputPlace">
      <xsl:attribute name="placeID">
	<xsl:value-of select="/tgwf/datalinks/link[@fromServiceID='crud']/@toServiceID"/>-<xsl:value-of select="/tgwf/datalinks/link[@fromServiceID='crud']/@toParam"/>
      </xsl:attribute>
      <xsl:attribute name="edgeExpression">tgc:data</xsl:attribute>
    </xsl:element>

    <outputPlace placeID="intermediateTGOMD" edgeExpression="tgc:tgObjectMetadata"/>

    <operation>
      <oc:operationClass xmlns:oc="http://www.gridworkflow.org/gworkflowdl/operationclass">
	<xsl:element name="oc:wsOperation">
	  <xsl:attribute name="operationName">read</xsl:attribute>
	  <xsl:attribute name="resourceName">
	    <xsl:value-of select="/tgwf/CRUD/@instance"/>?wsdl</xsl:attribute>
	  <xsl:attribute name="selected">true</xsl:attribute>
	</xsl:element>
      </oc:operationClass>
    </operation>

  </transition>
</xsl:template>
<!-- CRUD read -->

<!-- the proper Services of the workflow -->
<xsl:template name="Services">
  <xsl:for-each select="/tgwf/activities/service">
    <xsl:element name="transition">

      <xsl:attribute name="ID">
	<xsl:value-of select="./@name"/>
      </xsl:attribute>

      <description><xsl:value-of select="./@description"/></description>
      <property name="xmlns:tns"><xsl:value-of select="./@targetNamespace"/></property>

      <xsl:call-template name="printReadInputs">
	<xsl:with-param name="thisServiceID" select="./@serviceID"/>
      </xsl:call-template>

      <xsl:call-template name="inputDatalinks">
	<xsl:with-param name="thisServiceID" select="./@serviceID"/>
      </xsl:call-template>

      <xsl:call-template name="outputDatalinks">
	<xsl:with-param name="thisServiceID" select="./@serviceID"/>
      </xsl:call-template>
      
      <operation>
	<oc:operationClass xmlns:oc="http://www.gridworkflow.org/gworkflowdl/operationclass">
	  <xsl:element name="oc:wsOperation">
	    <xsl:attribute name="operationName">
	      <xsl:value-of select="./@operation"/>
	    </xsl:attribute>
	    <xsl:attribute name="resourceName">
	      <xsl:value-of select="./@wsdlLocation"/>
	    </xsl:attribute>
	    <xsl:attribute name="selected">true</xsl:attribute>
	  </xsl:element>
	</oc:operationClass>
      </operation>

    </xsl:element>
  </xsl:for-each>
</xsl:template> 
<!-- Services -->


<!-- CRUD create: write back to the Grid with same (!) metadata -->
<xsl:template name="CRUDcreate">
  <transition ID="CRUDcreate">

    <description>CRUD create</description>
    <property name="xmlns:tgc">http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService</property>

    <xsl:call-template name="printReadInputs">
      <xsl:with-param name="thisServiceID">crud</xsl:with-param>
    </xsl:call-template>

    <inputPlace placeID="crud-batchoutput" edgeExpression="tgc:data"/>
    <inputPlace placeID="intermediateTGOMD" edgeExpression="tgc:tgObjectMetadata"/>

    <outputPlace placeID="resultingTGOMD" edgeExpression="tgc:tgObjectMetadata"/>

    <operation>
      <oc:operationClass xmlns:oc="http://www.gridworkflow.org/gworkflowdl/operationclass">
	<xsl:element name="oc:wsOperation">
	  <xsl:attribute name="operationName">create</xsl:attribute>
	  <xsl:attribute name="resourceName">
	    <xsl:value-of select="/tgwf/CRUD/@instance"/>?wsdl</xsl:attribute>
	  <xsl:attribute name="selected">true</xsl:attribute>
	</xsl:element>
      </oc:operationClass>
    </operation>

  </transition>
</xsl:template>
<!-- CRUD create -->

<!-- variable function: print a read place -->
<xsl:template name="printReadInputs">
  <xsl:param name="thisServiceID"/>
  <xsl:for-each select="/tgwf/inputconstants/activity[@serviceID=$thisServiceID]/const">
    <xsl:element name="readPlace">
      <xsl:attribute name="placeID">
	<xsl:value-of select="../@serviceID"/>-<xsl:value-of select="./@name"/>
      </xsl:attribute>
      <xsl:attribute name="edgeExpression">
	<xsl:value-of select="./@name"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:for-each>
</xsl:template>

<!-- variable function: print an input place -->
<xsl:template name="inputDatalinks">
  <xsl:param name="thisServiceID"/>
  <xsl:for-each select="/tgwf/datalinks/link[@toServiceID=$thisServiceID]">
    <xsl:element name="inputPlace">
      <xsl:attribute name="placeID">
	<xsl:value-of select="./@toServiceID"/>-<xsl:value-of select="./@toParam"/>
      </xsl:attribute>
      <xsl:attribute name="edgeExpression">
	<xsl:value-of select="./@toParam"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:for-each>
</xsl:template>

<!-- variable function: print an output place -->
<xsl:template name="outputDatalinks">
  <xsl:param name="thisServiceID"/>
  <xsl:for-each select="/tgwf/datalinks/link[@fromServiceID=$thisServiceID]">
    <xsl:element name="outputPlace">
      <xsl:attribute name="placeID">
	<xsl:value-of select="./@toServiceID"/>-<xsl:value-of select="./@toParam"/>
      </xsl:attribute>
      <xsl:attribute name="edgeExpression">
	<xsl:value-of select="./@fromParam"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:for-each>
</xsl:template>



</xsl:transform>


