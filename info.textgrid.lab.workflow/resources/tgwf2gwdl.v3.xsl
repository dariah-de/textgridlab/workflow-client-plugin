<xsl:transform version="1.0"
xmlns="http://www.gridworkflow.org/gworkflowdl"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:oc="http://www.gridworkflow.org/gworkflowdl/operationclass"
xmlns:tgwf="http://textgrid.info/namespaces/middleware/workflow" >

<!--
    input: TextGrid Workflow <tgwf> Language Document
    output: GridWorkflowDL for the Fraunhofer GWES
-->

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<!--****** Main ******-->
<xsl:template match="/">
<workflow xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.gridworkflow.org/gworkflowdl" xsi:schemaLocation="http://www.gridworkflow.org/gworkflowdl http://www.gridworkflow.org/kwfgrid/src/xsd/gworkflowdl_1_1.xsd" ID="No_ID">

<xsl:call-template name="headers"/>
<xsl:call-template name="batchinput"/>
<xsl:call-template name="inputconstants"/>
<xsl:call-template name="defaultplaces"/>
<xsl:call-template name="datalinks"/>

<xsl:call-template name="CRUDread"/>
<xsl:call-template name="Services"/>
<xsl:call-template name="StreamingEditor"/>
<xsl:call-template name="CRUDcreate"/>

</workflow>
</xsl:template>

<!--***** Functions ********-->
<xsl:template name="headers">
  <description><xsl:value-of select="/tgwf:tgwf/tgwf:description"/></description>
  <property name="redistributionOfFailedActivities">false</property>
</xsl:template>

<!-- the documents to be processed -->
<xsl:template name="batchinput">
<place ID="batchinput">
  <xsl:for-each select="/tgwf:tgwf/tgwf:batchinput/tgwf:URI">
    <token>
      <property name="data.group">URInumber<xsl:number count="/tgwf:tgwf/tgwf:batchinput/tgwf:URI"/>
      </property>
      <data>
	<param xsi:type="xsd:anyURI">
	  <xsl:value-of select="."/>
	</param>
      </data>
    </token>
  </xsl:for-each>
</place>  
</xsl:template>

<!-- constant inputs to services -->
<xsl:template name="inputconstants">
  <xsl:for-each select="/tgwf:tgwf/tgwf:inputconstants/tgwf:activity/tgwf:const">
    <xsl:element name="place">
      <xsl:attribute name="ID">
	<xsl:value-of select="../@serviceID"/>-<xsl:value-of select="./@name"/>
      </xsl:attribute>
      <token>
	<data>
	  <param xsi:type="xsd:string">
	    <xsl:value-of select="."/>
	  </param>
	</data>
      </token>
    </xsl:element>
  </xsl:for-each>
</xsl:template>

<!-- places between services -->
<xsl:template name="datalinks">
  <xsl:for-each select="/tgwf:tgwf/tgwf:datalinks/tgwf:link">
    <xsl:element name="place">
      <xsl:attribute name="ID">
	<!-- intermediate places will be named after the service they serve as input to --> 
	<xsl:value-of select="./@toServiceID"/>-<xsl:value-of select="./@toParam"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:for-each>
</xsl:template>

<!-- places occuring in every Workflow -->
<xsl:template name="defaultplaces">
  <place ID="crud-sessionId">
    <token>
      <data>
	<param xsi:type="xsd:string"><xsl:value-of select="/tgwf:tgwf/tgwf:CRUD/@sessionID"/></param>
      </data>
    </token>
  </place>
  <place ID="crud-logParameter">
    <token>
      <data>
	<param xsi:type="xsd:string"><xsl:value-of select="/tgwf:tgwf/tgwf:CRUD/@logParameter"/></param>
      </data>
    </token>
  </place>
  <place ID="TGOMDbeforeTransformation"/>
  <place ID="TGOMDafterTransformation"/>
  <place ID="resultingTGOMD"/>
  <place ID="se-sid">
    <token>
      <data>
	<param xsi:type="xsd:string"></param>
      </data>
    </token>
  </place>
  <place ID="se-log">
    <token>
      <data>
	<param xsi:type="xsd:string"></param>
      </data>
    </token>
  </place>
  <place ID="se-params">
    <token>
      <data>
	<param xsi:type="xsd:string"></param>
      </data>
    </token>
  </place>
  <place ID="se-xsl">
    <token>
      <data>
	<xsl:copy-of select="/tgwf:tgwf/tgwf:metadatatransformation"/>
      </data>
    </token>
  </place>
</xsl:template>

<!-- CRUD read -->
<xsl:template name="CRUDread">
  <transition ID="CRUDread">

    <description>CRUD read</description>
    <property name="xmlns:tgc">http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService</property>

    <readPlace placeID="crud-sessionId" edgeExpression="sessionId"/>
    <readPlace placeID="crud-logParameter" edgeExpression="logParameter"/>
    <inputPlace placeID="batchinput" edgeExpression="uri"/>

    <xsl:element name="outputPlace">
      <xsl:attribute name="placeID">
	<xsl:value-of select="/tgwf:tgwf/tgwf:datalinks/tgwf:link[@fromServiceID='crud']/@toServiceID"/>-<xsl:value-of select="/tgwf:tgwf/tgwf:datalinks/tgwf:link[@fromServiceID='crud']/@toParam"/>
      </xsl:attribute>
      <xsl:attribute name="edgeExpression">tgc:data</xsl:attribute>
    </xsl:element>

    <outputPlace placeID="TGOMDbeforeTransformation" edgeExpression="tgc:tgObjectMetadata"/>

    <operation>
      <oc:operationClass xmlns:oc="http://www.gridworkflow.org/gworkflowdl/operationclass">
	<xsl:element name="oc:wsOperation">
	  <xsl:attribute name="operationName">read</xsl:attribute>
	  <xsl:attribute name="resourceName">
	    <xsl:value-of select="/tgwf:tgwf/tgwf:CRUD/@instance"/>?wsdl</xsl:attribute>
	  <xsl:attribute name="selected">true</xsl:attribute>
	</xsl:element>
      </oc:operationClass>
    </operation>

  </transition>
</xsl:template>
<!-- CRUD read -->

<!-- the proper Services of the workflow -->
<xsl:template name="Services">
  <xsl:for-each select="/tgwf:tgwf/tgwf:activities/tgwf:service">
    <xsl:element name="transition">

      <xsl:attribute name="ID">
	<xsl:value-of select="./@name"/>
      </xsl:attribute>

      <description><xsl:value-of select="./@description"/></description>
      <property name="xmlns:tns"><xsl:value-of select="./@targetNamespace"/></property>

      <xsl:call-template name="printReadInputs">
	<xsl:with-param name="thisServiceID" select="./@serviceID"/>
      </xsl:call-template>

      <xsl:call-template name="inputDatalinks">
	<xsl:with-param name="thisServiceID" select="./@serviceID"/>
      </xsl:call-template>

      <xsl:call-template name="outputDatalinks">
	<xsl:with-param name="thisServiceID" select="./@serviceID"/>
      </xsl:call-template>
      
      <operation>
	<oc:operationClass xmlns:oc="http://www.gridworkflow.org/gworkflowdl/operationclass">
	  <xsl:element name="oc:wsOperation">
	    <xsl:attribute name="operationName">
	      <xsl:value-of select="./@operation"/>
	    </xsl:attribute>
	    <xsl:attribute name="resourceName">
	      <xsl:value-of select="./@wsdlLocation"/>
	    </xsl:attribute>
	    <xsl:attribute name="selected">true</xsl:attribute>
	  </xsl:element>
	</oc:operationClass>
      </operation>

    </xsl:element>
  </xsl:for-each>
</xsl:template> 
<!-- Services -->


<!-- StreamingEditor: rules to transform metadata ofr new TextGridObjects -->
<xsl:template name="StreamingEditor">
  <transition ID="StreamingEditor">
    <description>StreamingEditor</description>
    <property name="xmlns:tns">http://sedit.textgrid.de</property>
    <property name="xmlns:tgc">http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService</property>
    <property name="xmlns:xsl">http://www.w3.org/1999/XSL/Transform</property>
    <readPlace placeID="se-sid" edgeExpression="tns:sid"/>
    <readPlace placeID="se-log" edgeExpression="tns:log"/>
    <readPlace placeID="se-params" edgeExpression="tns:params"/>
    <readPlace placeID="se-xsl" edgeExpression="tns:stylesheet"/>
    <inputPlace placeID="TGOMDbeforeTransformation" edgeExpression="tns:input/*"/>
    <outputPlace placeID="TGOMDafterTransformation" edgeExpression="tgc:tgObjectMetadata"/>
    <operation>
      <oc:operationClass>
	<oc:wsOperation operationName="sedit" resourceName="http://ingrid.sub.uni-goettingen.de:8081/axis2/services/StreamingEditorXslt?wsdl" selected="true"/>
      </oc:operationClass>
    </operation>
  </transition>
</xsl:template>


<!-- CRUD create: write back to the Grid with same (!) metadata -->
<xsl:template name="CRUDcreate">
  <transition ID="CRUDcreate">

    <description>CRUD create</description>
    <property name="xmlns:tgc">http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService</property>

    <readPlace placeID="crud-sessionId" edgeExpression="sessionId"/>
    <readPlace placeID="crud-logParameter" edgeExpression="logParameter"/>
    <inputPlace placeID="crud-batchoutput" edgeExpression="tgc:data"/>
    <inputPlace placeID="TGOMDafterTransformation" edgeExpression="tgc:tgObjectMetadata"/>

    <outputPlace placeID="resultingTGOMD" edgeExpression="*"/>

    <operation>
      <oc:operationClass xmlns:oc="http://www.gridworkflow.org/gworkflowdl/operationclass">
	<xsl:element name="oc:wsOperation">
	  <xsl:attribute name="operationName">create</xsl:attribute>
	  <xsl:attribute name="resourceName">
	    <xsl:value-of select="/tgwf:tgwf/tgwf:CRUD/@instance"/>?wsdl</xsl:attribute>
	  <xsl:attribute name="selected">true</xsl:attribute>
	</xsl:element>
      </oc:operationClass>
    </operation>

  </transition>
</xsl:template>
<!-- CRUD create -->

<!-- variable function: print a read place -->
<xsl:template name="printReadInputs">
  <xsl:param name="thisServiceID"/>
  <xsl:for-each select="/tgwf:tgwf/tgwf:inputconstants/tgwf:activity[@serviceID=$thisServiceID]/tgwf:const">
    <xsl:element name="readPlace">
      <xsl:attribute name="placeID">
	<xsl:value-of select="../@serviceID"/>-<xsl:value-of select="./@name"/>
      </xsl:attribute>
      <xsl:attribute name="edgeExpression">
	<xsl:value-of select="./@name"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:for-each>
</xsl:template>

<!-- variable function: print an input place -->
<xsl:template name="inputDatalinks">
  <xsl:param name="thisServiceID"/>
  <xsl:for-each select="/tgwf:tgwf/tgwf:datalinks/tgwf:link[@toServiceID=$thisServiceID]">
    <xsl:element name="inputPlace">
      <xsl:attribute name="placeID">
	<xsl:value-of select="./@toServiceID"/>-<xsl:value-of select="./@toParam"/>
      </xsl:attribute>
      <xsl:attribute name="edgeExpression">
	<xsl:value-of select="./@toParam"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:for-each>
</xsl:template>

<!-- variable function: print an output place -->
<xsl:template name="outputDatalinks">
  <xsl:param name="thisServiceID"/>
  <xsl:for-each select="/tgwf:tgwf/tgwf:datalinks/tgwf:link[@fromServiceID=$thisServiceID]">
    <xsl:element name="outputPlace">
      <xsl:attribute name="placeID">
	<xsl:value-of select="./@toServiceID"/>-<xsl:value-of select="./@toParam"/>
      </xsl:attribute>
      <xsl:attribute name="edgeExpression">
	<xsl:value-of select="./@fromParam"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:for-each>
</xsl:template>



</xsl:transform>


