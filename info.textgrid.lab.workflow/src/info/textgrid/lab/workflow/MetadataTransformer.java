package info.textgrid.lab.workflow;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;

import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class MetadataTransformer {
	private EntryLink output;
	private EntryLink fromInput;
	private TextGridObject stylesheet;
	private HashMap<String, String> params;
	public final static String DEFAULT_STYLESHEET_KEY = "DefaultMetaDataTransformationTextGridURI";
	public static String DEFAULT_STYLESHEET_URI; 
	
	public MetadataTransformer() {
		String defautlMdtURIstring = null;
		try {
			ConfClient confClient = ConfClient.getInstance();
			defautlMdtURIstring = confClient.getValue(DEFAULT_STYLESHEET_KEY);
		} catch (OfflineException e) {
			OnlineStatus
					.netAccessFailed(
							"Workflow: Could not find TextGridObject URI for default metadata transformation Stylesheet!",
							e);
		} finally {
			if (defautlMdtURIstring == null || defautlMdtURIstring.equals("")) {
				defautlMdtURIstring = "textgrid:hk2t";
			}
		}
		setStylesheet(defautlMdtURIstring);
		DEFAULT_STYLESHEET_URI = defautlMdtURIstring;
	}

	/**
	 * Set defaults for XSLT Parameters provided that we use the default
	 * stylesheet and the output is already set. Will be:
	 * 
	 * <pre>
	 * format.exact: <mimetype of output parameter>
	 * title:  <copy of original title> (<output port name>)
	 * </pre>
	 */
	public void setDefaultParams() {
		if (output == null) {
			return;
		}
		if (stylesheet
				.getURI()
				.toString()
				.equals(DEFAULT_STYLESHEET_URI + "."
						+ stylesheet.getRevisionNumber())) {
			if (params.get("format.exact") == null) {
				params.put("format.exact", output.getFromPort().getMimetypes());
			}
			if (params.get("title.exact") == null
					&& params.get("title.prepend") == null
					&& params.get("title.append") == null) {
				params.put("title.append", " ("
						+ output.getFromPort().getName() + ")");
			}
		}
	}

	/**
	 * This reads all toplevel XSL Parameters of the Stylesheet with the given
	 * URI. The names of these params should be chosen to be human-readable, as
	 * they will be displayed in the GUI.
	 * 
	 * @param URIstring
	 */
	public void setStylesheet(String URIstring) {
		params = new HashMap<String, String>();

		try {
			stylesheet = TextGridObject.getInstance(URI.create(URIstring),
					false);
		} catch (CrudServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		OMElement xslOM = readTGO(stylesheet);

		Iterator paramIterator = xslOM.getChildrenWithName(new QName(
				"http://www.w3.org/1999/XSL/Transform", "param"));

		while (paramIterator.hasNext()) {
			OMElement paramOM = (OMElement) paramIterator.next();
			String name = paramOM.getAttributeValue(new QName(null, "name"));
			params.put(name, null);
		}
	}

	public TextGridObject getStylesheet() {
		return stylesheet;
	}

	public HashMap<String, String> getParams() {
		return params;
	}

	public EntryLink getOutput() {
		return output;
	}

	public void setOutput(EntryLink output) {
		this.output = output;
	}

	public EntryLink getFromInput() {
		return fromInput;
	}

	public void setFromInput(EntryLink fromInput) {
		this.fromInput = fromInput;
	}

	/**
	 * Reads the contents of a TGO. Returns an OMElement if that is XML, null
	 * otherwise
	 * 
	 * @param tgo
	 * @return
	 */
	public OMElement readTGO(TextGridObject tgo) {
		OMElement result = null;

		IFile wffile = (IFile) tgo.getAdapter(IFile.class);
		InputStream wfstream = null;
		try {
			wfstream = wffile.getContents();
		} catch (CoreException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Could not read this stylesheet's contents", e);
			Activator.getDefault().getLog().log(status);
		}

		StAXOMBuilder builder = null;
		try {
			builder = new StAXOMBuilder(wfstream);
			result = builder.getDocumentElement();
		} catch (XMLStreamException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Could not read thsi stylesheet's contents", e);
			Activator.getDefault().getLog().log(status);

		}

		return result;
	}

}
