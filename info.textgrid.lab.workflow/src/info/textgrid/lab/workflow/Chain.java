package info.textgrid.lab.workflow;

import info.textgrid.lab.workflow.servicedescription.Input;
import info.textgrid.lab.workflow.servicedescription.Output;
import info.textgrid.lab.workflow.tgwf.Tgwf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Chain {
	private ArrayList<ChainEntry> chain;
	private HashSet<EntryLink> possibleLinks;
	private ChainTGWF tgwf;
	private ArrayList<MetadataTransformer> transformers;
	private String description = "";

	public Chain() {
		possibleLinks = new HashSet<EntryLink>();
		transformers = new ArrayList<MetadataTransformer>();
		setChain(new ArrayList<ChainEntry>());
	}

	public Chain(ArrayList<ChainEntry> chain) {
		possibleLinks = new HashSet<EntryLink>();
		transformers = new ArrayList<MetadataTransformer>();
		this.setChain(chain);
	}

	public void addEntry(ChainEntry ce) {
		chain.add(ce);
		assignIDs();
		connectEntriesMonotone();
		ce.setDefaultParams();
	}

	public ArrayList<ChainEntry> getChain() {
		return chain;
	}

	public void resetLinks() {
		possibleLinks.clear();
		transformers.clear();
	}

	public void clearLinks() {
		for (EntryLink link : possibleLinks) {
			link.resetState();
		}
		for (int i = 0; i < getChain().size(); i++) {
			getChain().get(i).clearLinks();
		}
		transformers.clear();
	}

	public void connectEntriesMonotone() {

		for (int num = 0; num < getChain().size(); num++) {
			ChainEntry entry = getChain().get(num);
			for (Input i : entry.getInputs()) {
				addPossibleLink(null, null, entry, i);
			}
			for (Output o : entry.getOutputs()) {
				addPossibleLink(entry, o, null, null);
			}
			// this connects the new entry's inputs with all possible PRECEDING
			// outputs. Effectively allows more than a chain but also forking
			// and joining, however it's still an ordered list/chain. Ensures
			// DAG properties, btw...
			for (int prev = 0; prev < num; prev++) {
				ChainEntry previousEntry = getChain().get(prev);
				for (Output o : previousEntry.getOutputs()) {
					for (Input i : entry.getInputs()) {
						addPossibleLink(previousEntry, o, entry, i);
					}
				}
			}
		}
	}

	public void addPossibleLink(ChainEntry from, Output fromPort,
			ChainEntry to, Input toPort) {
		possibleLinks.add(new EntryLink(from, fromPort, to, toPort));
	}

	/**
	 * returns success state. assures that an output and an input have only one
	 * connecting link in the end, i.e. if this link goes from port A.O to port
	 * B.I, marks all other links coming from A.O or leading to B.I as
	 * incompatible (for greying out) and this link as desired (for checking
	 * it).
	 * 
	 * @param oneLink
	 * @return
	 */
	public boolean addLink(EntryLink oneLink) {
		HashSet<EntryLink> incompatibleLinks = new HashSet<EntryLink>();
		EntryLink needle = null;

		for (EntryLink l : possibleLinks) {
			if (l.getFrom() != null && l.getFrom().equals(oneLink.getFrom())
					&& l.getFromPort() != null
					&& l.getFromPort().equals(oneLink.getFromPort())) {
//				System.out.println("Trying to add link with same source:");
//				l.dump();
				if (l.equals(oneLink)) {
					if (l.isUndecided()) {
						needle = l;
					} else if (l.isDeleted()) {
						return false;
					}
				} else { // all other links must not be set yet, otherwise
							// mark them as incompatible with the needle
					if (l.isAdded()) {
						return false;
					} else {
						incompatibleLinks.add(l);
//						System.out.println("...is incompatible");
					}
				}
			} else if (l.getTo() != null && l.getTo().equals(oneLink.getTo())
					&& l.getToPort() != null
					&& l.getToPort().equals(oneLink.getToPort())) {
//				System.out.println("Trying to add link with same target:");
//				l.dump();
				if (l.equals(oneLink)) {
					if (l.isUndecided()) {
						needle = l;
					} else if (l.isDeleted()) {
						return false;
					}
				} else { // all other links must not be set yet, otherwise
							// mark them as incompatible with the needle
					if (l.isAdded()) {
						return false;
					} else {
						incompatibleLinks.add(l);
//						System.out.println("...is incompatible");
					}
				}

			}
		}
		if (needle == null) {
//			System.out
//					.println("Strange error, could not find the link which you want to add among the links in the 'possible' category");
//			System.out.println("Something else");
			return false;
		}
		// we arrive here only if the link can be added, so do it now
		needle.markLinkAdded();
//		System.out.println("ADDED LINK:");
//		needle.dump();
		for (EntryLink out : incompatibleLinks) {
			out.markLinkDeleted();
			// System.out.println("DELETED INCOMPATIBLE LINK:");
			// out.dump();
		}
		return true;
	}

	public List<EntryLink> findLinksToHere(ChainEntry to, Input toPort) {
		ArrayList<EntryLink> result = new ArrayList<EntryLink>();
		for (EntryLink l : possibleLinks) {
			if (l.getTo() != null && l.getToPort() != null) {
				if (l.getTo().equals(to) && l.getToPort().equals(toPort)
						&& !l.isDeleted()) {
					result.add(l);
				}
			}
		}
		return result;
	}

	public List<EntryLink> findLinksFromHere(ChainEntry from, Output fromPort) {
		ArrayList<EntryLink> result = new ArrayList<EntryLink>();
		for (EntryLink l : possibleLinks) {
			if (l.getFrom() != null && l.getFromPort() != null) {
				if (l.getFrom().equals(from)
						&& l.getFromPort().equals(fromPort) && !l.isDeleted()) {
					result.add(l);
				}
			}
		}
		return result;
	}

	public boolean connectOutputs() {
//		System.out.println("Connecting outputs...");
		for (EntryLink l : possibleLinks) {
			if (l.isUndecided() && l.getTo() == null && l.getToPort() == null
			// better link optional ports just in case user wants their data
			// && (l.getFromPort().isOptional() == null || !l
			// .getFromPort().isOptional())
			) {
//				System.out.println("Connecting (adding) an output link...");
//				l.dump();
				addLink(l);
			}
		}
		return true;
	}

	public boolean validateLinks() {
		int linkID = 0;
		// at once, add links to the chain entries (the ID will bring the two
		// halfes together again), and return false whenever possibleLinks still
		// contains too many such that one port still has more than one link
		for (EntryLink l : possibleLinks) {
			if (!l.isAdded()) {
				continue;
			}
			linkID++;
			if (l.getFrom() != null && l.getFromPort() != null) {
				if (!l.getFrom().addOutgoingLink(l, String.valueOf(linkID))) {
//					System.out.println("Link already taken: Service number "
//							+ l.getFrom().getID() + " Output Port "
//							+ l.getFromPort().getName());
					return false;
				}
			}
			if (l.getTo() != null && l.getToPort() != null) {
				if (!l.getTo().addIncomingLink(l, String.valueOf(linkID))) {
//					System.out.println("Link already taken: Service number "
//							+ l.getTo().getID() + " Input Port "
//							+ l.getToPort().getName());
					return false;
				}
			}
		}

		// assure that all non-optional ports have their link
		for (ChainEntry c : getChain()) {
			for (Input i : c.getInputs()) {
				if (i.isOptional() != null && !i.isOptional()
						&& !c.existsIncomingLink(i)) {
//					System.out.println("Sorry, Service " + c.getName()
//							+ ", input port " + i.getName()
//							+ " has no Link attached yet");
					return false;
				}
			}
			// TODO do we need this?
			for (Output o : c.getOutputs()) {
				if (o.isOptional() != null && !o.isOptional() == true
						&& !c.existsOutgoingLink(o)) {
//					System.out.println("Sorry, Service " + c.getName()
//							+ ", output port " + o.getName()
//							+ " has no Link attached yet");
					return false;
				}
			}
		}
//System.out.println("Chain links are valid");
		return true;
	}

	public void dumpLinks() {
		for (EntryLink l : possibleLinks) {
			l.dump();
		}
	}

	public void assignIDs() {
		for (int i = 0; i < getChain().size(); i++) {
			getChain().get(i).setID(i);
		}
	}

	public HashSet<EntryLink> getLinks() {
		return possibleLinks;
	}

	public ChainTGWF toTGWF() {
//System.out.println("Constructing ...");
		tgwf = new ChainTGWF(this);
		if (description.length() > 0) {
			tgwf.setDescription(description);
		}
		for (MetadataTransformer m : transformers) {
			tgwf.insertMetadataTransformer(m.getOutput(), m.getFromInput(), m
					.getStylesheet().getURI(), m.getParams());
		}
		return tgwf;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String toString() {
		return toTGWF().marshal();
	}

	public void setChain(ArrayList<ChainEntry> chain) {
		this.chain = chain;
	}

	public ArrayList<MetadataTransformer> getTransformers() {
		return transformers;
	}

	public ArrayList<EntryLink> getInputLinks() {
		ArrayList<EntryLink> result = new ArrayList<EntryLink>();
		for (EntryLink l : possibleLinks) {
			if (l.isUndecided() || l.isDeleted()) {
				continue;
			}
			if (l.getFrom() == null && l.getFromPort() == null) {
				result.add(l);
			}
		}
		return result;
	}

}
