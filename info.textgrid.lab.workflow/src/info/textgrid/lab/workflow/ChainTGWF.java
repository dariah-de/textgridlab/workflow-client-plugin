package info.textgrid.lab.workflow;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.log.logsession;
import info.textgrid.lab.workflow.servicedescription.Configparameter;
import info.textgrid.lab.workflow.servicedescription.Input;
import info.textgrid.lab.workflow.tgwf.Activities;
import info.textgrid.lab.workflow.tgwf.Activity;
import info.textgrid.lab.workflow.tgwf.CRUD;
import info.textgrid.lab.workflow.tgwf.Configparameters;
import info.textgrid.lab.workflow.tgwf.Datalinks;
import info.textgrid.lab.workflow.tgwf.Inputs;
import info.textgrid.lab.workflow.tgwf.Link;
import info.textgrid.lab.workflow.tgwf.Metadatatransformation;
import info.textgrid.lab.workflow.tgwf.ObjectFactory;
import info.textgrid.lab.workflow.tgwf.Output;
import info.textgrid.lab.workflow.tgwf.Parameter;
import info.textgrid.lab.workflow.tgwf.Service;
import info.textgrid.lab.workflow.tgwf.Tgwf;
import info.textgrid.lab.workflow.tgwf.Xslparam;
import info.textgrid.middleware.confclient.ConfservClientConstants;

import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.xml.bind.JAXB;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;

public class ChainTGWF {

	private Tgwf t;
	private ObjectFactory tgwfFactory = new ObjectFactory();
	private Chain c;

	public ChainTGWF(Tgwf t) {
		this.t = t;
		Service[] sArr = t.getActivities().getService().toArray(new Service[0]);
		Arrays.sort(sArr, new Comparator<Service>() {
			public int compare(Service s1, Service s2) {
				return s1.getId().compareTo(s2.getId());
			}
		});
		ArrayList<ChainEntry> ces = new ArrayList<ChainEntry>();
		for (Service s : sArr) {
			try {
				TextGridObject sTGO = TextGridObject.getInstance(
						URI.create(s.getURI()), false);
				ChainEntry ce = new ChainEntry(sTGO);
				ce.setID(s.getId().intValue());
				ces.add(ce);
			} catch (CrudServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		c = new Chain(ces);

		// maintain two parallel lists in order to refer back later
		ArrayList<Link> tmpLinks = new ArrayList<Link>();
		ArrayList<EntryLink> tmpChainLinks = new ArrayList<EntryLink>();
		for (Link l : t.getDatalinks().getLink()) {
			tmpLinks.add(l);
			info.textgrid.lab.workflow.servicedescription.Output fromPort = null;
			Input toPort = null;
			ChainEntry from = null;
			ChainEntry to = null;

			if (l.getFromServiceID().equals("null")) {
				from = null;
				fromPort = null;
			} else {
				int lInt = Integer.parseInt(l.getFromServiceID());
				for (ChainEntry ce : ces) {
					if (ce.getID() == lInt) {
						from = ce;
						for (info.textgrid.lab.workflow.servicedescription.Output o : ce
								.getOutputs()) {
							if (o.getParam().equals(l.getFromParam())) {
								fromPort = o;
								break;
							}
						}
						break;
					}
				}
			}

			if (l.getToServiceID().equals("null")) {
				to = null;
				toPort = null;
			} else {
				int lInt = Integer.parseInt(l.getToServiceID());
				for (ChainEntry ce : ces) {
					if (ce.getID() == lInt) {
						to = ce;
						for (Input i : ce.getInputs()) {
							if (i.getParam().equals(l.getToParam())) {
								toPort = i;
								break;
							}
						}
						break;
					}
				}
			}

			EntryLink el = new EntryLink(from, fromPort, to, toPort);
			el.markLinkAdded();
			c.getLinks().add(el); // addLink(el) would be wrong as el is not
									// among the possible links there yet
			tmpChainLinks.add(el);
		}

		if (t.getMetadatatransformation() != null
				&& t.getMetadatatransformation().getOutput() != null) {
			for (Output mdto : t.getMetadatatransformation().getOutput()) {
				MetadataTransformer m = new MetadataTransformer();
				for (int i = 0; i < tmpLinks.size(); i++) {
					Link l = tmpLinks.get(i);
					if (mdto.getOutputLinkId().equals(l.getId())) {
						m.setOutput(tmpChainLinks.get(i));
					}
					if (mdto.getFromInputLinkId().equals(l.getId())) {
						m.setFromInput(tmpChainLinks.get(i));
					}
				}
				m.setStylesheet(mdto.getStylesheetURI());
				for (Xslparam x : mdto.getXslparam()) {
					m.getParams().put(x.getName(), x.getValue());
				}
				c.getTransformers().add(m);
			}
		} else {
			Activator
					.handleProblem(
							IStatus.WARNING,
							null,
							"Either no output (which would be ok), or forgot to specify metadata transformation rules for this workflow.",
							"x");
		}

		for (Activity a : t.getConfigparameters().getActivity()) {
			for (ChainEntry ce : c.getChain()) {
				if (ce.getID() == a.getServiceId().intValue()) {
					for (Parameter p : a.getParameter()) {
						for (Configparameter cp : ce.getConfigParameters()) {
							if (cp.getParam().equals(p.getParam())) {
								if (p.getParamId().equals(
										ChainEntry.NEW_FROM_STRING_ID)
										|| p.getParamId().equals(
												ChainEntry.NEW_FROM_URI_ID)) {
									// non-default case: user had specified in
									// workflow a new string or URI value
									ce.appendNewConfigValue(
											p.getParam(),
											p.getParamId()
													.equals(ChainEntry.NEW_FROM_STRING_ID) ? true
													: false,
											ChainEntry.anyToString(p
													.getContent()));
								} else {
									// the default case: workflow had specified
									// one of the example parameters
									ce.setConfigParam(p.getParam(),
											p.getParamId());
								}
							}
						}
					}
					break;
				}
			}
		}
		c.setDescription(t.getDescription());
	}

	public Chain getChain() {
		return c;
	}

	public void setDescription(String desc) {
		t.setDescription(desc);
	}

	public ChainTGWF(Chain c) {
		this.c = c;

		t = tgwfFactory.createTgwf();

		StringBuffer automaticDescription = new StringBuffer();

		// enter services
		Activities activities = tgwfFactory.createActivities();
		Configparameters configParameters = tgwfFactory
				.createConfigparameters();

		for (ChainEntry ce : c.getChain()) {
			automaticDescription.append(ce.getName()).append(" and ");

			Service s = tgwfFactory.createService();
			s.setId(BigInteger.valueOf(ce.getID()));
			s.setURI(ce.getTGOURI().toString()); // TODO check whether
													// versioned
													// or newest URI
			activities.getService().add(s);

			configParameters.getActivity().add(createConfigsForActivity(ce));
		}
		if (c.getDescription().length() > 0) {
			t.setDescription(automaticDescription.substring(0,
					automaticDescription.length() - 5));
		} else {
			t.setDescription(c.getDescription());
		}
		t.setActivities(activities);
		t.setConfigparameters(configParameters);

		// enter links
		Datalinks datalinks = tgwfFactory.createDatalinks();
		int linkIdcount = 0;
		for (EntryLink l : c.getLinks()) {
			if (l.isDeleted() || l.isUndecided()) {
				continue;
			}
			linkIdcount++;
			Link link = tgwfFactory.createLink();
			if (l.getFrom() == null || l.getFromPort() == null) {
				link.setFromServiceID("null");
				link.setFromParam("null");
				link.setType("input");
				link.setId("i" + Integer.toString(linkIdcount));
			} else {
				link.setFromServiceID(Integer.toString(l.getFrom().getID()));
				link.setFromParam(l.getFromPort().getParam());
			}

			if (l.getTo() == null || l.getToPort() == null) {
				link.setToServiceID("null");
				link.setToParam("null");
				link.setType("output");
				link.setId("o" + Integer.toString(linkIdcount));
			} else {
				link.setToServiceID(Integer.toString(l.getTo().getID()));
				link.setToParam(l.getToPort().getParam());
			}

			if (link.getType() == null) {
				link.setId(Integer.toString(linkIdcount));
				link.setType("interservice");
			}
			datalinks.getLink().add(link);
		}
		t.setDatalinks(datalinks);
	}

	public Activity createConfigsForActivity(ChainEntry ce) {
		Activity a = tgwfFactory.createActivity();
		a.setServiceId(BigInteger.valueOf(ce.getID()));
		for (Configparameter cp : ce.getConfigParameters()) {
			for (int i = 0; i < ce.getChosenConfigParamExampleValues(
					cp.getParam()).size(); i++) {
				Parameter p = tgwfFactory.createParameter();
				p.setParam(cp.getParam());
				p.setParamId(ce
						.getChosenConfigParamExampleValues(cp.getParam())
						.get(i).getId());
				if (p.getParamId().equals(ChainEntry.NEW_FROM_STRING_ID)
						|| p.getParamId().equals(ChainEntry.NEW_FROM_URI_ID)) {

					String seri = ChainEntry.anyToString(ce
							.getChosenConfigParamExampleValues(cp.getParam())
							.get(i).getContent());
					p.getContent().addAll(
							ce.getChosenConfigParamExampleValues(cp.getParam())
									.get(i).getContent());
				}
				a.getParameter().add(p);
			}
		}
		return a;
	}

	public void insertCRUD(String targetProject) {
		CRUD c = tgwfFactory.createCRUD();
		String crudEpr = "";
		try {
			ConfClient confClient = ConfClient.getInstance();
			crudEpr = confClient.getValue(ConfservClientConstants.TG_CRUD);
		} catch (OfflineException e) {
			// TODO Auto-generated catch block
			OnlineStatus.netAccessFailed("Could not contact Confserv", e);
		}
		c.setInstance(crudEpr);
		c.setSessionID(RBACSession.getInstance().getSID(false));
		c.setLogParameter(logsession.getInstance().getloginfo());
		c.setTargetProject(targetProject);
		t.setCRUD(c);
	}

	public void insertInputURI(EntryLink inputlink, String groupId, URI uri) {
		if (t.getInputs() == null) {
			Inputs i = tgwfFactory.createInputs();
			t.setInputs(i);
		}
		info.textgrid.lab.workflow.tgwf.URI u = tgwfFactory.createURI();
		for (Link l : t.getDatalinks().getLink()) {
			if (l.getType().equals("input")
					&& inputlink.getTo().getID() == Integer.valueOf(l
							.getToServiceID())
					&& inputlink.getToPort().getParam().equals(l.getToParam())) {
				u.setLinkId(l.getId());
			}
		}
		u.setGroupId(groupId);
		u.setValue(uri.toString());
		t.getInputs().getURI().add(u);
	}

	public void insertMetadataTransformer(EntryLink outputlink,
			EntryLink inputlink, URI stylesheet,
			HashMap<String, String> xslparams) {
		if (t.getMetadatatransformation() == null) {
			Metadatatransformation mdt = tgwfFactory
					.createMetadatatransformation();
			t.setMetadatatransformation(mdt);
		}
		Output o = tgwfFactory.createOutput();
		for (Link l : t.getDatalinks().getLink()) {
			if (l.getType().equals("output")
					&& outputlink.getFrom().getID() == Integer.valueOf(l
							.getFromServiceID())
					&& outputlink.getFromPort().getParam()
							.equals(l.getFromParam())) {
				o.setOutputLinkId(l.getId());
			}
			if (l.getType().equals("input")
					&& inputlink.getTo().getID() == Integer.valueOf(l
							.getToServiceID())
					&& inputlink.getToPort().getParam().equals(l.getToParam())) {
				o.setFromInputLinkId(l.getId());
			}
		}
		o.setStylesheetURI(stylesheet.toString());
		for (String key : xslparams.keySet()) {
			Xslparam x = tgwfFactory.createXslparam();
			x.setName(key);
			x.setValue(xslparams.get(key));
			o.getXslparam().add(x);
		}
		t.getMetadatatransformation().getOutput().add(o);
	}

	private void encodeB64inlineConfigs() {
		for (Activity a : t.getConfigparameters().getActivity()) {
			for (Parameter p : a.getParameter()) {
				// TODO XXXXXXXXXX TODO
			}
		}
	}

	public Tgwf getTGWF() {
		return t;
	}

	public String marshal() {
		encodeB64inlineConfigs();
		StringWriter xml = new StringWriter();
		JAXB.marshal(t, xml);
		return xml.toString();
	}

}
