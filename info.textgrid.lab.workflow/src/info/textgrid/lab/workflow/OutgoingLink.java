package info.textgrid.lab.workflow;

import info.textgrid.lab.workflow.servicedescription.Input;

public class OutgoingLink {
	ChainEntry to = null;
	Input toPort = null;
	String ID = null;

	public OutgoingLink(ChainEntry to, Input toPort, String iD) {
		super();
		this.to = to;
		this.toPort = toPort;
		ID = iD;
	}

	public ChainEntry getTo() {
		return to;
	}

	public void setTo(ChainEntry to) {
		this.to = to;
	}

	public Input getToPort() {
		return toPort;
	}

	public void setToPort(Input toPort) {
		this.toPort = toPort;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	} 
	

	
}
