package info.textgrid.lab.workflow.views;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.menus.TGOpenWithMenu;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;
import info.textgrid.lab.workflow.EntryLink;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.part.ViewPart;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;

/**
 * View for Lists of input documents for a Workflow
 * 
 * @author martin
 */

public class WorkflowInputs extends ViewPart {
	private ArrayList<TextGridObjectTableViewer> inputViewers;
	private ArrayList<ArrayList<TextGridObject>> inputTGOlists;
	private ArrayList<String> headings;
	private ArrayList<Boolean> multiState;
	private Composite inputBoxes;
	private Composite c;
	private ArrayList<Composite> reminder = new ArrayList<Composite>();

	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public WorkflowInputs() {
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		c = new Composite(parent, SWT.NONE);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		c.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		c.setLayout(glp);

		inputBoxes = new Composite(c, SWT.NONE);

		Composite bottom = new Composite(c, SWT.NONE);
		GridData gdb = new GridData(GridData.FILL, GridData.FILL, true, true);
		bottom.setLayoutData(gdb);
		GridLayout glb = new GridLayout(1, false);
		bottom.setLayout(glb);

		Label howtoAdd = new Label(bottom, SWT.WRAP);
		GridData gd7 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		howtoAdd.setLayoutData(gd7);
		howtoAdd.setText(Messages.WorkflowInputs_DragHelp);

		Button removeInputsButton = new Button(bottom, SWT.PUSH);
		GridData gd6 = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		removeInputsButton.setLayoutData(gd6);

		removeInputsButton.setText(Messages.WorkflowInputs_RemoveButtonText);
		removeInputsButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				removeSelectedInputs();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

	}

	public void drawInputBoxes(ArrayList<EntryLink> links) {
		int numberOfBoxes = links.size();
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		inputBoxes.setLayoutData(gdc);
		GridLayout glp = new GridLayout(numberOfBoxes, true);
		inputBoxes.setLayout(glp);

		inputViewers = new ArrayList<TextGridObjectTableViewer>();
		inputTGOlists = new ArrayList<ArrayList<TextGridObject>>();
		headings = new ArrayList<String>();
		multiState = new ArrayList<Boolean>();
		for (Composite c : reminder) {
			c.dispose();
		}
		reminder.clear();

		int index = 0;
		for (EntryLink l : links) {
			drawOneBox(l, index);
			index++;
		}
		c.layout(true, true);
	}

	public void drawOneBox(EntryLink l, final int index) {
		String heading = l.getTo().getName() + ": " + l.getToPort().getName(); //$NON-NLS-1$
		Boolean isMulti = l.getToPort().isMultiple();
		ArrayList<TextGridObject> tgos = new ArrayList<TextGridObject>();
		inputTGOlists.add(tgos); // #### zero

		Composite box = new Composite(inputBoxes, SWT.BORDER);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		box.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		box.setLayout(glp);
		reminder.add(box);

		Label headingLabel = new Label(box, SWT.SINGLE);
		headingLabel.setText(heading);
		headings.add(heading); // #### one

		Label multiLabel = new Label(box, SWT.SINGLE);
		if (isMulti) {
			multiLabel.setText(Messages.WorkflowInputs_PooledLabel);
		} else {
			multiLabel.setText(Messages.WorkflowInputs_OneByOneLabe);
		}
		multiState.add(isMulti); // #### two

		TextGridObjectTableViewer inputViewer = new TextGridObjectTableViewer(
				box, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER
						| SWT.FULL_SELECTION);
		inputViewers.add(inputViewer); // #### three

		GridData gd5 = new GridData(SWT.FILL, SWT.FILL, true, true);
		inputViewer.getControl().setLayoutData(gd5);
		inputViewer.setContentProvider(new ArrayContentProvider());
		inputViewer.setVisibleColumns(EnumSet.of(Column.TITLE));
		inputViewer.getTable().setHeaderVisible(false);
		inputViewer.addDropSupport(DND.DROP_COPY | DND.DROP_DEFAULT
				| DND.DROP_MOVE,
				new Transfer[] { LocalSelectionTransfer.getTransfer() },
				new ViewerDropAdapter(inputViewer) {

					@Override
					public boolean validateDrop(Object target, int operation,
							TransferData transferType) {
						return true;
					}

					@Override
					public boolean performDrop(Object data) {
						if (data instanceof IStructuredSelection) {
							IStructuredSelection selection = (IStructuredSelection) data;
							TextGridObject[] droppedObjects = AdapterUtils
									.getAdapters(Iterators.toArray(
											(Iterator<?>) selection.iterator(),
											Object.class),
											TextGridObject.class, false);
							inputViewers.get(index).add(droppedObjects);
							inputTGOlists.get(index).addAll(
									ImmutableList.copyOf(droppedObjects));
						}
						return true;
					}
				});

		hookContextMenu(inputViewer);
		inputViewer.setInput(tgos.toArray());
	}

	/**
	 * removes all selected TGOs, in all input boxes
	 */
	private void removeSelectedInputs() {
		for (int index = 0; index < inputViewers.size(); index++) {
			IStructuredSelection iss = (IStructuredSelection) inputViewers.get(
					index).getSelection();
			Iterator issiter = iss.iterator();
			while (issiter.hasNext()) {
				TextGridObject oneTarget = (TextGridObject) issiter.next();
				inputTGOlists.get(index).remove(oneTarget);
			}
			inputViewers.get(index)
					.setInput(inputTGOlists.get(index).toArray());
		}
	}

	public ArrayList<ArrayList<TextGridObject>> getInputs() {
		return inputTGOlists;
	}

	public void addtoInputs(ArrayList<TextGridObject> tgos, int inputIndex) {
		if (inputViewers.get(inputIndex) == null
				|| inputTGOlists.get(inputIndex) == null) {
			return;
		}
		TextGridObject[] existingTGOs = inputTGOlists.get(inputIndex).toArray(
				new TextGridObject[0]);
		Iterator<TextGridObject> iter = tgos.iterator();
		while (iter.hasNext()) {
			TextGridObject oneTGO = iter.next();
			boolean foundit = false;
			for (TextGridObject t : existingTGOs) {
				if (oneTGO == t) {
					foundit = true;
				}
			}
			if (!foundit) {
				Long oneTGOSize = Long.valueOf(0);
				boolean couldNotDetermineSize = false;
				try {
					oneTGOSize = oneTGO.getSize();
				} catch (CoreException ce) {
					couldNotDetermineSize = true;
				}
				if (couldNotDetermineSize) {
					showMessage(Messages.WorkflowInputs_SizeWarning);
				} else if (oneTGOSize > 1024 * 1024) {
					showMessage(MessageFormat
							.format(Messages.WorkflowInputs_TooLargeWarning,
									oneTGOSize.toString()));
				}

				// independent of last warnings, add the document to the inputs.
				inputTGOlists.get(inputIndex).add(oneTGO);
			}
		}
		inputViewers.get(inputIndex).setInput(
				inputTGOlists.get(inputIndex).toArray());
	}

	private void hookContextMenu(final TableViewer viewer) {
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				WorkflowInputs.this.fillContextMenu(manager, viewer);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void fillContextMenu(IMenuManager manager, TableViewer viewer) {
		MenuManager openWithSubMenu = new MenuManager(
				Messages.WorkflowInputs_openwithmenu);
		Object firstElement = ((IStructuredSelection) viewer.getSelection())
				.getFirstElement();
		if (firstElement == null)
			return;
		IFile iFile = (IFile) ((IAdaptable) firstElement)
				.getAdapter(IFile.class);

		openWithSubMenu.add(new TGOpenWithMenu(getSite().getPage(), iFile));
		// Other plug-ins can contribute their actions here
		manager.add(openWithSubMenu);
		manager.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void hookDoubleClickAction(TableViewer viewer) {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				// doubleClickAction.run();
			}
		});
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(inputBoxes.getShell(),
				Messages.WorkflowInputs_CommonMessagesTitle, message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		inputBoxes.setFocus();
	}

}
