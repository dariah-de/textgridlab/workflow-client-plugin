package info.textgrid.lab.workflow.views;

import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.EntryLink;
import info.textgrid.lab.workflow.MetadataTransformer;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

/**
 * Show the output links of the workflow where new metadata must be generated,
 * and the selected input links where md are based on.
 * @deprecated, use info.textgrid.lab.workflow.wizard.*
 * @author martin
 */

public class MetadataTransformView extends ViewPart {
	private Composite outerC;
	private Composite fromFields;
	private ArrayList<EntryLink> outputs;
	private ArrayList<ComboViewer> combos;
	private ArrayList<EntryLink> inputs;
	private Chain chain = null;

	public MetadataTransformView() {
	}

	class mdtLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			EntryLink l;
			if (obj instanceof EntryLink) {
				l = (EntryLink) obj;
				return l.getTo().getName() + "/" + l.getToPort().getName();
			} else {
				return "unknown class: " + obj.getClass();
			}
		}
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		outerC = new Composite(parent, SWT.NONE);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		outerC.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		outerC.setLayout(glp);

		if (chain != null) {
			refreshTransformerView(chain);
		}
	}

	private void readLinks() {
		outputs = new ArrayList<EntryLink>();
		inputs = new ArrayList<EntryLink>();
		for (EntryLink l : chain.getLinks()) {
			if (l.isUndecided() || l.isDeleted()) {
				continue;
			}
			if (l.getFrom() == null && l.getFromPort() == null) {
				inputs.add(l);
			}
			if (l.getTo() == null && l.getToPort() == null) {
				outputs.add(l);
			}
		}
	}

	public void refreshTransformerView(Chain c) {
		this.chain = c;
		readLinks();
		combos = new ArrayList<ComboViewer>();
		if (fromFields != null) {
			fromFields.dispose();
		}
		fromFields = new Composite(outerC, SWT.FILL);
		fromFields.setLayout(new GridLayout(4, false));
		GridData gd2 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		fromFields.setLayoutData(gd2);

		for (EntryLink l : outputs) {
			Label outputName = new Label(fromFields, SWT.SINGLE);
			outputName.setText(l.getFrom().getName() + " / "
					+ l.getFromPort().getName());

			Label fromLabel = new Label(fromFields, SWT.SINGLE);
			fromLabel.setText("generate from");

			ComboViewer cv = new ComboViewer(fromFields, SWT.SINGLE);
			cv.setLabelProvider(new mdtLabelProvider());
			cv.setContentProvider(new ArrayContentProvider());
			cv.setInput(inputs);
			// set default if only one input
			if (inputs.size() == 1) {
				cv.setSelection(new StructuredSelection(inputs.get(0)), true);
			}
			combos.add(cv);

			MetadataTransformer myMDT = null;
			for (MetadataTransformer mdt : chain.getTransformers()) {
				// we can re-use an existing Transformer if there has been set
				// one for this output and the input it referred to is still
				// existing
				if (mdt.getOutput().equals(l)
						&& inputs.contains(mdt.getFromInput())) {
					myMDT = mdt;
					cv.setSelection(
							new StructuredSelection(mdt.getFromInput()), true);
				}
			}
			if (myMDT == null) {
				myMDT = new MetadataTransformer();
				myMDT.setOutput(l);
				myMDT.setDefaultParams();
				chain.getTransformers().add(myMDT);
			}
			final int myMDTindex = chain.getTransformers().indexOf(myMDT);

			Button configureButton = new Button(fromFields, SWT.PUSH);
			configureButton.setText("Configure...");
			configureButton.addSelectionListener(new SelectionListener() {
				public void widgetSelected(SelectionEvent event) {

					MetadataTransformerDialogue mdtd = new MetadataTransformerDialogue(
							PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getShell());
					mdtd.setContents(chain.getTransformers().get(myMDTindex));
					mdtd.setBlockOnOpen(true);
					mdtd.open();
				}

				public void widgetDefaultSelected(SelectionEvent e) {
					widgetSelected(e);
				}
			});
		}
		outerC.layout(true, true);
	}

	public void setFromInputs() {
		Iterator<EntryLink> oIt = outputs.iterator();
		for (ComboViewer cv : combos) {
			EntryLink outputLink = oIt.next(); // assume same order as we built
												// it this way
			IStructuredSelection spss = (IStructuredSelection) cv
					.getSelection();
			EntryLink inputLink = null;
			if (spss.getFirstElement() instanceof EntryLink) {
				inputLink = (EntryLink) spss.getFirstElement();
				if (inputLink != null) {
					for (MetadataTransformer mdt : chain.getTransformers()) {
						if (mdt.getOutput().equals(outputLink)) {
							mdt.setFromInput(inputLink);
							System.out.println("Metadata for "
									+ outputLink.getFromPort().getName()
									+ " will be generated from "
									+ inputLink.getToPort().getName());
						}
					}
				} else {
					System.out.println("Bäääh");
				}
			} else {
				System.out.println("Bäääh²");
			}
		}
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(fromFields.getShell(), "Service List",
				message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		outerC.setFocus();
	}

	@Override
	public void dispose() {
	}

}
