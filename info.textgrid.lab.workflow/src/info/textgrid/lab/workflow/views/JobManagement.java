package info.textgrid.lab.workflow.views;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.workflow.Activator;
import info.textgrid.lab.workflow.WorkflowEngine;
import info.textgrid.lab.workflow.WorkflowJob;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.PortGwesproxy;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

/**
 * Upon button click, this view polls at the workflow engine for workflows owned
 * by the logged-in user and displays them in a viewer. a second button opens
 * the resulting TextGridObjects in the WorkflowResults View.
 * 
 * @author martin
 * 
 */
public class JobManagement extends ViewPart {
	public final static String WORKFLOW_RESULTS_VIEW_ID = "info.textgrid.lab.workflow.views.WorkflowResults"; //$NON-NLS-1$
	private TableViewer viewer;
	private ArrayList<WorkflowJob> jobList = new ArrayList<WorkflowJob>();
	private PortGwesproxy proxy;
	private SimpleDateFormat shortDateFormatter = new SimpleDateFormat(
			"dd.MM. HH:mm:ss"); //$NON-NLS-1$
	private Job neverEndingJob = null;
	private Job openOutputsJob = null;

	private WorkflowJob[] jobArray;
	private WorkflowResults wpInstance = null;

	public JobManagement() {
		proxy = WorkflowEngine.getInstance().getGwesProxy();
	}

	class ViewLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			if (index == 0) {
				return obj.toString(); // description
			} else if (index == 1) {
				return shortDateFormatter.format(((WorkflowJob) obj).getDate());
			} else if (index == 2) {
				String statusstring = ((WorkflowJob) obj).getStatus();
				if (statusstring.equals("TERMINATED")) { //$NON-NLS-1$
					return Messages.JobManagement_StatusFailed;
				} else if (statusstring.equals("COMPLETED")) { //$NON-NLS-1$
					return Messages.JobManagement_StatusCompleted;
				} else if (statusstring.equals("ACTIVE")) { //$NON-NLS-1$
					return Messages.JobManagement_StatusActive;
				} else {
					return statusstring.toLowerCase();
				}
			} else
				return "extra column " + index; //$NON-NLS-1$

		}

		public Image getColumnImage(Object obj, int index) {
			return null;// getImage(obj);
		}

		public Image getImage(Object obj) {
			// return PlatformUI.getWorkbench().
			// getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
			return null;
		}
	}

	class NameSorter extends ViewerSorter {
	}

	@Override
	public void createPartControl(Composite parent) {
		Composite c = new Composite(parent, SWT.FILL);
		c.setLayout(new GridLayout(1, false));
		GridData cGD = new GridData();
		cGD.horizontalAlignment = GridData.FILL;
		cGD.grabExcessVerticalSpace = true;
		cGD.grabExcessHorizontalSpace = true;
		cGD.heightHint = 100;
		cGD.widthHint = 400;
		cGD.verticalAlignment = GridData.FILL;
		c.setLayoutData(cGD);

		viewer = new TableViewer(c, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION);
		// viewer.setContentProvider(new ViewContentProvider());

		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);
		TableColumn tcDesc = new TableColumn(viewer.getTable(), SWT.NONE);
		tcDesc.setText(Messages.JobManagement_WFJobTitle);
		tcDesc.setWidth(150);
		TableColumn tcStart = new TableColumn(viewer.getTable(), SWT.NONE);
		tcStart.setText(Messages.JobManagement_WFJobStart);
		tcStart.setWidth(100);
		TableColumn tcStat = new TableColumn(viewer.getTable(), SWT.NONE);
		tcStat.setText(Messages.JobManagement_WFJobStatus);
		tcStat.setWidth(70);

		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		refreshJobList();
		viewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				openOutputs();
			}
		});

		GridData vGD = new GridData();
		vGD.horizontalAlignment = GridData.FILL;
		vGD.grabExcessVerticalSpace = true;
		vGD.grabExcessHorizontalSpace = true;
		// vGD.heightHint = 100;
		// vGD.widthHint = 400;
		vGD.verticalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(vGD);

		Composite bottomBar = new Composite(c, SWT.FILL);
		bottomBar.setLayout(new GridLayout(4, false));
		GridDataFactory.fillDefaults().applyTo(bottomBar);

		/*
		 * Button seeResults = new Button(bottomBar, SWT.PUSH);
		 * seeResults.setText("See results");
		 * seeResults.addSelectionListener(new SelectionListener() { public void
		 * widgetSelected(SelectionEvent e) { openOutputs(); }
		 * 
		 * public void widgetDefaultSelected(SelectionEvent e) {
		 * widgetSelected(e); } });
		 */
		Button abortButton = new Button(bottomBar, SWT.PUSH);
		abortButton.setText(Messages.JobManagement_AbortButtonText);
		abortButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				abortWorkflow();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

	}

	/**
	 * Takes first selected WorkflowJob, parses the result and opens a result
	 * view with these TGOs for further processing.
	 */
	public void openOutputs() {

		if (openOutputsJob != null) {
			openOutputsJob.cancel();
		}
		openOutputsJob = new Job(Messages.JobManagement_JobOpeningResults) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				viewer.getTable().getDisplay().asyncExec(new Runnable() {
					public void run() {

						IStructuredSelection wfss = (IStructuredSelection) viewer
								.getSelection();
						WorkflowJob wfJ = (WorkflowJob) wfss.getFirstElement();
						if (wfJ == null)
							return;
						if (wfJ.hasErrors()) {
							logNshowErrors(wfJ);
							wfJ = null;
						}
						if (wpInstance == null) {
							IViewPart wfResultsView = null;
							try {
								wfResultsView = PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow()
										.getActivePage()
										.showView(WORKFLOW_RESULTS_VIEW_ID);
							} catch (PartInitException e) {
								IStatus status = new Status(IStatus.ERROR,
										Activator.PLUGIN_ID,
										"Could not open workflow results view", //$NON-NLS-1$
										e);
								Activator.getDefault().getLog().log(status);
							}
							if (wfResultsView != null
									&& wfResultsView instanceof WorkflowResults) {
								WorkflowResults wpInstance = (WorkflowResults) wfResultsView;
								wpInstance.refreshViewer(wfJ);
							}
						} else {
							wpInstance.refreshViewer(wfJ);
						}
					}
				});
				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				return Status.OK_STATUS;
			}
		};
		openOutputsJob.setPriority(Job.SHORT);
		openOutputsJob.schedule();
	}

	/**
	 * aborts a workflow
	 */
	public void abortWorkflow() {
		IStructuredSelection wfss = (IStructuredSelection) viewer
				.getSelection();
		WorkflowJob oneJob = (WorkflowJob) wfss.getFirstElement();
		boolean success = WorkflowEngine.getInstance().abortWorkflow(
				oneJob.getWfID());
		// TODO do something if couldnt abort
		//		System.out.println("Abortion success: " + success); //$NON-NLS-1$
		refreshJobList();
	}

	public void spawnNeverEndingJob() {
		if (neverEndingJob != null) {
			neverEndingJob.cancel();
		}
		neverEndingJob = new Job(Messages.JobManagement_JobMgmtUpdaterJob) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {

				boolean allJobsCompleted = false;
				while (!allJobsCompleted && !viewer.getControl().isDisposed()) {
					allJobsCompleted = true;
					//					System.out.println("Getting jobs..."); //$NON-NLS-1$
					// TODO sleep a bit?
					ArrayList<WorkflowJob> jobList = WorkflowEngine
							.getInstance().getMyJobs();
					for (WorkflowJob wfjob : jobList) {
						if (wfjob.getStatus().equals("ACTIVE")) { //$NON-NLS-1$
							allJobsCompleted = false;
							break;
						}
					}
					jobArray = jobList.toArray(new WorkflowJob[0]);
					Arrays.sort(jobArray, null);

					viewer.getTable().getDisplay().asyncExec(new Runnable() {
						public void run() {
							viewer.setInput(jobArray);
						}
					});

					try {
						Thread.sleep(2500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (monitor.isCanceled())
						return Status.CANCEL_STATUS;
				}
				return Status.OK_STATUS;
			}
		};
		neverEndingJob.setSystem(true);
		neverEndingJob.setPriority(Job.LONG);
		neverEndingJob.schedule();
	}

	public void refreshJobList() {
		if (RBACSession.getInstance().getSID(false).equals("")) { //$NON-NLS-1$
			return;
		}
		spawnNeverEndingJob();
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	private void logNshowErrors(WorkflowJob j) {
		IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				j.getErrors(), new Exception(
						"GWES workflow engine found errors in workflow " //$NON-NLS-1$
								+ j.toString() + " started at " //$NON-NLS-1$
								+ shortDateFormatter.format(j.getDate())));
		Activator.getDefault().getLog().log(status);

		MessageBox mb = new MessageBox(getSite().getWorkbenchWindow()
				.getShell(), SWT.OK | SWT.ICON_ERROR);
		mb.setText(MessageFormat.format(
				Messages.JobManagement_FailedMessageTitle,
				j.toString(), shortDateFormatter.format(j.getDate())));
		mb.setMessage(Messages.JobManagement_FailedMessageDetails + j.getErrors());
		mb.open();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		super.dispose();
		neverEndingJob.cancel();
	}
}
