package info.textgrid.lab.workflow.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.workflow.views.messages"; //$NON-NLS-1$
	public static String ConfigParameterEditingDialogue_savebutton;
	public static String JobManagement_AbortButtonText;
	public static String JobManagement_FailedMessageDetails;
	public static String JobManagement_FailedMessageTitle;
	public static String JobManagement_JobMgmtUpdaterJob;
	public static String JobManagement_JobOpeningResults;
	public static String JobManagement_StatusActive;
	public static String JobManagement_StatusCompleted;
	public static String JobManagement_StatusFailed;
	public static String JobManagement_WFJobStart;
	public static String JobManagement_WFJobStatus;
	public static String JobManagement_WFJobTitle;
	public static String ListOfWorkflows_25;
	public static String ListOfWorkflows_26;
	public static String ListOfWorkflows_27;
	public static String ListOfWorkflows_28;
	public static String ListOfWorkflows_CommonViewMessageTiitle;
	public static String ListOfWorkflows_DifferentNumberOfInputsMesssage;
	public static String ListOfWorkflows_EditButtonText;
	public static String ListOfWorkflows_NewButtonText;
	public static String ListOfWorkflows_NoTargetProjectSelectedMessage;
	public static String ListOfWorkflows_openwithmenu;
	public static String ListOfWorkflows_RefreshButtonText;
	public static String ListOfWorkflows_RunButtonText;
	public static String ListOfWorkflows_TargetProjectLabel;
	public static String MetadataTransformerDialogue_EditMetadataForText;
	public static String WorkflowInputs_CommonMessagesTitle;
	public static String WorkflowInputs_DragHelp;
	public static String WorkflowInputs_OneByOneLabe;
	public static String WorkflowInputs_openwithmenu;
	public static String WorkflowInputs_PooledLabel;
	public static String WorkflowInputs_RemoveButtonText;
	public static String WorkflowInputs_SizeWarning;
	public static String WorkflowInputs_TooLargeWarning;
	public static String WorkflowResults_openwith;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
