package info.textgrid.lab.workflow.views;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.ProjectDoesNotExistException;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider.IDoneListener;
import info.textgrid.lab.core.swtutils.PendingLabelProvider;
import info.textgrid.lab.ui.core.menus.TGOpenWithMenu;
import info.textgrid.lab.ui.core.utils.UpdatingDeferredListContentProvider;
import info.textgrid.lab.workflow.Activator;
import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.ChainEntry;
import info.textgrid.lab.workflow.ChainTGWF;
import info.textgrid.lab.workflow.servicedescription.Service;
import info.textgrid.lab.workflow.tgwf.Tgwf;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;

import javax.xml.bind.JAXB;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.ViewPart;

/**
 * View for the workflow chain consisting of ordered, configured and linked
 * services.
 * @deprecated, use info.textgrid.lab.workflow.wizard.*
 * @author martin
 */

public class ChainView extends ViewPart {
	private TableViewer viewer;
	private Chain chain;
	private String tgwf;
	private ComboViewer saveProjectViewer;
	private Text workflowNameText;
	private Button finishButton;
	private TextGridObject originalTGO = null;
	private final UpdatingDeferredListContentProvider sPcontentProvider = new UpdatingDeferredListContentProvider();
	private IDoneListener doneListener;
	
	// class NameSorter extends ViewerSorter {
	// }

	class ChainLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			ChainEntry ce;
			if (obj instanceof ChainEntry) {
				ce = (ChainEntry) obj;
			} else {
				return "unknown class: " + obj.getClass();
			}
			if (index == 0) {
				return ce.getName();
			} else {
				return "extra column " + index;
			}
		}

		public Image getColumnImage(Object obj, int index) {
			return null;// getImage(obj);
		}

		public Image getImage(Object obj) {
			// return PlatformUI.getWorkbench().
			// getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
			return null;
		}
	}

	/**
	 * The constructor.
	 */
	public ChainView() {
		chain = new Chain();
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite c = new Composite(parent, SWT.NONE);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		c.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		c.setLayout(glp);

		viewer = new TableViewer(c, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.BORDER | SWT.FULL_SELECTION);
		viewer.getTable().setHeaderVisible(false);
		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setLabelProvider(new ChainLabelProvider());

		GridData gd1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		viewer.getControl().setLayoutData(gd1);
		// viewer.setSorter(new NameSorter());
		refreshViewer();
		getSite().setSelectionProvider(viewer);

		// makeActions();
		hookContextMenu(viewer);
		// hookDoubleClickAction();

		Composite aboveBottomBar = new Composite(c, SWT.FILL);
		aboveBottomBar.setLayout(new GridLayout(1, false));
		GridData gd2 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		aboveBottomBar.setLayoutData(gd2);
		// GridDataFactory.fillDefaults().applyTo(underbottomBar);

		GridData gd4a = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		Label workflowName = new Label(aboveBottomBar, SWT.SINGLE);
		workflowName.setLayoutData(gd4a);
		workflowName.setText("Workflow name");

		workflowNameText = new Text(aboveBottomBar, SWT.SINGLE | SWT.BORDER);
		GridData gd4b = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd4b.widthHint = 2000;
		workflowNameText.setLayoutData(gd4b);

		GridData gd3a = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		Label saveProjectlabel = new Label(aboveBottomBar, SWT.SINGLE);
		saveProjectlabel.setLayoutData(gd3a);
		saveProjectlabel.setText("Save in Project");

		saveProjectViewer = new ComboViewer(aboveBottomBar, SWT.SINGLE);
		GridData gd6b = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd6b.widthHint = 2000;
		saveProjectViewer.getControl().setLayoutData(gd6b);

		saveProjectViewer.setContentProvider(sPcontentProvider);
		saveProjectViewer.setLabelProvider(new PendingLabelProvider());
		saveProjectViewer.setInput(TextGridProjectRoot
				.getInstance(TextGridProjectRoot.LEVELS.EDITOR));

		Composite bottomBar = new Composite(c, SWT.FILL);
		bottomBar.setLayout(new GridLayout(2, false));
		GridData gd4 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		bottomBar.setLayoutData(gd4);

		Button newButton = new Button(bottomBar, SWT.PUSH);
		newButton.setText("New");
		newButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				originalTGO = null;
				chain = new Chain();
				refreshViewer();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Button validateButton = new Button(bottomBar, SWT.PUSH);
		validateButton.setText("Edit Metadata");
		validateButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				chain.connectOutputs();
				boolean valid = validateChain();
				chain.dumpLinks();
				System.out.println("Chain is valid: " + valid);
				MetadataTransformView mv = openMetadataView();
				mv.refreshTransformerView(chain);

				finishButton.setEnabled(true);
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Button clearButton = new Button(bottomBar, SWT.PUSH);
		clearButton.setText("Clear Links");
		clearButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				chain.clearLinks();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		finishButton = new Button(bottomBar, SWT.PUSH);
		finishButton.setText("Finish and Save");
		finishButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				// TODO refactor this code, put into a job, give a non-null
				// progress monitor in setContents

				MetadataTransformView mv = openMetadataView();
				mv.setFromInputs(); // actually read now the fields currently
									// visible and put them into the chain's
									// transformers

				String title = workflowNameText.getText();
				chain.setDescription(title);
				tgwf = chain.toString(); // will be automatic description
												// if ""
				// Save the TGO...
				IStructuredSelection spss = (IStructuredSelection) saveProjectViewer
						.getSelection();
				TextGridProject saveProject = null;
				if (spss.getFirstElement() instanceof TextGridProject) {
					saveProject = (TextGridProject) spss.getFirstElement();
					if (saveProject == null) {
						showMessage("No target project selected.");
						return;
					}
				} else {
					showMessage("No target project selected!");
				}

				if (title == null || title.length() < 1) {
					int a = tgwf.indexOf("<description>");
					int z = tgwf.indexOf("</description>");
					title = tgwf.substring(a + "<description>".length(), z);
				}

				boolean isNewObject = true;
				try {
					if (originalTGO != null
							&& title.equals(originalTGO.getTitle())
							&& saveProject.getId().equals(
									originalTGO.getProject())) {
						isNewObject = false;
					}
				} catch (CoreException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

				TextGridObject tgwfTGO;
				if (isNewObject) {
					tgwfTGO = TextGridObject.getNewObjectInstance(saveProject,
							TGContentType
									.getContentType("text/tg.workflow+xml"),
							title);
				} else {
					tgwfTGO = originalTGO;
				}

				IFile file = AdapterUtils.getAdapter(tgwfTGO, IFile.class);
				if (file == null)
					System.out.println("Cannot save " + title);
				else {
					try {
						file.setContents(
								new ByteArrayInputStream(tgwf.getBytes("UTF-8")),
								true, false, null);
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (CoreException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				originalTGO = tgwfTGO; // just to rememeber this such that we wont save twice
				openSubmitPerspective();
				// now refresh ListOfWorkflows viewer;
				IViewPart wfView = null;
				try {
					wfView = PlatformUI
							.getWorkbench()
							.getActiveWorkbenchWindow()
							.getActivePage()
							.showView(
									"info.textgrid.lab.workflow.views.WorkflowSelection");
				} catch (PartInitException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (wfView != null && wfView instanceof ListOfWorkflows) {
										ListOfWorkflows wfInstance = (ListOfWorkflows) wfView;
					wfInstance.refreshViewer();
				} 
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		finishButton.setEnabled(false);

	}

	private void openSubmitPerspective() {
		try {
			// first reset the perspective...
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchPage wbPage = wb.getActiveWorkbenchWindow()
					.getActivePage();
			if (wbPage == null)
				wb.getActiveWorkbenchWindow().openPage(
						"info.textgrid.lab.workflow.WorkflowPerspective", null);
			wbPage.setPerspective(wb.getPerspectiveRegistry()
					.findPerspectiveWithId(
							"info.textgrid.lab.workflow.WorkflowPerspective"));
			// show it when necessary
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.workflow.WorkflowPerspective",
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Could not open Workflow Submit Perspective!", e);
			Activator.getDefault().getLog().log(status);
		}
	}

	private void refreshViewer() {
		viewer.setInput(chain.getChain());
	}

	private boolean validateChain() {
		return chain.validateLinks();
	}

	private void openConfigureView() {
		IStructuredSelection wfss = (IStructuredSelection) viewer
				.getSelection();
		ChainEntry ce = (ChainEntry) wfss.getFirstElement();
		if (ce == null) {
			return;
		}

		IViewPart configureView = null;
		try {
			configureView = PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.showView("info.textgrid.lab.workflow.views.ChainEntryView");
		} catch (PartInitException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"View for Service Configuration is not existent", e);
			Activator.getDefault().getLog().log(status);
		}

		if (configureView != null && configureView instanceof ChainEntryView) {
			ChainEntryView cevInstance = (ChainEntryView) configureView;
			cevInstance.setChainEntry(ce);
		}
	}

	/**
	 * Called by ListOfServices, adds the service to (the end of) this workflow
	 * orderedServices
	 * 
	 * @param service
	 */
	public void addService(TextGridObject service) {
		IFile sf = (IFile) service.getAdapter(IFile.class);
		try {
			Service so = JAXB.unmarshal(sf.getContents(true), Service.class);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ChainEntry centry = new ChainEntry(service);
		chain.addEntry(centry);
		refreshViewer();
		viewer.setSelection(new StructuredSelection(centry), true);
	}

	public void setChainFromTGWF(TextGridObject tgwfTGO) {
		originalTGO = tgwfTGO;
		IFile tgwfFile = (IFile) tgwfTGO.getAdapter(IFile.class);
		Tgwf tgwf = null;
		try {
			tgwf = JAXB.unmarshal(tgwfFile.getContents(true), Tgwf.class);
			// TODO get Chain back
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ChainTGWF ct = new ChainTGWF(tgwf);
		this.chain = ct.getChain();
		try {
			workflowNameText.setText(tgwfTGO.getTitle());

			final String origProjectId = tgwfTGO.getProject();
			doneListener = new IDoneListener () {

				@Override
				public void loadDone(Viewer viewer) {
					// TODO Auto-generated method stub
					try {
						saveProjectViewer.setSelection(new StructuredSelection(
								TextGridProject.getProjectInstance(origProjectId)),
								true);
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (CrudServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ProjectDoesNotExistException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			
			};
			sPcontentProvider.addDoneListener(doneListener);


		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MetadataTransformView mv = openMetadataView();
		mv.refreshTransformerView(chain);
		finishButton.setEnabled(true);

		refreshViewer();
	}

	public Chain getChain() {
		return chain;
	}

	private void hookContextMenu(final TableViewer viewer) {
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				ChainView.this.fillContextMenu(manager, viewer);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void fillContextMenu(IMenuManager manager, TableViewer viewer) {
		MenuManager openWithSubMenu = new MenuManager("Open with...");
		Object firstElement = ((IStructuredSelection) viewer.getSelection())
				.getFirstElement();
		if (firstElement == null)
			return;
		IFile iFile = (IFile) ((IAdaptable) firstElement)
				.getAdapter(IFile.class);

		openWithSubMenu.add(new TGOpenWithMenu(getSite().getPage(), iFile));
		// Other plug-ins can contribute their actions here
		manager.add(openWithSubMenu);
		manager.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void hookDoubleClickAction(TableViewer viewer) {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				// doubleClickAction.run();
			}
		});
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(),
				"Service List", message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@Override
	public void dispose() {
		sPcontentProvider.removeDoneListener(doneListener);
	}

	private MetadataTransformView openMetadataView() {
		IViewPart mdtView = null;
		try {
			mdtView = PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.showView(
							"info.textgrid.lab.workflow.views.MetadataTransform");
		} catch (PartInitException pie) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"View for Metadata Transformation is not existent", pie);
			Activator.getDefault().getLog().log(status);
		}

		MetadataTransformView mdtInstance = null;
		if (mdtView != null && mdtView instanceof MetadataTransformView) {
			mdtInstance = (MetadataTransformView) mdtView;
		}
		return mdtInstance;
	}

}
