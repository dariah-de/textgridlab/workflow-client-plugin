package info.textgrid.lab.workflow.views;

import info.textgrid.lab.authn.AuthBrowser;
import info.textgrid.lab.authn.AuthBrowser.ISIDChangedListener;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.swtutils.PendingLabelProvider;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.menus.TGOpenWithMenu;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;
import info.textgrid.lab.ui.core.utils.UpdatingDeferredListContentProvider;
import info.textgrid.lab.workflow.Activator;
import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.ChainGWDL;
import info.textgrid.lab.workflow.ChainTGWF;
import info.textgrid.lab.workflow.EntryLink;
import info.textgrid.lab.workflow.WorkflowJob;
import info.textgrid.lab.workflow.tgwf.Tgwf;
import info.textgrid.lab.workflow.wizard.WorkflowWizard;

import java.util.ArrayList;
import java.util.EnumSet;

import javax.xml.bind.JAXB;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.ViewPart;

/**
 * View for Workflow things: select one, open it (optionally), update it with
 * currently selected TextGrid URIs, start deployment and monitoring.
 * 
 * @author martin
 */

public class ListOfWorkflows extends ViewPart {
	private TextGridObjectTableViewer viewer;
	private ComboViewer selectedProjectViewer;
	ISIDChangedListener sidChangedListener;
	private Chain chain;
	private ArrayList<EntryLink> inputLinks;
	private final ISelectionListener selectedWorkflowListener;
	private Job currentJob = null;
	private TextGridObject lastSelectedWorkflow = null;

	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public ListOfWorkflows() {
		selectedWorkflowListener = new ISelectionListener() {
			public void selectionChanged(IWorkbenchPart sourcepart,
					ISelection selection) {
				if (sourcepart == ListOfWorkflows.this
						&& selection instanceof IStructuredSelection) {
					final IStructuredSelection iss = (IStructuredSelection) selection;
					// take first selected element
					if (!iss.isEmpty()) {
						Object firstelem = iss.toArray()[0];
						if (firstelem instanceof TextGridObject) {
							TextGridObject tgo = (TextGridObject) firstelem;
							String format = null;
							try {
								format = ((TextGridObject) firstelem)
										.getContentTypeID();
							} catch (CoreException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (format.equals("text/tg.workflow+xml")) { //$NON-NLS-1$
								if (lastSelectedWorkflow != null
										&& lastSelectedWorkflow.equals(tgo)) {
									// do nothing
								} else {
									showInputs(tgo);
									lastSelectedWorkflow = tgo;
								}
							}
						}
					}
				}
			}
		};
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite c = new Composite(parent, SWT.NONE);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		c.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		c.setLayout(glp);

		viewer = new TextGridObjectTableViewer(c, SWT.SINGLE | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION);
		viewer.setVisibleColumns(EnumSet.of(Column.TITLE));
		viewer.getTable().setHeaderVisible(false);

		GridData gd1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		viewer.getControl().setLayoutData(gd1);
		viewer.setSorter(new NameSorter());
		refreshViewer();
		getSite().setSelectionProvider(viewer);

		// makeActions();
		hookContextMenu(viewer);
		// hookDoubleClickAction();

		Composite aboveBottomBar = new Composite(c, SWT.FILL);
		aboveBottomBar.setLayout(new GridLayout(2, false));
		GridData gd2 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		aboveBottomBar.setLayoutData(gd2);
		// GridDataFactory.fillDefaults().applyTo(underbottomBar);

		GridData gd3a = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		Label targetProjectlabel = new Label(aboveBottomBar, SWT.SINGLE);
		targetProjectlabel.setLayoutData(gd3a);
		targetProjectlabel.setText(Messages.ListOfWorkflows_TargetProjectLabel);
		getSite().getPage().addSelectionListener(selectedWorkflowListener);

		selectedProjectViewer = new ComboViewer(aboveBottomBar, SWT.SINGLE);
		GridData gd3b = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd3b.widthHint = 2000;
		selectedProjectViewer.getControl().setLayoutData(gd3b);

		final UpdatingDeferredListContentProvider sPcontentProvider = new UpdatingDeferredListContentProvider();
		selectedProjectViewer.setContentProvider(sPcontentProvider);
		selectedProjectViewer.setLabelProvider(new PendingLabelProvider());
		selectedProjectViewer.setInput(TextGridProjectRoot
				.getInstance(TextGridProjectRoot.LEVELS.EDITOR));

		Composite bottomBar = new Composite(c, SWT.FILL);
		bottomBar.setLayout(new GridLayout(2, false));
		GridData gd4 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		bottomBar.setLayoutData(gd4);

		Button refreshButton = new Button(bottomBar, SWT.PUSH);
		refreshButton.setText(Messages.ListOfWorkflows_RefreshButtonText);
		refreshButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				refreshViewer();
				selectedProjectViewer.setInput(TextGridProjectRoot
						.getInstance(TextGridProjectRoot.LEVELS.EDITOR));
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Button editB = new Button(bottomBar, SWT.PUSH);
		editB.setText(Messages.ListOfWorkflows_EditButtonText);
		editB.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				// startEditWF();
				startEditWFWizard();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Button newB = new Button(bottomBar, SWT.PUSH);
		newB.setText(Messages.ListOfWorkflows_NewButtonText);
		newB.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				WorkflowWizard wizard = new WorkflowWizard();

				wizard.init(getSite().getWorkbenchWindow().getWorkbench(), null);

				// Instantiates the wizard container with the wizard and opens it
				WizardDialog dialog = new WizardDialog(getSite().getShell(), wizard);
				dialog.create();
				dialog.open();
			
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		Button applyURIsButton = new Button(bottomBar, SWT.PUSH);
		applyURIsButton.setText(Messages.ListOfWorkflows_RunButtonText);
		applyURIsButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				startJob();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		sidChangedListener = new ISIDChangedListener() {
			public void sidChanged(String newSID, String newEPPN) {
				refreshViewer();
//				System.out.println("Change, new eppn is: " + newEPPN); //$NON-NLS-1$
				// selectedProjectViewer.setInput(TextGridProjectRoot
				// .getInstance(TextGridProjectRoot.LEVELS.EDITOR));

			}
		};
		AuthBrowser.addSIDChangedListener(sidChangedListener);

	}

	private void openInputs(TextGridObject wfTGO) {

		// TODO how to keep user from selecting and running another workflow
		// once he selected and gave input to this one?
		// viewer.getTable().setEnabled(false);

	}

	private void openWFcreationPerspective() {
		try {
			// first reset the perspective...
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchPage wbPage = wb.getActiveWorkbenchWindow()
					.getActivePage();
			if (wbPage == null)
				wb.getActiveWorkbenchWindow()
						.openPage(
								"info.textgrid.lab.workflow.WorkflowCreationPerspective", //$NON-NLS-1$
								null);
			wbPage.setPerspective(wb
					.getPerspectiveRegistry()
					.findPerspectiveWithId(
							"info.textgrid.lab.workflow.WorkflowCreationPerspective")); //$NON-NLS-1$
			// show it when necessary
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.workflow.WorkflowCreationPerspective", //$NON-NLS-1$
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Could not open Workflow Creation Perspective!", e); //$NON-NLS-1$
			Activator.getDefault().getLog().log(status);
		}
	}

	private void startEditWFWizard() {
		IStructuredSelection wfss = (IStructuredSelection) viewer
				.getSelection();
		WorkflowWizard wizard = new WorkflowWizard();

		if ((wfss instanceof IStructuredSelection) || (wfss == null))
			wizard.init(getSite().getWorkbenchWindow().getWorkbench(), wfss);

		// Instantiates the wizard container with the wizard and opens it
		WizardDialog dialog = new WizardDialog(getSite().getShell(), wizard);
		dialog.create();
		dialog.open();

	}

	private void startEditWF() {
		openWFcreationPerspective();

		IStructuredSelection wfss = (IStructuredSelection) viewer
				.getSelection();
		TextGridObject wfTGO = (TextGridObject) wfss.getFirstElement();
		if (wfTGO == null) {
//			System.out.println("No TGO selected!!!"); //$NON-NLS-1$
			return;
		}
		IViewPart chainView = null;
		try {
			chainView = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.showView("info.textgrid.lab.workflow.views.ChainView"); //$NON-NLS-1$
		} catch (PartInitException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"View with workflow is not existent", e); //$NON-NLS-1$
			Activator.getDefault().getLog().log(status);
		}

		if (chainView != null && chainView instanceof ChainView) {
			ChainView cvInstance = (ChainView) chainView;
			cvInstance.setChainFromTGWF(wfTGO);
		}
	}

	public void refreshViewer() {
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setQueryMetadata("format:\"text/tg.workflow+xml\""); //$NON-NLS-1$
		viewer.setInput(searchRequest);
	}

	private void startJob() {
		IStructuredSelection wfss = (IStructuredSelection) viewer
				.getSelection();
		TextGridObject wfTGO = (TextGridObject) wfss.getFirstElement();
		if (wfTGO == null)
			return;

		IViewPart inputView = null;
		try {
			inputView = PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.showView("info.textgrid.lab.workflow.views.WorkflowInputs"); //$NON-NLS-1$
		} catch (PartInitException e) {
			IStatus status = new Status(
					IStatus.ERROR,
					Activator.PLUGIN_ID,
					"View with input documents for this workflow is not existent, will be empty workflow batch", //$NON-NLS-1$
					e);
			Activator.getDefault().getLog().log(status);
		}
		ArrayList<ArrayList<TextGridObject>> inputTGOs = new ArrayList<ArrayList<TextGridObject>>();
		if (inputView != null && inputView instanceof WorkflowInputs) {
			WorkflowInputs wfiInstance = (WorkflowInputs) inputView;
			inputTGOs = wfiInstance.getInputs();
		}

		// ArrayList<URI> inputURIs = new ArrayList<URI>();
		// Iterator<TextGridObject> issiter = inputTGOs.iterator();
		// while (issiter.hasNext()) {
		// TextGridObject oneInput = (TextGridObject) issiter.next();
		// inputURIs.add(oneInput.getURI());
		// // System.out.println("Added URI " + oneInput.getURI());
		// }
		// if (inputURIs.size() == 0) {
		// showMessage("No input documents selected.");
		// }

		IStructuredSelection spss = (IStructuredSelection) selectedProjectViewer
				.getSelection();
		String targetProject = null;
		if (spss.getFirstElement() instanceof TextGridProject) {
			TextGridProject targetProjectObject = (TextGridProject) spss
					.getFirstElement();
			targetProject = targetProjectObject.getId();
			if (targetProject == null) {
				showMessage(Messages.ListOfWorkflows_NoTargetProjectSelectedMessage);
				return;
			}
		} else {
			showMessage(Messages.ListOfWorkflows_NoTargetProjectSelectedMessage);
			return;
		}

		if (!ChainGWDL.checkForEqualNumbers(inputTGOs, inputLinks)) {
			showMessage(Messages.ListOfWorkflows_DifferentNumberOfInputsMesssage);
			return;
		}
		WorkflowJob aJob = new WorkflowJob(wfTGO, targetProject, inputTGOs,
				inputLinks);
		aJob.start();
		// aJob will now live a life of its own... good luck to him!

		IViewPart jmView = null;
		try {
			jmView = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.showView("info.textgrid.lab.workflow.views.JobManagement"); //$NON-NLS-1$
		} catch (PartInitException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"Could not open workflow job management view", e); //$NON-NLS-1$
			Activator.getDefault().getLog().log(status);
		}
		if (jmView != null && jmView instanceof JobManagement) {
			JobManagement jmInstance = (JobManagement) jmView;
			jmInstance.refreshJobList();
		}
	}

	private void hookContextMenu(final TableViewer viewer) {
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				ListOfWorkflows.this.fillContextMenu(manager, viewer);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void fillContextMenu(IMenuManager manager, TableViewer viewer) {
		MenuManager openWithSubMenu = new MenuManager(Messages.ListOfWorkflows_openwithmenu);
		Object firstElement = ((IStructuredSelection) viewer.getSelection())
				.getFirstElement();
		if (firstElement == null)
			return;
		IFile iFile = (IFile) ((IAdaptable) firstElement)
				.getAdapter(IFile.class);

		openWithSubMenu.add(new TGOpenWithMenu(getSite().getPage(), iFile));
		// Other plug-ins can contribute their actions here
		manager.add(openWithSubMenu);
		manager.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(),
				Messages.ListOfWorkflows_CommonViewMessageTiitle, message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@Override
	public void dispose() {
		AuthBrowser.removeSIDChangedListener(sidChangedListener);
		if (selectedWorkflowListener != null) {
			getSite().getPage().removeSelectionListener(
					selectedWorkflowListener);
		}
	}

	/**
	 * called in a the UI Thread..., will spawn a Job and update the viewer in
	 * the UI thread once it's loaded
	 * 
	 * @param tgp
	 */
	private void showInputs(final TextGridObject wf) {

		if (currentJob != null) {
			currentJob.cancel();
		}
		currentJob = new Job(Messages.ListOfWorkflows_25) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(Messages.ListOfWorkflows_26,
						IProgressMonitor.UNKNOWN);

				IFile sf = (IFile) wf.getAdapter(IFile.class);
				try {
					Tgwf tgwf = JAXB
							.unmarshal(sf.getContents(true), Tgwf.class);
					monitor.worked(1);
					// check if this is GWDL and not TGWF
					if (tgwf.getActivities() == null) {
						return Status.CANCEL_STATUS;
					}
					if (monitor.isCanceled())
						return Status.CANCEL_STATUS;
					ChainTGWF chainTgwf = new ChainTGWF(tgwf);
					monitor.worked(1);
					if (monitor.isCanceled())
						return Status.CANCEL_STATUS;
					chain = chainTgwf.getChain();
					monitor.worked(1);
					if (monitor.isCanceled())
						return Status.CANCEL_STATUS;
				} catch (CoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				inputLinks = chain.getInputLinks();
				monitor.worked(1);
				if (monitor.isCanceled() || viewer.getTable().isDisposed())
					return Status.CANCEL_STATUS;

				viewer.getTable().getDisplay().asyncExec(new Runnable() {
					public void run() {
						IViewPart inputView = null;
						try {
							inputView = PlatformUI
									.getWorkbench()
									.getActiveWorkbenchWindow()
									.getActivePage()
									.showView(
											Messages.ListOfWorkflows_27);
						} catch (PartInitException e) {
							IStatus status = new Status(
									IStatus.ERROR,
									Activator.PLUGIN_ID,
									Messages.ListOfWorkflows_28,
									e);
							Activator.getDefault().getLog().log(status);
						}

						if (inputView != null
								&& inputView instanceof WorkflowInputs) {
							WorkflowInputs wfiInstance = (WorkflowInputs) inputView;
							wfiInstance.drawInputBoxes(inputLinks);
						}
					}
				});

				if (monitor.isCanceled())
					return Status.CANCEL_STATUS;
				return Status.OK_STATUS;
			}
		};
		currentJob.setPriority(Job.SHORT);
		currentJob.schedule();
	}

}
