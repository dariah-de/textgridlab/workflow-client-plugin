package info.textgrid.lab.workflow.views;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.util.StringToOM;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.NewObjectWizard;
import info.textgrid.lab.workflow.Activator;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.xpath.AXIOMXPath;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class NewWorkflowPreparator implements INewObjectPreparator {
	private ITextGridWizard wizard;

	public static final String NEW_SCHEMA_CONTENTS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<tgwf:tgwf xmlns:tgwf=\"http://textgrid.info/namespaces/middleware/workflow\" version=\"0.5\">\n"
			+ "  <tgwf:description/>\n"
			+ "  <tgwf:activities>\n"
			+ "    <tgwf:service description=\"\" name=\"\" operation=\"\" serviceID=\"\" targetNamespace=\"\" wsdlLocation=\"\" usetns=\"\"/>\n"
			+ "  </tgwf:activities>\n"
			+ "  <tgwf:datalinks>\n"
			+ "    <tgwf:link linkID=\"read\" fromServiceID=\"crud\" fromParam=\"batchinput\" toServiceID=\"\" toParam=\"\"/>\n"
			+ "    <tgwf:link linkID=\"write\" fromServiceID=\"\" fromParam=\"\" toServiceID=\"crud\" toParam=\"batchoutput\"/>\n"
			+ "  </tgwf:datalinks>\n"
			+ "  <tgwf:CRUD instance=\"inserted automatically\" sessionID=\"inserted automatically\" logParameter=\"inserted automatically\"/>\n"
			+ "  <tgwf:batchinput/>\n"
			+ "  <tgwf:metadatatransformation>\n"
			+ "   <xsl:transform xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:ns1=\"http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService\" xmlns:tgomd=\"http://textgrid.info/namespaces/metadata/core/2008-07-24\" version=\"1.0\">\n"
			+ "      <xsl:output method=\"xml\" encoding=\"UTF-8\" indent=\"no\"/>\n"
			+ "      <xsl:template match=\"/\">\n"
			+ "        <ns1:tgObjectMetadata xmlns:ns1=\"http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService\">\n"
			+ "          <xsl:copy-of select=\"/ns1:tgObjectMetadata/tgomd:descriptive\"/>\n"
			+ "          <administrative xmlns=\"http://textgrid.info/namespaces/metadata/core/2008-07-24\">\n"
			+ "            <client>\n"
			+ "              <project id=\"TGPRXXX\"/>\n"
			+ "              <xsl:copy-of select=\"/ns1:tgObjectMetadata/tgomd:administrative/tgomd:client/tgomd:format\"/>\n"
			+ "              <xsl:copy-of select=\"/ns1:tgObjectMetadata/tgomd:administrative/tgomd:client/tgomd:partNo\"/>\n"
			+ "            </client>\n"
			+ "          </administrative>\n"
			+ "          <xsl:copy-of select=\"/ns1:tgObjectMetadata/tgomd:custom\"/>\n"
			+ "        </ns1:tgObjectMetadata>\n"
			+ "      </xsl:template>\n"
			+ "    </xsl:transform>\n"
			+ "  </tgwf:metadatatransformation>\n"
			+ "  <tgwf:inputconstants>\n"
			+ "    <tgwf:activity serviceID=\"\">\n"
			+ "      <tgwf:const name=\"\" needsB64encoding=\"false\">\n"
			+ "      </tgwf:const>\n"
			+ "    </tgwf:activity>\n"
			+ "  </tgwf:inputconstants>\n" + "</tgwf:tgwf>\n";

	public void initializeObject(TextGridObject textGridObject) {

		// pre-fill metadata
		PersonType rightsHolder = RBACSession.getInstance().getPerson();
		textGridObject.setItemMetadata(rightsHolder);
		
//		String currentDate = Calendar.getInstance().get(Calendar.YEAR)
//				+ "-"
//				+ String.format("%02d", (Calendar.getInstance().get(
//						Calendar.MONTH) + 1))
//				+ "-"
//				+ String.format("%02d", Calendar.getInstance().get(
//						Calendar.DAY_OF_MONTH));
//
//		// pre-fill metadata: date fields and type
//		try {
//			OMElement md = textGridObject.getMetadataXML();
//			System.out.println("Die Metadaten bisher: ");
//			System.out.println(md.toString());
//
//			AXIOMXPath adminPath = new AXIOMXPath(
//					"/tg:tgObjectMetadata/tg:administrative");
//			adminPath.addNamespace("tg",
//					TextGridObject.TEXTGRID_METADATA_NAMESPACE);
//
//			OMElement adminOM = (OMElement) adminPath.selectSingleNode(md);
//
//			String descrString = "<tg:descriptive xmlns:tg=\""
//					+ TextGridObject.TEXTGRID_METADATA_NAMESPACE
//					+ "\"><tg:date><approxDate xmlns=\""
//					+ TextGridObject.TEXTGRID_DATERANGE_NAMESPACE
//					+ "\" approximateGregorianDate=\""
//					+ currentDate
//					+ "\">"
//					+ currentDate
//					+ "</approxDate></tg:date><tg:type>workflow</tg:type></tg:descriptive>";
//
//			OMElement descripOMElem = StringToOM.getOMElement(descrString);
//			adminOM.insertSiblingBefore(descripOMElem);
//
//			textGridObject.setMetadataXML(md);
//			
////		} catch (CrudServiceException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		} catch (JaxenException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		} catch (XMLStreamException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
//		} catch (Exception e) {
//			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
//					"Could not pre-set metadata of new workflow object", e);
//			Activator.getDefault().getLog().log(status);
//		}

	}

	public boolean performFinish(TextGridObject textGridObject) {
		try {
			IFile file = AdapterUtils.getAdapter(textGridObject, IFile.class);
			file.setCharset("UTF-8", null);
			textGridObject.setInitialContent(NEW_SCHEMA_CONTENTS
					.getBytes("UTF-8"));
			System.out.println("URI of new TGO:" + textGridObject.getURI());
		} catch (IllegalStateException e) {
			Activator
					.handleProblem(
							IStatus.WARNING,
							e,
							"The TextGrid Workflow object {0} has already been made persistent in this new wizard. Cannot initialize its contents properly.",
							textGridObject);
		} catch (UnsupportedEncodingException e) {
			Activator
					.handleProblem(
							IStatus.ERROR,
							e,
							"UTF-8 is unsupported!? What a strange setup is this? It will be hard to run the lab here ...");
		} catch (CoreException e) {
			Activator
			.handleProblem(
					IStatus.ERROR,
					e,
					"Core Exception during PerformFinish()");

		}

		if (wizard instanceof NewObjectWizard)
			return ((NewObjectWizard) wizard)
					.defaultPerformFinish("info.textgrid.lab.workflow.WorkflowPerspective");
		else
			return false;
	}

	public void setWizard(ITextGridWizard wizard) {
		this.wizard = wizard;

	}

}
