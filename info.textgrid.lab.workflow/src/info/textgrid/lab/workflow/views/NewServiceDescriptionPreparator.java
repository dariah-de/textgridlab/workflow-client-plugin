package info.textgrid.lab.workflow.views;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.util.StringToOM;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.NewObjectWizard;
import info.textgrid.lab.workflow.Activator;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Calendar;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.xpath.AXIOMXPath;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;

public class NewServiceDescriptionPreparator implements INewObjectPreparator {
	private ITextGridWizard wizard;

	public static final String TEMPLATE_PATH = "resources/EmptyService.service";
	private static final String LINESEPARATOR = System.getProperty("line.separator");

	public void initializeObject(TextGridObject textGridObject) {

		// pre-fill metadata
		PersonType rightsHolder = RBACSession.getInstance().getPerson();
		textGridObject.setItemMetadata(rightsHolder);
		
		
//		String currentDate = Calendar.getInstance().get(Calendar.YEAR)
//				+ "-"
//				+ String.format("%02d",
//						(Calendar.getInstance().get(Calendar.MONTH) + 1))
//				+ "-"
//				+ String.format("%02d",
//						Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

		// pre-fill metadata: date fields and type
//		try {
//			OMElement md = textGridObject.getMetadataXML();
//
//			AXIOMXPath adminPath = new AXIOMXPath(
//					"/tg:tgObjectMetadata/tg:administrative");
//			adminPath.addNamespace("tg",
//					TextGridObject.TEXTGRID_METADATA_NAMESPACE);
//
//			OMElement adminOM = (OMElement) adminPath.selectSingleNode(md);
//
//			String descrString = "<tg:descriptive xmlns:tg=\""
//					+ TextGridObject.TEXTGRID_METADATA_NAMESPACE
//					+ "\"><tg:date><approxDate xmlns=\""
//					+ TextGridObject.TEXTGRID_DATERANGE_NAMESPACE
//					+ "\" approximateGregorianDate=\""
//					+ currentDate
//					+ "\">"
//					+ currentDate
//					+ "</approxDate></tg:date><tg:type>servicedescription</tg:type></tg:descriptive>";
//
//			OMElement descripOMElem = StringToOM.getOMElement(descrString);
//			adminOM.insertSiblingBefore(descripOMElem);
//
//			textGridObject.setMetadataXML(md);
//
//		} catch (Exception e) {
//			IStatus status = new Status(
//					IStatus.ERROR,
//					Activator.PLUGIN_ID,
//					"Could not pre-set metadata of new service description object",
//					e);
//			Activator.getDefault().getLog().log(status);
//		}

	}

	public boolean performFinish(TextGridObject textGridObject) {
		try {
			IFile file = AdapterUtils.getAdapter(textGridObject, IFile.class);
			file.setCharset("UTF-8", null);
			
			StringBuffer templateBuffer = new StringBuffer();
			try {
				URL url = FileLocator.find(Activator.getDefault().getBundle(),
						new Path(TEMPLATE_PATH), null);
				url = FileLocator.resolve(url);
				
				BufferedReader in = new BufferedReader( new InputStreamReader( url.openStream()));	
				String s;
				while ((s = in.readLine())!= null) {
					templateBuffer.append(s+LINESEPARATOR);
				}
				in.close();
			} catch (IOException e1) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						"Could not open template file!", e1);
				Activator.getDefault().getLog().log(status);
			}
						
			textGridObject.setInitialContent(templateBuffer.toString().getBytes("UTF-8"));
		} catch (IllegalStateException e) {
			Activator
					.handleProblem(
							IStatus.WARNING,
							e,
							"The TextGrid Service Description object {0} has already been made persistent in this new wizard. Cannot initialize its contents properly.",
							textGridObject);
		} catch (UnsupportedEncodingException e) {
			Activator
					.handleProblem(
							IStatus.ERROR,
							e,
							"UTF-8 is unsupported!? What a strange setup is this? It will be hard to run the lab here ...");
		} catch (CoreException e) {
			Activator
			.handleProblem(
					IStatus.ERROR,
					e,
					"Core Exception during PerformFinish()");

		}

		if (wizard instanceof NewObjectWizard)
			return ((NewObjectWizard) wizard)
					.defaultPerformFinish("info.textgrid.lab.welcome.XMLEditorPerspective");
		else
			return false;
	}

	public void setWizard(ITextGridWizard wizard) {
		this.wizard = wizard;

	}

}
