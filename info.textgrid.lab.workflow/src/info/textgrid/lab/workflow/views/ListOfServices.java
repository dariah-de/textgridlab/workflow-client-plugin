package info.textgrid.lab.workflow.views;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.menus.TGOpenWithMenu;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;
import info.textgrid.lab.workflow.Activator;
import info.textgrid.lab.workflow.approvedlist.Approved;
import info.textgrid.lab.workflow.approvedlist.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.EnumSet;
import java.util.List;

import javax.xml.bind.JAXB;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

/**
 * View for Services: see them listed, search for one (later), add one to a
 * workflow
 * @deprecated, use info.textgrid.lab.workflow.wizard.*
 * @author martin
 */

public class ListOfServices extends ViewPart {
	private TextGridObjectTableViewer allViewer;
	private TableViewer approvedViewer;
	public final String APPROVED_LIST_KEY = "ApprovedServicesTextGridURI"; // ConfServ
	private List<Service> approvedServices;
	private TabFolder folder;
	private TabItem approvedTab;

	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public ListOfServices() {
	}

	class ApprovedServicesLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			Service s;
			if (obj instanceof Service) {
				s = (Service) obj;
			} else {
				return "unknown class: " + obj.getClass();
			}
			if (index == 0) {
				return s.getName();
			} else {
				return "extra column " + index;
			}
		}

		public Image getColumnImage(Object obj, int index) {
			return null;// getImage(obj);
		}

		public Image getImage(Object obj) {
			// return PlatformUI.getWorkbench().
			// getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
			return null;
		}
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite c = new Composite(parent, SWT.NONE);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		c.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		c.setLayout(glp);

		folder = new TabFolder(c, SWT.TOP);
		GridData gf = new GridData(GridData.FILL, GridData.FILL, true, true);
		folder.setLayoutData(gf);
		GridLayout glf = new GridLayout(1, false);
		folder.setLayout(glf);

		Composite approvedComposite = new Composite(folder, SWT.NONE);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, true);
		approvedComposite.setLayoutData(gdstd);
		GridLayout glstd = new GridLayout(1, false);
		approvedComposite.setLayout(glstd);
		approvedTab = new TabItem(folder, SWT.NONE);
		approvedTab.setText("Approved Services");
		approvedTab.setControl(approvedComposite);

		Composite allServicesComposite = new Composite(folder, SWT.NONE);
		GridData gdexp = new GridData(GridData.FILL, GridData.FILL, true, true);
		allServicesComposite.setLayoutData(gdexp);
		GridLayout glexp = new GridLayout(1, false);
		allServicesComposite.setLayout(glexp);
		TabItem expi = new TabItem(folder, SWT.NONE);
		expi.setText("All Services");
		expi.setControl(allServicesComposite);

		approvedViewer = new TableViewer(approvedComposite, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		approvedViewer.getTable().setHeaderVisible(false);
		approvedViewer.setContentProvider(new ArrayContentProvider());
		approvedViewer.setLabelProvider(new ApprovedServicesLabelProvider());

		GridData gd0 = new GridData(SWT.FILL, SWT.FILL, true, true);
		approvedViewer.getControl().setLayoutData(gd0);
		approvedViewer.setSorter(new NameSorter());
		boolean foundApproved = fillApprovedViewer();
		if (foundApproved) {
			getSite().setSelectionProvider(approvedViewer);
			// makeActions();
			hookContextMenu(approvedViewer);
			// hookDoubleClickAction();
		} else {
			approvedComposite.dispose();
			approvedTab.dispose();
		}


		allViewer = new TextGridObjectTableViewer(allServicesComposite, SWT.SINGLE
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		allViewer.setVisibleColumns(EnumSet.of(Column.TITLE));
		allViewer.getTable().setHeaderVisible(false);

		GridData gd1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		allViewer.getControl().setLayoutData(gd1);
		allViewer.setSorter(new NameSorter());
		refreshViewer();
		getSite().setSelectionProvider(allViewer);

		// makeActions();
		hookContextMenu(allViewer);
		// hookDoubleClickAction();

		Composite aboveBottomBar = new Composite(c, SWT.FILL);
		aboveBottomBar.setLayout(new GridLayout(2, false));
		GridData gd2 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		aboveBottomBar.setLayoutData(gd2);
		// GridDataFactory.fillDefaults().applyTo(underbottomBar);

		GridData gd3b = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd3b.widthHint = 2000;

		Composite bottomBar = new Composite(c, SWT.FILL);
		bottomBar.setLayout(new GridLayout(2, false));
		GridData gd4 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		bottomBar.setLayoutData(gd4);

		Button addToChainButton = new Button(bottomBar, SWT.PUSH);
		addToChainButton.setText("Add to workflow");
		addToChainButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				addToChain();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

	}

	private boolean fillApprovedViewer() {
		try {
			ConfClient confClient = ConfClient.getInstance();
			String approvedURIstring = confClient.getValue(APPROVED_LIST_KEY);
			if (approvedURIstring == null || approvedURIstring.equals("")) {
				Activator
						.handleProblem(IStatus.WARNING, new Throwable(),
								"Workflow: Could not find TextGridObject holding the approved services list!");
				return false;
			}
			URI approvedURI = new URI(approvedURIstring);
			TextGridObject listtgo = TextGridObject.getInstance(approvedURI,
					false);
			IFile sf = (IFile) listtgo.getAdapter(IFile.class);
			Approved approvedlist = JAXB.unmarshal(sf.getContents(true),
					Approved.class);
			approvedServices = approvedlist.getService();
		} catch (CrudServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (URISyntaxException e) {
			Activator.handleProblem(IStatus.ERROR, e,
					"Can't ascertain TG-URI.", e);
			return false;
		} catch (OfflineException e) {
			OnlineStatus
					.netAccessFailed(
							"Workflow: Could not contact Confserver for Approved Services List location!",
							e);
			return false;
		}
		approvedViewer.setInput(approvedServices);
		return true;
	}

	private void refreshViewer() {
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setAllProjects(true);
		searchRequest
				.setQueryMetadata("format:\"text/tg.servicedescription+xml\"");
		allViewer.setInput(searchRequest);
	}

	private void addToChain() {
		TabItem[] tiarr = folder.getSelection();
		System.out.println("Adding one service from the " + tiarr[0].getText()
				+ " tab's list to the workflow");
		TextGridObject serviceTGO = null;

		if (tiarr[0].equals(approvedTab)) {
			IStructuredSelection wfss = (IStructuredSelection) approvedViewer
					.getSelection();
			Service selService = (Service) wfss.getFirstElement();
			if (selService == null)
				return;
			try {
				serviceTGO = TextGridObject.getInstance(
						URI.create(selService.getUri()), false);
			} catch (CrudServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			IStructuredSelection wfss = (IStructuredSelection) allViewer
					.getSelection();
			serviceTGO = (TextGridObject) wfss.getFirstElement();
			if (serviceTGO == null)
				return;
		}

		IViewPart chainView = null;
		try {
			chainView = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage()
					.showView("info.textgrid.lab.workflow.views.ChainView");
		} catch (PartInitException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					"View with workflow is not existent", e);
			Activator.getDefault().getLog().log(status);
		}

		if (chainView != null && chainView instanceof ChainView) {
			ChainView cvInstance = (ChainView) chainView;
			cvInstance.addService(serviceTGO);
		}
	}

	private void hookContextMenu(final TableViewer viewer) {
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				ListOfServices.this.fillContextMenu(manager, viewer);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void fillContextMenu(IMenuManager manager, TableViewer viewer) {
		MenuManager openWithSubMenu = new MenuManager("Open with...");
		Object firstElement = ((IStructuredSelection) viewer.getSelection())
				.getFirstElement();
		if (firstElement == null)
			return;
		IFile iFile = (IFile) ((IAdaptable) firstElement)
				.getAdapter(IFile.class);

		openWithSubMenu.add(new TGOpenWithMenu(getSite().getPage(), iFile));
		// Other plug-ins can contribute their actions here
		manager.add(openWithSubMenu);
		manager.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void hookDoubleClickAction(TableViewer viewer) {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				// doubleClickAction.run();
			}
		});
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(allViewer.getControl().getShell(),
				"Service List", message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		allViewer.getControl().setFocus();
	}

	@Override
	public void dispose() {
	}

}
