package info.textgrid.lab.workflow.views;

import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.ChainEntry;
import info.textgrid.lab.workflow.EntryLink;
import info.textgrid.lab.workflow.EntryLinkFromComparator;
import info.textgrid.lab.workflow.servicedescription.Configparameter;
import info.textgrid.lab.workflow.servicedescription.Examplevalue;
import info.textgrid.lab.workflow.servicedescription.Input;
import info.textgrid.lab.workflow.servicedescription.Output;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

/**
 * View for configuration and linking of a single service
 * @deprecated, use info.textgrid.lab.workflow.wizard.*
 * @author martin
 */

public class ChainEntryView extends ViewPart {
	private Chain chain;
	private Group rahmen;
	private Group descGroup;
	private Group inputGroup;
	private Group outputGroup;
	private Group configGroup;
	private final ISelectionListener selectedChainEntryListener;
	private ChainEntry currentChainEntry = null;
	private ArrayList<Widget> reminder = new ArrayList<Widget>();
	private ChainView cvInstance;

	/**
	 * The constructor.
	 */
	public ChainEntryView() {
		selectedChainEntryListener = new ISelectionListener() {
			public void selectionChanged(IWorkbenchPart sourcepart,
					ISelection selection) {
				if (sourcepart != ChainEntryView.this
						&& selection instanceof IStructuredSelection) {
					final IStructuredSelection iss = (IStructuredSelection) selection;
					// take first selected element
					if (!iss.isEmpty()) {
						Object firstelem = iss.toArray()[0];
						if (firstelem instanceof ChainEntry) {
							currentChainEntry = (ChainEntry) firstelem;
						} else {
							currentChainEntry = null;
						}
						chain = cvInstance.getChain();
						setChainEntry(currentChainEntry);
					}
				}
			}
		};
	}

	class LinkLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			EntryLink l;
			if (obj instanceof EntryLink) {
				l = (EntryLink) obj;
				if (l.getFrom() == null || l.getFromPort() == null) {
					return "Workflow Inputs";
				} else {
					return l.getFrom().getName() + ", "
							+ l.getFromPort().getName();
				}
			} else {
				return "unknown class: " + obj.getClass();
			}
		}
	}

	class LinkOutLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			EntryLink l;
			if (obj instanceof EntryLink) {
				l = (EntryLink) obj;
				if (l.getTo() == null || l.getToPort() == null) {
					return "Workflow Outputs";
				} else {
					return l.getTo().getName() + ", " + l.getToPort().getName();
				}
			} else {
				return "unknown class: " + obj.getClass();
			}
		}
	}

	class ConfigLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			Examplevalue ex;
			if (obj instanceof Examplevalue) {
				ex = (Examplevalue) obj;
				return ex.getName();
			} else {
				return "unknown class: " + obj.getClass();
			}
		}
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		rahmen = new Group(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		rahmen.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		rahmen.setLayout(glp);
		
		descGroup = new Group(rahmen, SWT.H_SCROLL | SWT.V_SCROLL);
		descGroup.setText("Description");
		GridData gdcd = new GridData(GridData.FILL, GridData.FILL, true, true);
		descGroup.setLayoutData(gdcd);
		GridLayout glpd = new GridLayout(1, false);
		descGroup.setLayout(glpd);
		
		inputGroup = new Group(rahmen, SWT.H_SCROLL | SWT.V_SCROLL);
		inputGroup.setText("This service's input data, select data sources");
		GridData gdci = new GridData(GridData.FILL, GridData.FILL, true, true);
		inputGroup.setLayoutData(gdci);
		GridLayout glpi = new GridLayout(1, false);
		inputGroup.setLayout(glpi);

		outputGroup = new Group(rahmen, SWT.H_SCROLL | SWT.V_SCROLL);
		outputGroup.setText("This Service's output data, select data targets");
		GridData gdco = new GridData(GridData.FILL, GridData.FILL, true, true);
		outputGroup.setLayoutData(gdco);
		GridLayout glpo = new GridLayout(1, false);
		outputGroup.setLayout(glpo);

		configGroup = new Group(rahmen, SWT.NONE);
		configGroup.setText("Configuration Options");
		GridData gdcc = new GridData(GridData.FILL, GridData.FILL, true, true);
		configGroup.setLayoutData(gdcc);
		GridLayout glpc = new GridLayout(1, false);
		configGroup.setLayout(glpc);

		IViewPart chainView = null;

		chainView = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage()
				.findView("info.textgrid.lab.workflow.views.ChainView");

		if (chainView != null && chainView instanceof ChainView) {
			cvInstance = (ChainView) chainView;
		}
		getSite().getPage().addSelectionListener(selectedChainEntryListener);
	}

	public void setChainEntry(ChainEntry e) {
		for (Widget old : reminder) {
			old.dispose();
		}
		reminder.clear();

		if (e == null) {
			rahmen.setText("Please select a service from the workflow");
			return;
		}
		rahmen.setText(e.getName());
		currentChainEntry = e;
		Label descriptionText = new Label (descGroup, SWT.WRAP);
		GridData gdll = new GridData(GridData.FILL, GridData.FILL, true, true);
		descriptionText.setLayoutData(gdll);
		descriptionText.setText(e.getDescription());
		descriptionText.setBounds(descGroup.getBounds());
		reminder.add(descriptionText);
		
		for (Input i : e.getInputs()) {
			final Composite c = new Composite(inputGroup, SWT.NONE);
			reminder.add(c);
			c.setLayout(new GridLayout(3, false));
			GridData gdc = new GridData(GridData.FILL, GridData.FILL, true,
					true);
			c.setLayoutData(gdc);

			Label t = new Label(c, SWT.SINGLE);
			t.setText(i.getName() + ": from ");

			List<EntryLink> linksToHere = chain.findLinksToHere(e, i);

			final LinkLabelProvider clp = new LinkLabelProvider();
			// check if there is only one possibility. If so, add link at
			// once
			// and do not show the list/button
			if (linksToHere.size() == 1) {
				EntryLink solitaire = linksToHere.get(0);
				Label from = new Label(c, SWT.SINGLE);
				from.setText(clp.getText(solitaire));
				if (solitaire.isUndecided()) { // only add link if it had'nt
												// been added before yet
					chain.addLink(solitaire);
				}
			} else {
				final ComboViewer cv = new ComboViewer(c, SWT.SINGLE);
				cv.setLabelProvider(clp);
				cv.setContentProvider(new ArrayContentProvider());
				cv.setComparator(new EntryLinkFromComparator());
				cv.setInput(linksToHere);
				// check if reminder is needed?

				final Button setB = new Button(c, SWT.PUSH);
				setB.setText("Set");
				setB.addSelectionListener(new SelectionListener() {
					public void widgetSelected(SelectionEvent event) {
						IStructuredSelection spss = (IStructuredSelection) cv
								.getSelection();
						EntryLink selectedLink = null;
						if (spss.getFirstElement() instanceof EntryLink) {
							selectedLink = (EntryLink) spss.getFirstElement();
							if (selectedLink != null) {
								boolean success = chain.addLink(selectedLink);
								if (success) {
									// this loses all other configurations not
									// made permanent yet...
									// setChainEntry(surroundingEntry);
									// ...so better:
									cv.getControl().dispose();
									setB.dispose();
									Label from = new Label(c, SWT.SINGLE);
									from.setText(clp.getText(selectedLink));
									c.layout();
								} else {
									System.out
											.println("Could not add link, there were conflicts.");
								}
							} else {
								System.out.println("no incoming link selected");
							}
						} else {
							System.out.println("no incoming link selected");
						}
					}

					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);
					}
				});
				// TODO check if button reminder is needed?
			}
		}

		for (Output o : e.getOutputs()) {
			final Composite c = new Composite(outputGroup, SWT.NONE);
			reminder.add(c);
			c.setLayout(new GridLayout(3, false));
			GridData gdc = new GridData(GridData.FILL, GridData.FILL, true,
					true);
			c.setLayoutData(gdc);

			Label t = new Label(c, SWT.SINGLE);
			t.setText(o.getName() + ": to ");

			List<EntryLink> linksFromHere = chain.findLinksFromHere(e, o);

			final LinkOutLabelProvider clp = new LinkOutLabelProvider();
			// check if there is only one possibility. If so, add link at
			// once
			// and do not show the list/button
			if (linksFromHere.size() == 1) {
				EntryLink solitaire = linksFromHere.get(0);
				Label to = new Label(c, SWT.SINGLE);
				to.setText(clp.getText(solitaire));
				if (solitaire.isUndecided()) { // only add link if it had'nt
												// been added before yet
					// chain.addLink(solitaire); dont do this, as we might be in
					// construction phase yet and this output could be connected
					// to some next input

				}
			} else {
				final ComboViewer cv = new ComboViewer(c, SWT.SINGLE);
				cv.setLabelProvider(clp);
				cv.setContentProvider(new ArrayContentProvider());
				// cv.setComparator(new EntryLinkFromComparator());
				cv.setInput(linksFromHere);
				// check if reminder is needed?

				final Button setB = new Button(c, SWT.PUSH);
				setB.setText("Set");
				setB.addSelectionListener(new SelectionListener() {
					public void widgetSelected(SelectionEvent event) {
						IStructuredSelection spss = (IStructuredSelection) cv
								.getSelection();
						EntryLink selectedLink = null;
						if (spss.getFirstElement() instanceof EntryLink) {
							selectedLink = (EntryLink) spss.getFirstElement();
							if (selectedLink != null) {
								boolean success = chain.addLink(selectedLink);
								if (success) {
									// this loses all other configurations not
									// made permanent yet...
									// setChainEntry(surroundingEntry);
									// ...so better:
									cv.getControl().dispose();
									setB.dispose();
									Label to = new Label(c, SWT.SINGLE);
									to.setText(clp.getText(selectedLink));
									c.layout();
								} else {
									System.out
											.println("Could not add link, there were conflicts.");
								}
							} else {
								System.out.println("no incoming link selected");
							}
						} else {
							System.out.println("no incoming link selected");
						}
					}

					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);
					}
				});
				// TODO check if button reminder is needed?
			}
		}

		for (Configparameter cp : e.getConfigParameters()) {
			Composite c = new Composite(configGroup, SWT.NONE);
			reminder.add(c);

			final String thisConfigParam = cp.getParam();
			final String thisConfigParamName = cp.getName();

			c.setLayout(new GridLayout(cp.isMultiple() ? 6 : 4, false));
			GridData gdc = new GridData(GridData.FILL, GridData.FILL, true,
					true);
			c.setLayoutData(gdc);

			ArrayList<Examplevalue> exValues = e
					.getChosenConfigParamExampleValues(cp.getParam());

			if (exValues.size() == 0) {
				// we need a possiblity to add param values even if no value is
				// set

				Label t = new Label(c, SWT.SINGLE);
				t.setText(cp.getName() + ": (no value set)");

				Button addB = new Button(c, SWT.PUSH);
				addB.setText("Add...");
				addB.addSelectionListener(new SelectionListener() {
					public void widgetSelected(SelectionEvent event) {
						currentChainEntry
								.addDefaultExampleValue(thisConfigParam);
						setChainEntry(currentChainEntry);
					}

					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);
					}
				});
				break;
			}

			for (int i = 0; i < exValues.size(); i++) {
				if (i == 0) {
					Label t = new Label(c, SWT.SINGLE);
					t.setText(cp.getName() + ": ");
				} else {
					Label dummy = new Label(c, SWT.SINGLE);
					dummy.setText("");
				}

				final ComboViewer cv = new ComboViewer(c, SWT.SINGLE);
				cv.setLabelProvider(new ConfigLabelProvider());
				cv.setContentProvider(new ArrayContentProvider());
				cv.setComparator(new EntryLinkFromComparator());
				cv.setInput(cp.getExamplevalue());

				cv.setSelection(new StructuredSelection(exValues.get(i)), true);

				final int fi = i;

				Button setB = new Button(c, SWT.PUSH);
				setB.setText("Set");
				final Button editB = new Button(c, SWT.PUSH);
				editB.setText("Edit...");
				if (cp.isMultiple()) {
					Button addB = new Button(c, SWT.PUSH);
					addB.setText("Add...");
					addB.addSelectionListener(new SelectionListener() {
						public void widgetSelected(SelectionEvent event) {
							currentChainEntry
									.addDefaultExampleValue(thisConfigParam);
							setChainEntry(currentChainEntry);
						}

						public void widgetDefaultSelected(SelectionEvent e) {
							widgetSelected(e);
						}
					});
					Button delB = new Button(c, SWT.PUSH);
					delB.setText("Delete");
					delB.addSelectionListener(new SelectionListener() {
						public void widgetSelected(SelectionEvent event) {
							currentChainEntry.deleteExampleValue(
									thisConfigParam, fi);
							setChainEntry(currentChainEntry);
						}

						public void widgetDefaultSelected(SelectionEvent e) {
							widgetSelected(e);
						}
					});

				}
				editB.addSelectionListener(new SelectionListener() {
					public void widgetSelected(SelectionEvent event) {

						// assume some value has already been set, however...
						Examplevalue selectedExValue = currentChainEntry
								.getChosenConfigParamExampleValues(
										thisConfigParam).get(fi);
						// ...look if the selection has been changed and update
						// on-the-fly
						IStructuredSelection spss = (IStructuredSelection) cv
								.getSelection();
						if (spss.getFirstElement() instanceof Examplevalue) {
							selectedExValue = (Examplevalue) spss
									.getFirstElement();
						}

						String orig = ChainEntry.anyToString(selectedExValue
								.getContent());
						ConfigParameterEditingDialogue cped = new ConfigParameterEditingDialogue(
								PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow().getShell());
						cped.setContents(thisConfigParamName, orig,
								currentChainEntry, thisConfigParam,
								selectedExValue.isInline(), fi);
						cped.setBlockOnOpen(true);
						cped.open();

						// ...on-the-fly! (on the way back only if save had been
						// pressed)
						cv.setInput(currentChainEntry
								.getAllAvailableExampleValues(thisConfigParam));
						cv.setSelection(
								new StructuredSelection(currentChainEntry
										.getChosenConfigParamExampleValues(
												thisConfigParam).get(fi)), true);
					}

					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);
					}
				});

				setB.addSelectionListener(new SelectionListener() {
					public void widgetSelected(SelectionEvent event) {
						IStructuredSelection spss = (IStructuredSelection) cv
								.getSelection();
						Examplevalue selectedID = null;
						if (spss.getFirstElement() instanceof Examplevalue) {
							selectedID = (Examplevalue) spss.getFirstElement();
							if (selectedID != null) {
								currentChainEntry.selectExampleValue(
										thisConfigParam, selectedID, fi);
								editB.setEnabled(true);
							} else {
								System.out.println("no example value selected");
							}
						} else {
							System.out.println("no example value selected");
						}
					}

					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);
					}
				});
			}
		}
		rahmen.layout(true, true);
	}

	@Override
	public void setFocus() {
		// TODO NPE when called from "Configure Workflow in ChainView
		// rahmen.setFocus();
	}

	@Override
	public void dispose() {
		if (selectedChainEntryListener != null) {
			getSite().getPage().removeSelectionListener(
					selectedChainEntryListener);
		}

	}

}
