package info.textgrid.lab.workflow.views;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


/**
 * Implements the Document perspective.
 */
public class WorkflowPerspective implements IPerspectiveFactory {
    
    public void createInitialLayout(IPageLayout layout) {
    	defineActions(layout);
    	defineLayout(layout);
    }
    
    /**
     * Defines the initial actions for a page.  
     */
    public void defineActions(IPageLayout layout) {
    	// Add "new wizards".
//    	layout.addNewWizardShortcut("net.sf.vex.editor.NewDocumentWizard");//$NON-NLS-1$
//    	layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.folder");//$NON-NLS-1$
//    	layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.file");//$NON-NLS-1$
//
//    	layout.addShowViewShortcut(IPageLayout.ID_RES_NAV);
//    	layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
//    	layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);

//    	layout.addActionSet(IPageLayout.ID_NAVIGATE_ACTION_SET);
    }
    
    /**
     * Defines the initial layout for a page.  
     */
    public void defineLayout(IPageLayout layout) {

    	String editorArea = layout.getEditorArea();
       	
     	IFolderLayout left = 
    		layout.createFolder("left", IPageLayout.LEFT, (float)0.3, editorArea);//$NON-NLS-1$
       	left.addView("info.textgrid.lab.navigator.view");
       	left.addView("info.textgrid.lab.search.views.ResultView");

    	IFolderLayout bottomMiddle =
    		layout.createFolder("bottomMiddle", IPageLayout.BOTTOM, (float)0.5, editorArea);//$NON-NLS-1$
       	bottomMiddle.addView("info.textgrid.lab.workflow.views.WorkflowSelection");

    	IFolderLayout topMiddle =
    		layout.createFolder("topMiddle", IPageLayout.TOP, (float)0.5, "bottomMiddle");//$NON-NLS-1$
       	topMiddle.addView("info.textgrid.lab.workflow.views.WorkflowInputs");
 	
    	IFolderLayout bottomRight= 
    		layout.createFolder("bottomRight", IPageLayout.RIGHT, (float)0.5, "bottomMiddle");//$NON-NLS-1$
       	bottomRight.addView("info.textgrid.lab.workflow.views.JobManagement");

       	IFolderLayout topRight= 
    		layout.createFolder("topRight", IPageLayout.RIGHT, (float)0.7, "topMiddle");//$NON-NLS-1$
       	topRight.addView("info.textgrid.lab.workflow.views.WorkflowResults");
    	
       	layout.setEditorAreaVisible(false);   

       
    }

}
