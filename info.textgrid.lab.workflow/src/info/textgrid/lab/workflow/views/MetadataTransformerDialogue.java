package info.textgrid.lab.workflow.views;

import info.textgrid.lab.workflow.MetadataTransformer;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class MetadataTransformerDialogue extends TrayDialog {
	MetadataTransformerDialogue thisDialog;
	Group frame;
	MetadataTransformer transformer;
	ArrayList<Label> labels = new ArrayList<Label>();
	ArrayList<Text> values = new ArrayList<Text>();

	protected MetadataTransformerDialogue(IShellProvider parentShell) {
		super(parentShell);
		initialize();// parentShell.getShell());
	}

	public MetadataTransformerDialogue(Shell parentShell) {
		super(parentShell);
		initialize();
	}

	protected void initialize() {
		thisDialog = this;
	}

	protected void initialize(Composite parent) {
	}

	protected void destroy() {
		thisDialog.close();
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		createButton(parent, IDialogConstants.FINISH_ID,
				IDialogConstants.FINISH_LABEL, true);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CANCEL_ID) {
			this.destroy();
		}
		if (buttonId == IDialogConstants.FINISH_ID) {
			readAndStoreParams();
			this.destroy();
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite c = new Composite(parent, SWT.NONE);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		c.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		c.setLayout(glp);

		frame = new Group(c, SWT.NONE);
		GridData gdf = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdf.widthHint = 500;
		gdf.heightHint = 300;
		frame.setLayoutData(gdf);
		GridLayout glpf = new GridLayout(2, false);
		frame.setLayout(glpf);
		frame.setText(MessageFormat.format(Messages.MetadataTransformerDialogue_EditMetadataForText, transformer
				.getOutput().getFromPort().getName()));

		for (String key : transformer.getParams().keySet()) {
			Label keyLabel = new Label(frame, SWT.NONE);
			keyLabel.setText(key);
			labels.add(keyLabel);

			Text valueText = new Text(frame, SWT.SINGLE | SWT.BORDER);
			GridData gdValueText = new GridData(GridData.FILL, GridData.CENTER,
					true, false);
			valueText.setLayoutData(gdValueText);
			String value = transformer.getParams().get(key);
			valueText.setText(value == null ? "" : value); //$NON-NLS-1$
			values.add(valueText);
		}

		return c;
	}

	private void readAndStoreParams() {
		// assume order remains the same for labels and values
		Iterator<Text> valueTexts = values.iterator();
		for (Label l : labels) {
			Text t = valueTexts.next();
			String v = t.getText();
			if (v.length() < 1 || v.matches("\\s+")) { //$NON-NLS-1$
				v = null;
			}
			transformer.getParams().put(l.getText(), v);
		}
	}

	public void setContents(MetadataTransformer mdt) {
		transformer = mdt;
	}

}
