package info.textgrid.lab.workflow.views;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.menus.TGOpenWithMenu;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;
import info.textgrid.lab.workflow.WorkflowJob;

import java.util.ArrayList;
import java.util.EnumSet;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.part.ViewPart;

public class WorkflowResults extends ViewPart {
	private TextGridObjectTableViewer viewer;

	@Override
	public void createPartControl(Composite parent) {
		Composite c = new Composite(parent, SWT.FILL);
		c.setLayout(new GridLayout(1, false));
		GridData cGD = new GridData();
		cGD.horizontalAlignment = GridData.FILL;
		cGD.grabExcessVerticalSpace = true;
		cGD.grabExcessHorizontalSpace = true;
		// cGD.heightHint = 100;
		// cGD.widthHint = 400;
		cGD.verticalAlignment = GridData.FILL;
		c.setLayoutData(cGD);

		viewer = new TextGridObjectTableViewer(c, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION);
		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setVisibleColumns(EnumSet.of(Column.TITLE));
		viewer.getTable().setHeaderVisible(false);
		getSite().setSelectionProvider(viewer);
		hookContextMenu();

		GridData vGD = new GridData();
		vGD.horizontalAlignment = GridData.FILL;
		vGD.grabExcessVerticalSpace = true;
		vGD.grabExcessHorizontalSpace = true;
		// vGD.heightHint = 100;
		// vGD.widthHint = 400;
		vGD.verticalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(vGD);

	}

	public void refreshViewer(WorkflowJob job) {
		if (job == null) {
			viewer.setInput(new ArrayList<TextGridObject>());
		} else {
			// System.out.println("Displaying outputs for Job " +
			// job.toString());
			viewer.setInput(job.getResultTGOs().toArray());
		}
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				WorkflowResults.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void fillContextMenu(IMenuManager manager) {
		MenuManager openWithSubMenu = new MenuManager(Messages.WorkflowResults_openwith);
		Object firstElement = ((IStructuredSelection) viewer.getSelection())
				.getFirstElement();

		if (firstElement == null)
			return;
		IFile iFile = (IFile) ((IAdaptable) firstElement)
				.getAdapter(IFile.class);

		openWithSubMenu.add(new TGOpenWithMenu(getSite().getPage(), iFile));
		// Other plug-ins can contribute their actions here
		manager.add(openWithSubMenu);
		manager.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}

}
