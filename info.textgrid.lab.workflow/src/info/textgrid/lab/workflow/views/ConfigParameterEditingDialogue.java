package info.textgrid.lab.workflow.views;

import info.textgrid.lab.workflow.ChainEntry;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ConfigParameterEditingDialogue extends TrayDialog {
	ConfigParameterEditingDialogue thisDialog;
	Group frame;
	Text contents;
	String heading = ""; //$NON-NLS-1$
	String initialContents = ""; //$NON-NLS-1$
	ChainEntry ce;
	String param;
	boolean inline;
	int position;
	
	protected ConfigParameterEditingDialogue(IShellProvider parentShell) {
		super(parentShell);
		initialize();// parentShell.getShell());
	}

	public ConfigParameterEditingDialogue(Shell parentShell) {
		super(parentShell);
		initialize();
	}

	protected void initialize() {
		thisDialog = this;
	}

	protected void initialize(Composite parent) {
	}

	protected void destroy() {
		thisDialog.close();
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		createButton(parent, IDialogConstants.FINISH_ID, Messages.ConfigParameterEditingDialogue_savebutton, true);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
		if (buttonId == IDialogConstants.CANCEL_ID) {
			this.destroy();
		}
		if (buttonId == IDialogConstants.FINISH_ID) {
			String text = contents.getText();
			if (ce.needsB64encoding(param) && inline) {
				try {
					text = new String(Base64.encodeBase64(text.getBytes("UTF-8"))); //$NON-NLS-1$
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			ce.setNewConfigValue(param, inline, position, text);
			this.destroy();
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite c = new Composite(parent, SWT.NONE);
		GridData gdc = new GridData(GridData.FILL, GridData.FILL, true, true);
		c.setLayoutData(gdc);
		GridLayout glp = new GridLayout(1, false);
		c.setLayout(glp);

		frame = new Group(c, SWT.NONE);
		GridData gdf = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdf.widthHint = 500;
		gdf.heightHint = 300;
		frame.setLayoutData(gdf);
		GridLayout glpf = new GridLayout(1, false);
		frame.setLayout(glpf);
		frame.setText(heading);

		contents = new Text(frame, SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL);
		GridData gdt = new GridData(GridData.FILL, GridData.FILL, true, true);
		gdt.widthHint = 500;
		gdt.heightHint = 300;
		contents.setLayoutData(gdt);
		contents.setText(initialContents);

		return c;
	}

	public void setContents(String heading, String initialContents,
			ChainEntry ce, String param, boolean inline, int position) {
		this.heading = heading;
		// have to re-convert in order to be able to edit as non-b64
		if (ce.needsB64encoding(param) && inline) {
			try {
				initialContents = new String(Base64.decodeBase64(initialContents
						.getBytes("UTF-8"))); //$NON-NLS-1$
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.initialContents = initialContents;
		this.ce = ce;
		this.param = param;
		this.inline = inline;
		this.position = position;
	}

}
