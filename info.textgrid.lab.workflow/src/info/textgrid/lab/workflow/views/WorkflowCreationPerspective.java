package info.textgrid.lab.workflow.views;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


/**
 * Implements the Document perspective.
 * @deprecated, use info.textgrid.lab.workflow.wizard.*
 */
public class WorkflowCreationPerspective implements IPerspectiveFactory {
    
    public void createInitialLayout(IPageLayout layout) {
    	defineActions(layout);
    	defineLayout(layout);
    }
    
    /**
     * Defines the initial actions for a page.  
     */
    public void defineActions(IPageLayout layout) {
    }
    
    /**
     * Defines the initial layout for a page.  
     */
    public void defineLayout(IPageLayout layout) {

    	String editorArea = layout.getEditorArea();
       	
     	IFolderLayout left = 
    		layout.createFolder("left", IPageLayout.LEFT, (float)0.3, editorArea);//$NON-NLS-1$
       	left.addView("info.textgrid.lab.workflow.views.ServiceList");

       	IFolderLayout middleBottom = 
    		layout.createFolder("rightBottom", IPageLayout.BOTTOM, (float)0.75, editorArea);//$NON-NLS-1$
       	middleBottom.addView("info.textgrid.lab.workflow.views.MetadataTransform");

       	IFolderLayout middle =
    		layout.createFolder("middle", IPageLayout.LEFT, (float)0.3, editorArea);//$NON-NLS-1$
       	middle.addView("info.textgrid.lab.workflow.views.ChainView");

       	IFolderLayout right= 
    		layout.createFolder("righttop", IPageLayout.RIGHT, (float)0.1, editorArea);//$NON-NLS-1$
       	right.addView("info.textgrid.lab.workflow.views.ChainEntryView");
  	
       	
       	layout.setEditorAreaVisible(false);   

       
    }

}
