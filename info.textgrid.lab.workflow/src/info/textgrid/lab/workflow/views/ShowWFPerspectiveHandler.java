package info.textgrid.lab.workflow.views;

import info.textgrid.lab.workflow.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

public class ShowWFPerspectiveHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			// first reset the perspective...
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchPage wbPage = wb.getActiveWorkbenchWindow().getActivePage();
			if (wbPage == null) 
				wb.getActiveWorkbenchWindow().openPage(
						"info.textgrid.lab.workflow.WorkflowPerspective", null);
			wbPage.setPerspective(
					wb.getPerspectiveRegistry().findPerspectiveWithId(
							"info.textgrid.lab.workflow.WorkflowPerspective"));
			wb.getActiveWorkbenchWindow().getActivePage().resetPerspective();

			// show it when necessary
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.workflow.WorkflowPerspective",
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());
			wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());
		} catch (WorkbenchException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				    "Could not open Workflow Perspective!", e);
			Activator.getDefault().getLog().log(status);
		}
		return null;
	}
}
