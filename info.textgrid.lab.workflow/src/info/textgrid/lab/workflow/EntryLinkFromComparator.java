package info.textgrid.lab.workflow;

import java.util.Comparator;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

public class EntryLinkFromComparator extends ViewerComparator implements
		Comparator<EntryLink> {
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		if (e1 instanceof EntryLink && e2 instanceof EntryLink) {
			return this.compare((EntryLink) e1, (EntryLink) e2);
		} else {
			return 0;
		}
	}

	@Override
	public int compare(EntryLink o1, EntryLink o2) {
		// TODO Auto-generated method stub
		if (o1.getFrom() == null && o2.getFrom() != null) {
			return 1;
		} else if (o1.getFrom() != null && o2.getFrom() == null) {
			return -1;
		} else if (o1.getFrom() == null && o2.getFrom() == null) {
			return 0;
		} else if (o1.getFrom().getID() == o2.getFrom().getID()) {
			return o1.getFromPort().getName()
					.compareTo(o2.getFromPort().getName());
		} else {
			return o2.getFrom().getID() - o1.getFrom().getID();
		}
	}
}
