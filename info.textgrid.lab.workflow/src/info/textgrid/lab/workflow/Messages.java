package info.textgrid.lab.workflow;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.workflow.messages"; //$NON-NLS-1$
	public static String WorkflowEngine_0;
	public static String WorkflowEngine_1;
	public static String WorkflowEngine_2;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
