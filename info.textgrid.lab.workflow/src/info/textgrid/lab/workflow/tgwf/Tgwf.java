//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.01.12 at 11:37:50 AM MEZ 
//


package info.textgrid.lab.workflow.tgwf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://textgrid.info/namespaces/middleware/workflow/tgwf/2011/01}description"/>
 *         &lt;element ref="{http://textgrid.info/namespaces/middleware/workflow/tgwf/2011/01}activities"/>
 *         &lt;element ref="{http://textgrid.info/namespaces/middleware/workflow/tgwf/2011/01}datalinks"/>
 *         &lt;element ref="{http://textgrid.info/namespaces/middleware/workflow/tgwf/2011/01}CRUD"/>
 *         &lt;element ref="{http://textgrid.info/namespaces/middleware/workflow/tgwf/2011/01}inputs"/>
 *         &lt;element ref="{http://textgrid.info/namespaces/middleware/workflow/tgwf/2011/01}metadatatransformation"/>
 *         &lt;element ref="{http://textgrid.info/namespaces/middleware/workflow/tgwf/2011/01}configparameters"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "description",
    "activities",
    "datalinks",
    "crud",
    "inputs",
    "metadatatransformation",
    "configparameters"
})
@XmlRootElement(name = "tgwf")
public class Tgwf {

    @XmlElement(required = true)
    protected String description;
    @XmlElement(required = true)
    protected Activities activities;
    @XmlElement(required = true)
    protected Datalinks datalinks;
    @XmlElement(name = "CRUD", required = true)
    protected CRUD crud;
    @XmlElement(required = true)
    protected Inputs inputs;
    @XmlElement(required = true)
    protected Metadatatransformation metadatatransformation;
    @XmlElement(required = true)
    protected Configparameters configparameters;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the activities property.
     * 
     * @return
     *     possible object is
     *     {@link Activities }
     *     
     */
    public Activities getActivities() {
        return activities;
    }

    /**
     * Sets the value of the activities property.
     * 
     * @param value
     *     allowed object is
     *     {@link Activities }
     *     
     */
    public void setActivities(Activities value) {
        this.activities = value;
    }

    /**
     * Gets the value of the datalinks property.
     * 
     * @return
     *     possible object is
     *     {@link Datalinks }
     *     
     */
    public Datalinks getDatalinks() {
        return datalinks;
    }

    /**
     * Sets the value of the datalinks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Datalinks }
     *     
     */
    public void setDatalinks(Datalinks value) {
        this.datalinks = value;
    }

    /**
     * Gets the value of the crud property.
     * 
     * @return
     *     possible object is
     *     {@link CRUD }
     *     
     */
    public CRUD getCRUD() {
        return crud;
    }

    /**
     * Sets the value of the crud property.
     * 
     * @param value
     *     allowed object is
     *     {@link CRUD }
     *     
     */
    public void setCRUD(CRUD value) {
        this.crud = value;
    }

    /**
     * Gets the value of the inputs property.
     * 
     * @return
     *     possible object is
     *     {@link Inputs }
     *     
     */
    public Inputs getInputs() {
        return inputs;
    }

    /**
     * Sets the value of the inputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Inputs }
     *     
     */
    public void setInputs(Inputs value) {
        this.inputs = value;
    }

    /**
     * Gets the value of the metadatatransformation property.
     * 
     * @return
     *     possible object is
     *     {@link Metadatatransformation }
     *     
     */
    public Metadatatransformation getMetadatatransformation() {
        return metadatatransformation;
    }

    /**
     * Sets the value of the metadatatransformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Metadatatransformation }
     *     
     */
    public void setMetadatatransformation(Metadatatransformation value) {
        this.metadatatransformation = value;
    }

    /**
     * Gets the value of the configparameters property.
     * 
     * @return
     *     possible object is
     *     {@link Configparameters }
     *     
     */
    public Configparameters getConfigparameters() {
        return configparameters;
    }

    /**
     * Sets the value of the configparameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link Configparameters }
     *     
     */
    public void setConfigparameters(Configparameters value) {
        this.configparameters = value;
    }

}
