package info.textgrid.lab.workflow;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.log.logsession;
import info.textgrid.lab.workflow.servicedescription.Configparameter;
import info.textgrid.lab.workflow.servicedescription.Examplevalue;
import info.textgrid.middleware.confclient.ConfservClientConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.kwfgrid.gworkflowdl.structure.ArrayListProperties;
import net.kwfgrid.gworkflowdl.structure.CapacityException;
import net.kwfgrid.gworkflowdl.structure.Data;
import net.kwfgrid.gworkflowdl.structure.Edge;
import net.kwfgrid.gworkflowdl.structure.Factory;
import net.kwfgrid.gworkflowdl.structure.JdomString;
import net.kwfgrid.gworkflowdl.structure.Operation;
import net.kwfgrid.gworkflowdl.structure.OperationCandidate;
import net.kwfgrid.gworkflowdl.structure.OperationClass;
import net.kwfgrid.gworkflowdl.structure.Place;
import net.kwfgrid.gworkflowdl.structure.Token;
import net.kwfgrid.gworkflowdl.structure.Transition;
import net.kwfgrid.gworkflowdl.structure.Workflow;
import net.kwfgrid.gworkflowdl.structure.WorkflowFormatException;

import org.apache.axiom.om.util.Base64;

public class ChainGWDL {
	Workflow w;
	Chain c;
	Place sidReadPlace = null;
	Place logReadPlace = null;
	String crudEpr = "";
	String streamingEditorEpr = "";

	public final static HashMap<String, String> wsTypeMap = new HashMap<String, String>();
	static {
		wsTypeMap.put("soap", "soap");
		wsTypeMap.put("rest", "XXXunimplementedXXX");
	}

	/**
	 * Create simplified GWDL from a Workflow "Chain" with ChainEntries and
	 * Links
	 * 
	 * @param c
	 */
	public ChainGWDL(Chain c) {
		this.c = c;
		w = Factory.newWorkflow();
		w.setProperties(new ArrayListProperties());
		w.getProperties().put("redistributionOfFailedActivities", "false");
		w.getProperties().put("occurrence.sequence", ""); // TODO check if comes
															// out: <property
															// name="o.s" />

		StringBuffer automaticDescription = new StringBuffer();
		// create transitions
		for (ChainEntry ce : c.getChain()) {
			automaticDescription.append(ce.getName()).append(" and ");

			Transition t = constructTransition(ce);
			w.addTransition(t);

			// create places for config parameters of this transition
			for (Configparameter cp : ce.getConfigParameters()) {
				ArrayList<Place> configPlaces = constructPlacesWithConfigDataToken(
						ce, cp);
				for (Place p : configPlaces) {
					w.addPlace(p);
					Edge e = Factory.newEdge();
					if (ce.getTns() == null) {
						e.setExpression(cp.getParam());
					} else {
						e.setExpression("tns:" + cp.getParam());
					}
					e.setPlace(p);
					t.addReadEdge(e);
				}
			}
		}
		if (c.getDescription().length() > 0) {
			w.setDescription(c.getDescription());
		} else {
			int tmp = automaticDescription.length();
			automaticDescription.delete(tmp - 5, tmp);
			w.setDescription(automaticDescription.toString());
		}

		// add places for the links
		int placecount = 0;
		for (EntryLink l : c.getLinks()) {
			if (l.isDeleted() || l.isUndecided()) {
				continue;
			}
			placecount++;
			addPlaceAndEdge(l, placecount);
		}
	}

	public void addPlaceAndEdge(EntryLink l, int placecount) {
		Place p = Factory.newPlace();
		p.setID("placeid" + Integer.toString(placecount));
		p.setProperties(new ArrayListProperties());

		if (l.getFrom() != null && l.getFromPort() != null) {
			// a link from a Transition
			p.getProperties().put("FromTransition", getTransID(l.getFrom()));
			p.getProperties().put("FromPort", l.getFromPort().getParam());
			p.getProperties().put("FromPortMultiple",
					Boolean.toString(l.getFromPort().isMultiple()));
			p.getProperties().put("FromPortCRUD",
					Boolean.toString(l.getFromPort().isCrud()));

			Transition source = w.getTransition(getTransID(l.getFrom()));
			Edge e = Factory.newEdge();
			if (l.getFrom().getTns() == null) {
				e.setExpression("$" + l.getFromPort().getParam());
			} else {
				e.setExpression("$tns:" + l.getFromPort().getParam());
			}
			e.setPlace(p);
			// TODO what to do with multiple output ports?
			source.addOutEdge(e);
		} else {
			// a link from workflow inputs
			p.getProperties().put("FromPortCRUD", "true"); // i.e. an URI
															// comes in
			p.getProperties().put("Input",
					getTransID(l.getTo()) + "@" + l.getToPort().getParam());
		}

		if (l.getTo() != null && l.getToPort() != null) {
			// a link to some transition
			p.getProperties().put("ToTransition", getTransID(l.getTo()));
			p.getProperties().put("ToPort", l.getToPort().getParam());
			p.getProperties().put("ToPortMultiple",
					Boolean.toString(l.getToPort().isMultiple()));
			p.getProperties().put("ToPortCRUD",
					Boolean.toString(l.getToPort().isCrud()));

			Transition target = w.getTransition(getTransID(l.getTo()));
			Edge e = Factory.newEdge();
			if (l.getTo().getTns() == null) {
				e.setExpression(l.getToPort().getParam());
			} else {
				e.setExpression("tns:" + l.getToPort().getParam());
			}
			e.setPlace(p);
			// we need to switch here between read places for multiple
			// inputs and input places where tokens are being consumed
			if (l.getToPort().isMultiple()) {
				target.addReadEdge(e);
				// TODO make sure we put a input edge with a control token here
			} else {
				target.addInEdge(e);
			}

		} else {
			// a link to workflow outputs
			p.getProperties().put("ToPortCRUD", "true"); // i.e. an URI
															// comes out
			p.getProperties().put("Output",
					getTransID(l.getFrom()) + "@" + l.getFromPort().getParam());
		}

		// just a check out of curiousity
		if (p.getProperties().get("Input") != null
				&& p.getProperties().get("Output") != null) {
//			System.out.println("Weird: Have a link from hades to nirvana!");
		} else {
			w.addPlace(p);
		}

	}

	public ArrayList<Place> constructPlacesWithConfigDataToken(ChainEntry ce,
			Configparameter cp) {

		ArrayList<Examplevalue> exes = ce.getChosenConfigParamExampleValues(cp
				.getParam());

		ArrayList<Place> result = new ArrayList<Place>();

		int distinctPlaceIDcounter = 0; // this one we need for params occurring
										// several times
		for (Examplevalue ex : exes) {
			distinctPlaceIDcounter++;
			String dataString = ChainEntry.anyToString(ex.getContent());
			if ((cp.isNeedsB64Encoding() || cp.isCrud()) && ex.isInline()) {
				// b64 encode only if:
				// content is not a TGO URI (i.e. inline) AND
				// (service takes b64 (needsb64..) OR
				// service takes URI, i.e. needs a CRUD.create which needs b64
				// anyway)
				dataString = Base64.encode(dataString.getBytes());
			}

			Place p = constructDataTokenPlace("config--" + getTransID(ce)
					+ "--" + cp.getParam() + distinctPlaceIDcounter,
					cp.getParam(), dataString);

			p.getProperties().put("ToTransition", getTransID(ce));
			p.getProperties().put("ConfigForParam", cp.getParam());
			// treat config places like other places as regards URIs vs. inline,
			// i.e. automatic CRUD.read/.create where needed
			p.getProperties().put("FromPortCRUD",
					Boolean.toString(!ex.isInline()));
			p.getProperties().put("ToPortCRUD", Boolean.toString(cp.isCrud()));
			result.add(p);
		}
		return result;
	}

	public Transition constructTransition(ChainEntry ce) {
		Transition t = Factory.newTransition();
		t.setID(getTransID(ce));
		if (ce.getTns() != null) {
			t.setProperties(new ArrayListProperties());
			t.getProperties().put("xmlns:tns", ce.getTns());
		}
		Operation op = Factory.newOperation();
		t.setOperation(op);

		OperationClass oc = Factory.newOperationClass();
		oc.setName(ce.getOperationName());
		op.set(oc);

		OperationCandidate o = Factory.newOperationCandidate();
		o.setType(wsTypeMap.get(ce.getType())); // TODO check if key
												// present???
		o.setOperationName(ce.getOperationName());
		o.setResourceName(ce.getURL()); // TODO CAVE: for inline w[as]dl's,
										// this yet points nowhere
		o.setSelected(true);
		oc.addOperationCandidate(o);

		return t;
	}

	/**
	 * Create a simplified GWDL representation from a stored workflow.
	 * 
	 * @param tgo
	 */
	public ChainGWDL(TextGridObject tgo) {

	}

	public static String getTransID(ChainEntry ce) {
		return "n" + Integer.toString(ce.getID()) + "-" + ce.getOperationName();
	}

	public String marshal() {
		try {
			return JdomString.workflow2string(w);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Sorry, there was some exception while marshalling";
	}

	/**
	 * returns false if the input TGO lists have different numbers of objects.
	 * pooled input parameters will be ignored.
	 * 
	 * @param inputTGOs
	 * @param inputLinks
	 * @return
	 */
	public static boolean checkForEqualNumbers(
			ArrayList<ArrayList<TextGridObject>> inputTGOs,
			ArrayList<EntryLink> inputLinks) {

		int previousNumberOfInputs = inputTGOs.get(0).size();
		for (int i = 1; i < inputLinks.size(); i++) {
			if (inputLinks.get(i).getFrom() == null
					&& inputLinks.get(i).getFromPort() == null
					&& !inputLinks.get(i).getToPort().isMultiple()) {
				if (inputTGOs.get(i).size() != previousNumberOfInputs) {
					return false;
				}
			}
		}
		return true;
	}

	public void mergeInputObjects(
			ArrayList<ArrayList<TextGridObject>> inputTGOs,
			ArrayList<EntryLink> inputLinks) {

		Pattern extractChainLevel = Pattern.compile("n(\\d+)-.*");

		int index = 0;
		Place foundPlace = null;
		for (EntryLink l : inputLinks) {
			// find the right Place
			for (Place cand : w.getPlaces()) {
				if (cand.getProperties().get("Input") != null
						&& cand.getProperties().get("ToPort")
								.equals(l.getToPort().getParam())) {
					String toTransition = cand.getProperties().get(
							"ToTransition");
					Matcher mt = extractChainLevel.matcher(toTransition);
					if (!mt.matches()) {
//						System.out.println("Urks! Bad Transition Id '"
//								+ toTransition + "'");
						return;
					}
					int level = Integer.parseInt(mt.group(1));
					if (level == l.getTo().getID()) {
						foundPlace = cand;
						break;
					}
				}
			}

			ArrayList<TextGridObject> thisBoxList = inputTGOs.get(index);
			Iterator<TextGridObject> issiter = thisBoxList.iterator();
			// switch here if l.ismultipe ---> need many places.
			if (l.getToPort().isMultiple()) {
				// duplicate: foundplace, the CRUDtransition pointing there, and
				// the following data place... and add those read palces to the
				// following Service Transition

				// in this order: foundplace -> followingCrudRead -> afterRead
				// -> followingService
				String foundPlaceID = foundPlace.getID();
				Transition followingCrudRead = w.getTransition("crudRead-"
						+ foundPlaceID);
				Place afterRead = null;
				Place beforeService = null; // could be the same as afterRead
				Transition followingService = w.getTransition(foundPlace
						.getProperties().get("ToTransition"));
				if (followingCrudRead != null) {
					afterRead = followingCrudRead.getOutEdge(
							foundPlaceID + "-afterRead").getPlace();
					if (afterRead == null) {
//						System.out.println("Urks, Workflow not consistent! "
//								+ foundPlaceID);
					}
					beforeService = afterRead;
				} else {
					// Service knows crud, no crud.read inserted
					beforeService = foundPlace;
				}

				// now multiply Workflow objects (foundPlace, and if needed,
				// followingCrudRead and afterRead)...
				int multicount = 0;
				while (issiter.hasNext()) {
					TextGridObject oneInput = (TextGridObject) issiter.next();
					multicount++;
					String suffix = "multi" + Integer.toString(multicount);

					// in each case, we need to multiply the place the uri
					// originates from
					// can set the token right now
					Place p = constructDataTokenPlace(foundPlaceID + suffix,
							"param", oneInput.getURI().toString());
					w.addPlace(p);

					if (followingCrudRead == null) {
						// if the service knows how to read uris, its only this
						// one:
						Edge e = Factory.newEdge();
						e.setExpression(followingService.getReadEdge(
								beforeService.getID()).getExpression());
						e.setPlace(p);
						followingService.addReadEdge(e);
					} else {
						// if the service got a CRUD.read inserted, its longer.
						// need to multiply the Crudread and the Place inbetween
						Place q = Factory.newPlace();
						q.setID(afterRead.getID() + suffix);
						w.addPlace(q);

						Transition t = Factory.newTransition();
						t.setID(followingCrudRead.getID() + suffix);

						Operation opX = Factory.newOperation();
						t.setOperation(opX);

						OperationClass ocX = Factory.newOperationClass();
						ocX.setName("read");
						opX.set(ocX);

						OperationCandidate oX = Factory.newOperationCandidate();
						oX.setType(wsTypeMap.get("soap"));
						oX.setOperationName("read");
						oX.setResourceName(crudEpr);

						oX.setSelected(true);
						ocX.addOperationCandidate(oX);

						Edge sidEdge = Factory.newEdge();
						sidEdge.setExpression("tns:sessionId");
						sidEdge.setPlace(sidReadPlace);
						t.addReadEdge(sidEdge);

						Edge logEdge = Factory.newEdge();
						logEdge.setExpression("tns:logParameter");
						logEdge.setPlace(logReadPlace);
						t.addReadEdge(logEdge);

						Edge uriRead = Factory.newEdge();
						uriRead.setExpression("tns:uri");
						uriRead.setPlace(p);
						t.addInEdge(uriRead);

						Edge b64afterRead = Factory.newEdge();
						b64afterRead.setExpression("$tns:data");
						b64afterRead.setPlace(q);
						t.addOutEdge(b64afterRead);

						w.addTransition(t);

						Edge e = Factory.newEdge();
						e.setExpression(followingService.getReadEdge(
								beforeService.getID()).getExpression());
						e.setPlace(q);
						followingService.addReadEdge(e);
					}
				}
				followingService.removeReadEdge(beforeService.getID());
				String[] numberedPlaces = w.getPlaceIDs();
				for (int i = 0; i < numberedPlaces.length; i++) {
					if (numberedPlaces[i].equals(foundPlace.getID())) {
						w.removePlace(i);
					}
				}
				if (followingCrudRead != null) {
					String[] numberedTransitions = w.getTransitionIDs();
					for (int i = 0; i < numberedTransitions.length; i++) {
						if (numberedTransitions[i].equals(followingCrudRead
								.getID())) {
							w.removeTransition(i);
						}
					}
					numberedPlaces = w.getPlaceIDs(); // should have changed, so
														// query anew
					for (int i = 0; i < numberedPlaces.length; i++) {
						if (numberedPlaces[i].equals(afterRead.getID())) {
							w.removePlace(i);
						}
					}
					Place afterReadMD = followingCrudRead.getOutEdge(
							foundPlaceID + "-afterReadMD").getPlace();
					numberedPlaces = w.getPlaceIDs(); // should have changed, so
					// query anew
					for (int i = 0; i < numberedPlaces.length; i++) {
						if (numberedPlaces[i].equals(afterReadMD.getID())) {
							w.removePlace(i);
						}
					}

				}

			} else { // not multiple, now grouped.
				int groupIndex = 0;
				while (issiter.hasNext()) {
					TextGridObject oneInput = (TextGridObject) issiter.next();

					StringBuffer buffer = new StringBuffer();
					buffer.append("<data><param>")
							.append(oneInput.getURI().toString())
							.append("</param></data>");
					Data data = Factory.newData();
					try {
						data.fromXML(buffer.toString());
					} catch (WorkflowFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					Token t = Factory.newToken(data);

					t.getProperties().put("data.group",
							"UriNumber" + groupIndex);

					try {
						foundPlace.addToken(t);
					} catch (CapacityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					groupIndex++;
				}
			}
			index++;
		}
	}

	/**
	 * Connect each Crud.create that needs Metadata with one of the available
	 * Crud.reads that provides Metadata, possibly iterating over the latter if
	 * there are not enough of them. It ensures that no consumer is located
	 * higher in the chain than its provider. Dummy connections are made only if
	 * there could not be found a MD provider for a MD consumer among the
	 * chain's MetadataTransformers.
	 * 
	 */
	public void connectMetadata() {
		ArrayList<Place> mdProviders = new ArrayList<Place>();
		ArrayList<Place> mdConsumers = new ArrayList<Place>();

		try {
			ConfClient confClient = ConfClient.getInstance();
			streamingEditorEpr = confClient.getValue("StreamingEditor");
		} catch (OfflineException e) {
			// TODO Auto-generated catch block
			OnlineStatus.netAccessFailed("Could not contact Confserv", e);
		}

		for (Place p : w.getPlaces()) {
			if (p.getProperties().get("MetadataFrom") != null
					&& !p.getProperties().get("MetadataFrom").equals("unknown")) {
				mdProviders.add(p);
			}
			if (p.getProperties().get("MetadataFor") != null) {
				mdConsumers.add(p);
			}
		}

		Pattern extractChainLevel = Pattern.compile("n(\\d+)-.*");
		Pattern extractParam = Pattern.compile("[^@]+@(\\S+)");

		// One possible solution: dummy connecting
		Iterator<Place> nextProviderIterator = mdProviders.iterator();
		int seIDcount = 0;
		for (Place consumer : mdConsumers) {
			// find the next one, checking that the consumer is deeper
			// than the provider
			String consumerId = consumer.getProperties()
					.get("MetadataConsumer");
			Matcher mc = extractChainLevel.matcher(consumerId);
			if (!mc.matches()) {
//				System.out.println("Urks! Bad Transition Id '" + consumerId
//						+ "'");
				return;
			}
			int consumerLevel = Integer.parseInt(mc.group(1));

			String metadataForPhrase = consumer.getProperties().get(
					"MetadataFor");
			Matcher mfor = extractParam.matcher(metadataForPhrase);
			if (!mfor.matches()) {
//				System.out.println("Urks! Bad MetadataFor Phrase'"
//						+ metadataForPhrase + "'");
				return;
			}
			String consumerParam = mfor.group(1);

			int triedAllProviders = 0;
			while (true) {
				if (!nextProviderIterator.hasNext()) {
					// have to re-iterate over inputs or other metadata
					// providers such that all consumers receive metadata
					nextProviderIterator = mdProviders.iterator();
				}
				Place possibleProvider = nextProviderIterator.next();
				triedAllProviders++;
				String providerId = possibleProvider.getProperties().get(
						"MetadataProvider");
				Matcher mp = extractChainLevel.matcher(providerId);
				if (!mp.matches()) {
//					System.out.println("Urks! Bad Transition Id '" + providerId
//							+ "'");
					return;
				}
				int providerLevel = Integer.parseInt(mp.group(1));

				String metadataFromPhrase = possibleProvider.getProperties()
						.get("MetadataFrom");
				Matcher mfrom = extractParam.matcher(metadataFromPhrase);
				if (!mfrom.matches()) {
//					System.out.println("Urks! Bad MetadataFrom Phrase'"
//							+ metadataFromPhrase + "'");
					return;
				}
				String providerParam = mfrom.group(1);

				if (providerLevel <= consumerLevel) {

					boolean foundInTransformers = false;
					MetadataTransformer foundmdt = null;
					for (MetadataTransformer mdt : c.getTransformers()) {
						if (mdt.getOutput().getFrom().getID() == consumerLevel
								&& mdt.getOutput().getFromPort().getParam()
										.equals(consumerParam)
								&& mdt.getFromInput().getTo().getID() == providerLevel
								&& mdt.getFromInput().getToPort().getParam()
										.equals(providerParam)) {
							// YEAH! Found it!
							foundmdt = mdt;
							foundInTransformers = true;
							break;
						}
					}
					if (!foundInTransformers
							&& triedAllProviders < mdProviders.size()) {
						// try at least all possible Providers for a
						// Transformer. Only if no Transformer could be found,
						// connect to the next matching one.
						continue;
					}
//					System.out.println("connecting " + possibleProvider.getID()
//							+ " giving metadata input for " + consumer.getID());

					triedAllProviders = 0;
					seIDcount++;

					String stylesheetUri = MetadataTransformer.DEFAULT_STYLESHEET_URI;
					if (foundmdt != null) {
						stylesheetUri = foundmdt.getStylesheet().getURI()
								.toString();
					}

					Place xslURI = constructDataTokenPlace("xslURIforSE-"
							+ seIDcount, "param", stylesheetUri);
					Place xslB64 = Factory.newPlace();
					xslB64.setID("xslB64forSE-" + seIDcount);

					Transition readXSL = Factory.newTransition();
					readXSL.setID("XSLreaderForSE-" + seIDcount);

					Operation opX = Factory.newOperation();
					readXSL.setOperation(opX);

					OperationClass ocX = Factory.newOperationClass();
					ocX.setName("read");
					opX.set(ocX);

					OperationCandidate oX = Factory.newOperationCandidate();
					oX.setType(wsTypeMap.get("soap"));
					oX.setOperationName("read");
					oX.setResourceName(crudEpr);

					oX.setSelected(true);
					ocX.addOperationCandidate(oX);

					Edge sidEdge = Factory.newEdge();
					sidEdge.setExpression("tns:sessionId");
					sidEdge.setPlace(sidReadPlace);
					readXSL.addReadEdge(sidEdge);

					Edge logEdge = Factory.newEdge();
					logEdge.setExpression("tns:logParameter");
					logEdge.setPlace(logReadPlace);
					readXSL.addReadEdge(logEdge);

					Edge xslUriRead = Factory.newEdge();
					xslUriRead.setExpression("tns:uri");
					xslUriRead.setPlace(xslURI);
					readXSL.addInEdge(xslUriRead);

					Edge b64afterRead = Factory.newEdge();
					b64afterRead.setExpression("$tns:tgObjectData");
					b64afterRead.setPlace(xslB64);
					readXSL.addOutEdge(b64afterRead);

					Transition mdTrans = Factory.newTransition();
					mdTrans.setID("SE-" + seIDcount);

					Operation op = Factory.newOperation();
					mdTrans.setOperation(op);

					OperationClass oc = Factory.newOperationClass();
					oc.setName("sedit");
					op.set(oc);

					OperationCandidate o = Factory.newOperationCandidate();
					o.setType(wsTypeMap.get("soap"));
					o.setOperationName("transformInline");
					o.setResourceName(streamingEditorEpr);
					o.setSelected(true);
					oc.addOperationCandidate(o);

					if (foundmdt != null) {
						StringBuffer paramStruct = new StringBuffer();
						// paramStruct.append("<params>");
						// TODO check!
						boolean tg1109isfixed = true;
						if (tg1109isfixed) {
							for (String key : foundmdt.getParams().keySet()) {
								paramStruct.append("<param><name>").append(key)
										.append("</name><value>")
										.append(foundmdt.getParams().get(key))
										.append("</value></param>");
							}
						}
						// paramStruct.append("</params>");
						Place paramsPlace = constructDataTokenPlace(
								"ParamsForSE-" + seIDcount, "param",
								paramStruct.toString());
						w.addPlace(paramsPlace);
						Edge paramEdge = Factory.newEdge();
						paramEdge.setExpression("params");
						paramEdge.setPlace(paramsPlace);
						mdTrans.addReadEdge(paramEdge);
					} else {
//						System.out
//								.println("Assuming no parameters and default stylesheet for metadata transformation");
					}

					Edge xslEdge = Factory.newEdge();
					xslEdge.setExpression("stylesheet");
					xslEdge.setPlace(xslB64);
					mdTrans.addReadEdge(xslEdge);

					Edge inEdge = Factory.newEdge();
					inEdge.setExpression("input");
					inEdge.setPlace(possibleProvider);
					mdTrans.addInEdge(inEdge);

					Edge siddEdge = Factory.newEdge();
					siddEdge.setExpression("session");
					siddEdge.setPlace(sidReadPlace);
					mdTrans.addReadEdge(siddEdge);

					Edge loggEdge = Factory.newEdge();
					loggEdge.setExpression("loginfo");
					loggEdge.setPlace(logReadPlace);
					mdTrans.addReadEdge(loggEdge);

					Edge outEdge = Factory.newEdge();
					outEdge.setExpression("$result");
					outEdge.setPlace(consumer);
					mdTrans.addOutEdge(outEdge);

					w.addPlace(xslB64);
					w.addPlace(xslURI);
					w.addTransition(readXSL);
					w.addTransition(mdTrans);

					break;
				}
			}
		}
	}

	public boolean insertCrudTransitions(String targetProject) {
		Place targetProjectReadPlace = null;
		if (!targetProject.equals("")) {
			targetProjectReadPlace = constructDataTokenPlace("targetProject",
					"param", targetProject);
			w.addPlace(targetProjectReadPlace);
		}

		sidReadPlace = constructDataTokenPlace("crud-sessionId", "param",
				RBACSession.getInstance().getSID(false));
		logReadPlace = constructDataTokenPlace("crud-logParameter", "param",
				logsession.getInstance().getloginfo());
		w.addPlace(sidReadPlace);
		w.addPlace(logReadPlace);

		Place emptyRevison = null;
		Place emptyURI = null;
		if (targetProjectReadPlace != null && targetProject.length() > 0) {
			// need those two dummies for crudcreate
			emptyRevison = constructDataTokenPlace("emptyRevision", "param", "");
			emptyURI = constructDataTokenPlace("emptyURI", "param", "");
			w.addPlace(emptyRevison);
			w.addPlace(emptyURI);
		}

		try {
			ConfClient confClient = ConfClient.getInstance();
			crudEpr = confClient.getValue(ConfservClientConstants.TG_CRUD)
					+ "?wsdl";
		} catch (OfflineException e) {
			// TODO Auto-generated catch block
			OnlineStatus.netAccessFailed("Could not contact Confserv", e);
		}

		for (Place p : w.getPlaces()) {
			String precedingProducesURIString = p.getProperties().get(
					"FromPortCRUD");
			String followingConsumesURIString = p.getProperties().get(
					"ToPortCRUD");
			if (precedingProducesURIString == null
					|| followingConsumesURIString == null) {
				continue;
			}
			boolean precedingProducesURI = Boolean
					.parseBoolean(precedingProducesURIString);
			boolean followingConsumesURI = Boolean
					.parseBoolean(followingConsumesURIString);

			if (precedingProducesURI && !followingConsumesURI) {
				insertCrudRead(p);
			} else if (!precedingProducesURI && followingConsumesURI) {
				if (targetProjectReadPlace == null
						|| targetProject.length() < 1) {
					if (p.getProperties().get("Output") == null) {
//						System.out
//								.println("ERROR: This Workflow has an intermediate Place and you didnt give me a targetProject to write my temporary objects to!");
					} else {
						// Add the MD place (dangling, to be connected later)
						Place metadataForCreate = Factory.newPlace();
						metadataForCreate.setID(p.getID() + "-mdbeforeCreate");
						metadataForCreate
								.setProperties(new ArrayListProperties());
						String metadataFor = "Output:"
								+ p.getProperties().get("Output");
						metadataForCreate.getProperties().put("MetadataFor",
								metadataFor);
						Transition source = w.getTransition(p.getProperties()
								.get("FromTransition"));
						metadataForCreate.getProperties().put(
								"MetadataConsumer", source.getID());
						w.addPlace(metadataForCreate);
					}
				} else {

					insertCrudCreate(p, emptyRevison, emptyURI,
							targetProjectReadPlace);
				}
			} else {
				// do nothing, produce and consume are compatible
			}
		}
		return true;
	}

	/**
	 * Transform a structure like the following (note that input and expected
	 * output do not match for the place in the middle):
	 * 
	 * <pre>
	 * [A]-------URI---------->(p)---------DATA--------->[B]
	 * </pre>
	 * 
	 * into this:
	 * 
	 * <pre>
	 * [A]----URI---->(p)----URI---->[CRUDread]----DATA--->()-----DATA--->[B] 
	 * 	    						     \-----Metadata--->()
	 * </pre>
	 * 
	 * i.e. we have to remove one Link (the DATA Link) and add four, inserting
	 * the Transition for CrudRead and two new Places. The new Metadata Place is
	 * dangling, to be connected later to some CRUD.create needing it.
	 * 
	 * @param p
	 * @param logReadPlace
	 * @param sidReadPlace
	 * @param crudEpr
	 * @return
	 */
	public boolean insertCrudRead(Place p) {
		// insert a CRUD.read transition
		// this also applies to input and config places where an URI comes in
//		System.out.println("Inserting CRUDread for place " + p.getID());

		Transition crudRead = Factory.newTransition();
		crudRead.setID("crudRead-" + p.getID());

		Operation op = Factory.newOperation();
		crudRead.setOperation(op);

		OperationClass oc = Factory.newOperationClass();
		oc.setName("read");
		op.set(oc);

		OperationCandidate o = Factory.newOperationCandidate();
		o.setType(wsTypeMap.get("soap"));
		o.setOperationName("read");
		o.setResourceName(crudEpr);

		o.setSelected(true);
		oc.addOperationCandidate(o);

		Edge sidEdge = Factory.newEdge();
		sidEdge.setExpression("tns:sessionId");
		sidEdge.setPlace(sidReadPlace);
		crudRead.addReadEdge(sidEdge);

		Edge logEdge = Factory.newEdge();
		logEdge.setExpression("tns:logParameter");
		logEdge.setPlace(logReadPlace);
		crudRead.addReadEdge(logEdge);

		Place afterRead = Factory.newPlace();
		afterRead.setID(p.getID() + "-afterRead");

		Place afterReadMD = Factory.newPlace();
		afterReadMD.setID(p.getID() + "-afterReadMD");

		String metadataFrom;
		if (p.getProperties().get("Input") != null) {
			metadataFrom = "Input:" + p.getProperties().get("Input");
		} else if (p.getProperties().get("FromTransition") != null) {
			metadataFrom = "Transition:"
					+ p.getProperties().get("FromTransition") + "@"
					+ p.getProperties().get("FromPort");

		} else {
			metadataFrom = "unknown";
		}
		afterReadMD.setProperties(new ArrayListProperties());
		afterReadMD.getProperties().put("MetadataFrom", metadataFrom);

		Transition target = w.getTransition(p.getProperties().get(
				"ToTransition"));
		afterReadMD.getProperties().put("MetadataProvider", target.getID());

		boolean isReadEdge;
		String originalExpression;
		if (target.getInEdge(p.getID()) != null) {
			originalExpression = target.getInEdge(p.getID()).getExpression();
			target.removeInEdge(p.getID());
			isReadEdge = false;
		} else if (target.getReadEdge(p.getID()) != null) {
			originalExpression = target.getReadEdge(p.getID()).getExpression();
			target.removeReadEdge(p.getID());
			isReadEdge = true;
		} else {
			return false;
		}

		Edge uri2CrudRead = Factory.newEdge();
		uri2CrudRead.setExpression("tns:uri");
		uri2CrudRead.setPlace(p); // re-use old place where the URI comes in
		crudRead.addInEdge(uri2CrudRead); // always inEdge for crudread, only
											// the service will have read edges

		Edge crudRead2Data = Factory.newEdge();
		crudRead2Data.setExpression("$tns:tgObjectData");
		crudRead2Data.setPlace(afterRead);
		crudRead.addOutEdge(crudRead2Data);

		Edge crudRead2MD = Factory.newEdge();
		crudRead2MD.setExpression("$tns:tgObjectMetadata");
		crudRead2MD.setPlace(afterReadMD);
		crudRead.addOutEdge(crudRead2MD);

		Edge data2target = Factory.newEdge();
		data2target.setExpression(originalExpression);
		data2target.setPlace(afterRead);
		if (isReadEdge)
			target.addReadEdge(data2target);
		else
			target.addInEdge(data2target);

		w.addPlace(afterRead);
		w.addPlace(afterReadMD);
		w.addTransition(crudRead);
		return true;
	}

	/**
	 * Transform a structure like the following (note that input and expected
	 * output do not match for the place in the middle):
	 * 
	 * <pre>
	 * [A]-------DATA--------->(p)---------URI--------->[B]
	 * </pre>
	 * 
	 * into this:
	 * 
	 * <pre>
	 * [A]----DATA---->()----DATA---->[CRUDcreate]----URI--->(p)-----URI--->[B] 
	 *                 ()----Metadata-----^
	 * </pre>
	 * 
	 * i.e. we have to remove one Link (the DATA Link) and add four, inserting
	 * the Transition for Crud.create and two new Places. The new Place that
	 * provides the Metadata needed for a CRUD.create is dangling, waiting for a
	 * Metadata-transforming Streamingeditor to fill it.
	 * 
	 * @param p
	 * @param logReadPlace
	 * @param sidReadPlace
	 * @param crudEpr
	 * @return
	 */
	public boolean insertCrudCreate(Place p, Place emptyRevision,
			Place emptyURI, Place targetProjectReadPlace) {
		// insert a CRUD.create transition
		// this also applies to output places that produce a URI
//		System.out.println("Inserting CRUDcreate for place " + p.getID()
//				+ ", targetprojectplace " + targetProjectReadPlace.getID());

		Transition crudCreate = Factory.newTransition();
		crudCreate.setID("crudCreate-" + p.getID());

		Operation op = Factory.newOperation();
		crudCreate.setOperation(op);

		OperationClass oc = Factory.newOperationClass();
		oc.setName("create");
		op.set(oc);

		OperationCandidate o = Factory.newOperationCandidate();
		o.setType(wsTypeMap.get("soap"));
		o.setOperationName("create");
		o.setResourceName(crudEpr);

		o.setSelected(true);
		oc.addOperationCandidate(o);

		Edge sidEdge = Factory.newEdge();
		sidEdge.setExpression("tns:sessionId");
		sidEdge.setPlace(sidReadPlace);
		crudCreate.addReadEdge(sidEdge);

		Edge logEdge = Factory.newEdge();
		logEdge.setExpression("tns:logParameter");
		logEdge.setPlace(logReadPlace);
		crudCreate.addReadEdge(logEdge);

		Edge fromTargetProject = Factory.newEdge();
		fromTargetProject.setExpression("tns:projectId");
		fromTargetProject.setPlace(targetProjectReadPlace);
		crudCreate.addReadEdge(fromTargetProject);

		Edge emptyURIEdge = Factory.newEdge();
		emptyURIEdge.setExpression("tns:uri");
		emptyURIEdge.setPlace(emptyURI);
		crudCreate.addReadEdge(emptyURIEdge);

		Edge emptyRevisionEdge = Factory.newEdge();
		emptyRevisionEdge.setExpression("tns:createRevision");
		emptyRevisionEdge.setPlace(emptyRevision);
		crudCreate.addReadEdge(emptyRevisionEdge);

		Place dataForCreate = Factory.newPlace();
		dataForCreate.setID(p.getID() + "-beforeCreate");

		Place metadataForCreate = Factory.newPlace();
		metadataForCreate.setID(p.getID() + "-mdbeforeCreate");

		crudCreate.setProperties(new ArrayListProperties());
		crudCreate
				.getProperties()
				.put("xmlns:tgc",
						"http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService");

		String metadataFor;
		if (p.getProperties().get("Output") != null) {
			metadataFor = "Output:" + p.getProperties().get("Output");
		} else if (p.getProperties().get("ToTransition") != null) {
			metadataFor = "Transition:" + p.getProperties().get("ToTransition")
					+ "@" + p.getProperties().get("ToPort");

		} else {
			metadataFor = "unknown";
		}
		metadataForCreate.setProperties(new ArrayListProperties());
		metadataForCreate.getProperties().put("MetadataFor", metadataFor);

		Transition source = w.getTransition(p.getProperties().get(
				"FromTransition"));
		metadataForCreate.getProperties().put("MetadataConsumer",
				source.getID());

		// TODO Check if we really really do not have writeEdges...for MTOM
		// maybe?
		String originalExpression = source.getOutEdge(p.getID())
				.getExpression();
		source.removeOutEdge(p.getID());

		Edge create2uri = Factory.newEdge();
		create2uri.setExpression("*");
		create2uri.setPlace(p); // re-use old place where the URI goes out
		crudCreate.addOutEdge(create2uri);

		Edge data2create = Factory.newEdge();
		data2create.setExpression("tgc:tgObjectData");
		data2create.setPlace(dataForCreate);
		crudCreate.addInEdge(data2create); // TODO check: why not a readEdge?

		Edge metadata2create = Factory.newEdge();
		metadata2create.setExpression("tgc:tgObjectMetadata");
		metadata2create.setPlace(metadataForCreate);
		crudCreate.addInEdge(metadata2create); // TODO check: why not a
												// readEdge?

		Edge transition2data = Factory.newEdge();
		transition2data.setExpression(originalExpression);
		transition2data.setPlace(dataForCreate);
		source.addOutEdge(transition2data);

		w.addPlace(dataForCreate);
		w.addPlace(metadataForCreate);
		w.addTransition(crudCreate);
		return true;
	}

	public void addTrueControl(Transition t) {
		Place controlPlace = Factory.newPlace();
		controlPlace.setID(t.getID() + "control");
		Token trueControl = Factory.newToken(true);
		try {
			controlPlace.addToken(trueControl);
		} catch (CapacityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Edge controlEdge = Factory.newEdge();
		controlEdge.setPlace(controlPlace);
		t.addInEdge(controlEdge);
		w.addPlace(controlPlace);
	}

	public static Token constructDataToken(String tag, String value) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<data><").append(tag).append(">").append(value)
				.append("</").append(tag).append("></data>");
		Data data = Factory.newData();
		try {
			data.fromXML(buffer.toString());
		} catch (WorkflowFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Factory.newToken(data);
	}

	public static Place constructDataTokenPlace(String id, String tag,
			String value) {
		Place p = Factory.newPlace();
		p.setID(id);
		p.setProperties(new ArrayListProperties());

		try {
			p.addToken(constructDataToken(tag, value));
		} catch (CapacityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
	}

	// and more methods enhancing the ChainGWDL

}
