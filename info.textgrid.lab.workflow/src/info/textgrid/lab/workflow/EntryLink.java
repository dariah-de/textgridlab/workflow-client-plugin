package info.textgrid.lab.workflow;

import info.textgrid.lab.workflow.servicedescription.Input;
import info.textgrid.lab.workflow.servicedescription.Output;

public class EntryLink {
	private ChainEntry from = null;
	private ChainEntry to = null;
	private Output fromPort = null;
	private Input toPort = null;
	private int hashCode = 0;
	private LinkState linkState = LinkState.POSSIBLE;
	
	public enum LinkState {
		POSSIBLE,
		ADDED,
		DELETED
	}
	
	public EntryLink(ChainEntry from, Output fromPort, ChainEntry to,
			Input toPort) {
		super();
		this.from = from;
		this.to = to;
		this.fromPort = fromPort;
		this.toPort = toPort;
		this.linkState = LinkState.POSSIBLE;
		this.hashCode = hashCode();
	}

	@Override
	public boolean equals(Object oo) {
		if (oo instanceof EntryLink) {
			EntryLink o = (EntryLink) oo;
			return (from == o.from && to == o.to && fromPort == o.fromPort && toPort == o.toPort);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		if (hashCode != 0) {
			return hashCode;
		}
		int result = 17;
		if (from != null && fromPort != null) {
			result = 9 * result + from.hashCode() + fromPort.hashCode(); 
		}
		if (to != null && toPort != null) {
			result = 13 * result + to.hashCode() + toPort.hashCode(); 
		}
		return result;
	}
	
	

	public Boolean isAdded() {
		return linkState == LinkState.ADDED;
	}
	public void markLinkAdded() {
		this.linkState = LinkState.ADDED;
	}

	public Boolean isDeleted() {
		return linkState == LinkState.DELETED;
	}
	public void markLinkDeleted() {
		this.linkState = LinkState.DELETED;
	}

	public boolean isUndecided() {
		return linkState == LinkState.POSSIBLE;
	}
	public void resetState() {
		this.linkState = LinkState.POSSIBLE;
	}
	
	
	public Output getFromPort() {
		return fromPort;
	}

	public void setFromPort(Output fromPort) {
		this.hashCode = 0;
		this.fromPort = fromPort;
	}

	public Input getToPort() {
		return toPort;
	}

	public void setToPort(Input toPort) {
		this.hashCode = 0;
		this.toPort = toPort;
	}

	public ChainEntry getFrom() {
		return from;
	}

	public void setFrom(ChainEntry from) {
		this.hashCode = 0;
		this.from = from;
	}
	
	public ChainEntry getTo() {
		return to;
	}
	
	public void setTo(ChainEntry to) {
		this.hashCode = 0;
		this.to = to;
	}
	
	public void dump () {
		if (linkState == LinkState.POSSIBLE) {
			System.out.println("Possible: From " + getFrom() + ", " + (getFromPort() == null ? "WF-global Input" : getFromPort().getName()) + " to " + getTo() + ", " + (getToPort() == null ? "WF-global Output" : getToPort().getName()));
		} else if (linkState == LinkState.ADDED) {
			System.out.println("Added: From " + getFrom() + ", " + (getFromPort() == null ? "WF-global Input" : getFromPort().getName()) + " to " + getTo() + ", " + (getToPort() == null ? "WF-global Output" : getToPort().getName()));
		} else {
			System.out.println("Deleted: From " + getFrom() + ", " + (getFromPort() == null ? "WF-global Input" : getFromPort().getName()) + " to " + getTo() + ", " + (getToPort() == null ? "WF-global Output" : getToPort().getName()));
		}

	}
	
}
