package info.textgrid.lab.workflow;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.log.logsession;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.AbortWorkflowRequest;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.AbortWorkflowResponse;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.GetMyWorkflowIDsRequest;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.GetMyWorkflowIDsResponse;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.GetWorkflowDocumentRequest;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.GetWorkflowDocumentResponse;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.Gwesproxy;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.PortGwesproxy;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.Property;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.WFDetails;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

/**
 * A wrapper for creation of a client for the GWESProxy
 * 
 * @author martin
 * 
 */
public class WorkflowEngine {
	private static final String GWESPROXY_DEFAULT_ENDPOINT = "https://textgridlab.org/1.0/workflow/GWESproxy.wsdl"; //$NON-NLS-1$
	private static PortGwesproxy gwesProxy;
	private static WorkflowEngine we = null;

	private WorkflowEngine() {
		String endpoint = GWESPROXY_DEFAULT_ENDPOINT;
		try {
			endpoint = ConfClient.getInstance().getValue("workflow"); //$NON-NLS-1$
		} catch (OfflineException e) {
			OnlineStatus.netAccessFailed(Messages.WorkflowEngine_2, e);
		}
		if ("".equals(endpoint)) { //$NON-NLS-1$
			IStatus status = new Status(IStatus.WARNING, Activator.PLUGIN_ID,
					Messages.WorkflowEngine_1, null);
			Activator.getDefault().getLog().log(status);
			endpoint = GWESPROXY_DEFAULT_ENDPOINT;
		}
		IStatus status = new Status(IStatus.INFO, Activator.PLUGIN_ID,
				"gwesProxy endpoint: " + endpoint, null); //$NON-NLS-1$
		Activator.getDefault().getLog().log(status);

		try {
			Gwesproxy gp = new Gwesproxy(URI.create(endpoint).toURL());
			gwesProxy = gp.getTnsGwesproxy();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public PortGwesproxy getGwesProxy() {
		return gwesProxy;
	}

	public static WorkflowEngine getInstance() {
		if (we == null) {
			we = new WorkflowEngine();
		}
		return we;
	}

	public String getStatus(String workflowID) {
		GetMyWorkflowIDsRequest req = new GetMyWorkflowIDsRequest();
		req.setAuth(RBACSession.getInstance().getSID(false));

		GetMyWorkflowIDsResponse resp = getGwesProxy().getMyWorkflowIDs(req);

		for (WFDetails detail : resp.getWorkflow()) {
			if (detail.getID().equals(workflowID)) {
				for (Property p : detail.getProperty()) {
					if (p.getName().equals("status")) { //$NON-NLS-1$
						return p.getValue();
					}
				}
			}
		}
		return Messages.WorkflowEngine_0;
	}

	public WorkflowJob getWorkflowJob(String workflowID) {
		GetMyWorkflowIDsRequest req = new GetMyWorkflowIDsRequest();
		req.setAuth(RBACSession.getInstance().getSID(false));

		GetMyWorkflowIDsResponse resp = getGwesProxy().getMyWorkflowIDs(req);

		for (WFDetails detail : resp.getWorkflow()) {
			if (detail.getID().equals(workflowID)) {
				return new WorkflowJob(detail);
			}
		}
		return null;
	}

	public ArrayList<WorkflowJob> getMyJobs() {
		ArrayList<WorkflowJob> result = new ArrayList<WorkflowJob>();

		GetMyWorkflowIDsRequest req = new GetMyWorkflowIDsRequest();
		req.setAuth(RBACSession.getInstance().getSID(false));
		req.setLog(logsession.getInstance().getloginfo());

		GetMyWorkflowIDsResponse resp = getGwesProxy().getMyWorkflowIDs(req);
		for (WFDetails detail : resp.getWorkflow()) {
			result.add(new WorkflowJob(detail));
		}
		return result;
	}

	public String getWorkflowString(String workflowID) {
		GetWorkflowDocumentRequest req = new GetWorkflowDocumentRequest();
		req.setWorkflowid(workflowID);
		req.setAuth(RBACSession.getInstance().getSID(false));
		req.setLog(logsession.getInstance().getloginfo());

		GetWorkflowDocumentResponse resp = getGwesProxy().getWorkflowDocument(
				req);
		return resp.getGwdl();
	}

	public boolean abortWorkflow(String workflowID) {
		AbortWorkflowRequest req = new AbortWorkflowRequest();
		req.setWorkflowid(workflowID);
		req.setAuth(RBACSession.getInstance().getSID(false));
		req.setLog(logsession.getInstance().getloginfo());

		AbortWorkflowResponse resp = getGwesProxy().abortWorkflow(req);
		return resp.isResult();
	}
}
