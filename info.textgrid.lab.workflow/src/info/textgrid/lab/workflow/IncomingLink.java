package info.textgrid.lab.workflow;

import info.textgrid.lab.workflow.servicedescription.Output;

public class IncomingLink {
	ChainEntry from = null;
	Output fromPort = null;
	String ID = null;

	public IncomingLink(ChainEntry from, Output fromPort, String iD) {
		super();
		this.from = from;
		this.fromPort = fromPort;
		ID = iD;
	}
	
	public ChainEntry getFrom() {
		return from;
	}
	public void setFrom(ChainEntry from) {
		this.from = from;
	}
	public Output getFromPort() {
		return fromPort;
	}
	public void setFromPort(Output fromPort) {
		this.fromPort = fromPort;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	
}
