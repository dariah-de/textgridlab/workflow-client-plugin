package info.textgrid.lab.workflow;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.log.logsession;
import info.textgrid.lab.workflow.tgwf.CRUD;
import info.textgrid.lab.workflow.tgwf.Inputs;
import info.textgrid.lab.workflow.tgwf.Link;
import info.textgrid.lab.workflow.tgwf.ObjectFactory;
import info.textgrid.lab.workflow.tgwf.Tgwf;
import info.textgrid.lab.workflow.tgwf.URI;
import info.textgrid.middleware.confclient.ConfservClientConstants;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.PortGwesproxy;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.Property;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.UploadWorkflowRequest;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.UploadWorkflowResponse;
import info.textgrid.namespaces.middleware.workflow.gwesproxy.WFDetails;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXB;
import javax.xml.stream.XMLStreamException;

import net.kwfgrid.gworkflowdl.structure.CapacityException;
import net.kwfgrid.gworkflowdl.structure.Data;
import net.kwfgrid.gworkflowdl.structure.JdomString;
import net.kwfgrid.gworkflowdl.structure.Place;
import net.kwfgrid.gworkflowdl.structure.Token;
import net.kwfgrid.gworkflowdl.structure.Workflow;
import net.kwfgrid.gworkflowdl.structure.WorkflowFormatException;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.PlatformObject;

public class WorkflowJob extends PlatformObject implements
		Comparable<WorkflowJob> {
	private String workflowID;
	private String workflowDescription;
	private String workflowStatus;
	private Date workflowStartDate;
	private Long workflowStartDateMS;
	private String warnings = "";
	private String workflowForSubmission = "";
	private PortGwesproxy proxy;
	private ObjectFactory tgwfFactory = new ObjectFactory();
	private Tgwf tgwf;
	private String gwdl;
	private String targetProjectId;

	private List<Property> workflowProperties;

	/**
	 * Take a TGWF workflow description and some input documents to be
	 * processed. Inserts the latter into the workflow, changes some fields,
	 * 
	 * @param workflow
	 *            XML Representation of a TextGridWorklfow (tgwf format) as a
	 *            TextGridObject
	 * @param inputURIs
	 *            ArrayList of URIs of the Documents (being TextGridObjects) to
	 *            be processed.
	 */
	public WorkflowJob(TextGridObject workflow, String targetProjectId,
			ArrayList<ArrayList<TextGridObject>> inputTGOs,
			ArrayList<EntryLink> inputLinks) {
		// TODO will put all this in a background job later...
		IFile sf = (IFile) workflow.getAdapter(IFile.class);
		try {
			tgwf = JAXB.unmarshal(sf.getContents(true), Tgwf.class);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		this.targetProjectId = targetProjectId;
		ChainTGWF ct = new ChainTGWF(tgwf);
		Chain c = ct.getChain();
		ChainGWDL cg = new ChainGWDL(c);
		cg.insertCrudTransitions(targetProjectId);
		// System.out.println("XXXXX after insertCRUD"+cg.marshal());
		cg.connectMetadata();
		// System.out.println("XXXXX after connectMD"+cg.marshal());
		cg.mergeInputObjects(inputTGOs, inputLinks);
		// System.out.println("XXXXX after mergeInputs"+cg.marshal());
		gwdl = cg.marshal();

		// mergeWithInputs(tgwf, inputTGOs, inputLinks);
		// insertCurrentSettings(tgwf, targetProjectId);

	}

	/**
	 * Hand over a TextGridObject which is already GWDL instead of TGWF. Assumes
	 * the following Places with their input data tokens:
	 * 
	 * <pre>
	 * PlaceID Input for the URI Batch 
	 * PlaceID crud-sessionId for the SID 
	 * PlaceID crud-logParameter for the Log 
	 * PlaceID targetProject
	 * PlaceID Output with property "Output"
	 * 
	 * @param gwdl
	 * @param targeProjectId
	 * @param inputbatch
	 */
	public WorkflowJob(TextGridObject gwdlTGO, String targeProjectId,
			ArrayList<java.net.URI> inputbatch) {
		Workflow w = null;
		IFile sf = (IFile) gwdlTGO.getAdapter(IFile.class);
		try {
			InputStream is = sf.getContents();
			String s = readFully(is);
//			System.out.println("DEBUG: GWDL before transform: " + s);
			w = JdomString.string2workflow(s);
		} catch (WorkflowFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CapacityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Place targetProjectPlace = w.getPlace("targetProject");
		Place inputURIsPlace = w.getPlace("Input");
		Place sidPlace = w.getPlace("crud-sessionId");
		Place logPlace = w.getPlace("crud-logParameter");
		targetProjectPlace.removeAllTokens();
		inputURIsPlace.removeAllTokens();
		sidPlace.removeAllTokens();
		logPlace.removeAllTokens();
		try {
			targetProjectPlace.addToken(ChainGWDL.constructDataToken("param",
					targeProjectId));
			sidPlace.addToken(ChainGWDL.constructDataToken("param", RBACSession
					.getInstance().getSID(false)));
			logPlace.addToken(ChainGWDL.constructDataToken("param", logsession
					.getInstance().getloginfo()));
			int groupIndex = 0;
			for (java.net.URI oneURI : inputbatch) {
				Token oneToken = ChainGWDL.constructDataToken("param",
						oneURI.toString());
				oneToken.getProperties().put("data.group",
						"UriNumber" + groupIndex);
				inputURIsPlace.addToken(oneToken);
				groupIndex++;
			}
		} catch (CapacityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			gwdl = JdomString.workflow2string(w);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.targetProjectId = targetProjectId;
	}

	public String start() {
		proxy = WorkflowEngine.getInstance().getGwesProxy();

		UploadWorkflowRequest req = new UploadWorkflowRequest();

		req.setGwdl(gwdl);
//		System.out.println("XXXXXXXXXXXX Here is the GWDL ready for submission:\n" + gwdl);
		req.setTargetproject(targetProjectId);
		req.setAuth(RBACSession.getInstance().getSID(false));
		req.setLog(logsession.getInstance().getloginfo());

		// TODO put this in an extra job, might take long
		UploadWorkflowResponse resp = proxy.uploadWorkflow(req);
		workflowID = resp.getWorkflowid();
		return workflowID;

	}

	public WorkflowJob(WFDetails wfd) {
		workflowID = wfd.getID();
		workflowProperties = wfd.getProperty();
		workflowDescription = wfd.getDescription();

		for (Property p : workflowProperties) {
			// // System.out.println("Debug: property '" + p[0] +
			// "' has value '"
			// // + p[1] + "'.");
			// // Debug: the property named 'redistributionOfFailedActivities'
			// has
			// // value 'false'.
			// // Debug: the property named 'complexity' has value '0'.
			// // Debug: the property named 'isUnbounded' has value 'false'.
			// // Debug: the property named 'faultManagementPolicy' has value
			// // 'AbortOnActivityTerminated'.
			// // Debug: the property named 'workflow.persistence' has value
			// // 'true'.
			// // Debug: the property named 'status' has value 'COMPLETED'.
			// // Debug: the property named 'birthdayMs' has value
			// '1231859818957'.
			// // Debug: the property named 'durationUndefinedMs' has value
			// '126'.
			// // Debug: the property named 'durationInitiatedMs' has value
			// '180'.
			// // Debug: the property named 'durationRunningMs' has value '2'.
			// // Debug: the property named 'durationActiveMs' has value '0'.
			// // Debug: the property named 'durationSuspendedMs' has value '0'.
			// // Debug: the property named 'durationTotalMs' has value '308'.
			// // Debug: the property named 'endTimeMs' has value
			// '1231859819265'.
			// // Debug: the property named
			// // 'workflow.sequentialExecutionPathSizeMs' has value '0'.
			// // Debug: the property named 'workflow.speedupTotal' has value
			// // '0.0'.
			// // Debug: the property named 'workflow.speedupActive' has value
			// // 'NaN'.
			// // Debug: the property named 'level' has value 'MEMORY'.
			if ("status".equals(p.getName())) {
				workflowStatus = p.getValue();
			} else if ("birthdayMs".equals(p.getName())) {
				workflowStartDateMS = new Long(p.getValue());
				workflowStartDate = new Date((long) workflowStartDateMS);
			} else if (p.getName().matches("warn\\..+")
					|| p.getName().matches("error\\..+")) {
				warnings += p.getName() + ": " + p.getValue() + "\n";
			}
		}
	}

	/**
	 * Take current RBAC SessionID and LogParameter as known in the LAB and put
	 * them into the right places. Also insert current CRUD endpoint from
	 * confserv and target project.
	 * 
	 * @param tgwf
	 *            Workflow Document
	 * 
	 */
	public void insertCurrentSettings(Tgwf tgwf, String targetProject) {
		String crudEpr = "";
		try {
			ConfClient confClient = ConfClient.getInstance();
			crudEpr = confClient.getValue(ConfservClientConstants.TG_CRUD);
		} catch (OfflineException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CRUD crud = tgwfFactory.createCRUD();
		crud.setInstance(crudEpr);
		crud.setSessionID(RBACSession.getInstance().getSID(false));
		crud.setLogParameter(logsession.getInstance().getloginfo());
		crud.setTargetProject(targetProject);
		tgwf.setCRUD(crud);
	}

	/**
	 * insert URIs of inputTGOs into tgwf document. input Links are just for
	 * reference in order to find the right ids.
	 * 
	 * @param tgwf
	 * @param inputTGOs
	 * @param inputLinks
	 */
	public void mergeWithInputs(Tgwf tgwf,
			ArrayList<ArrayList<TextGridObject>> inputTGOs,
			ArrayList<EntryLink> inputLinks) {
		Inputs inputList = tgwfFactory.createInputs();

		int index = 0;
		for (EntryLink l : inputLinks) {
			// find the right id
			String tgwfLinkId = "";
			for (Link cand : tgwf.getDatalinks().getLink()) {
				if (cand.getType().equals("input")
						&& cand.getToParam().equals(l.getToPort().getParam())
						&& cand.getToServiceID().equals(
								Integer.toString(l.getTo().getID()))) {
					tgwfLinkId = cand.getId();
					break;
				}
			}
			// do the merge of many input boxes to one flat list with this box'
			// tgwfLinkId
			ArrayList<TextGridObject> thisBoxList = inputTGOs.get(index);
			Iterator<TextGridObject> issiter = thisBoxList.iterator();
			int groupIndex = 0;
			while (issiter.hasNext()) {
				URI u = tgwfFactory.createURI();
				u.setLinkId(tgwfLinkId);

				if (l.getToPort().isMultiple()) {
					u.setGroupId("multi");
				} else {
					u.setGroupId(Integer.toString(groupIndex));
				}

				TextGridObject oneInput = (TextGridObject) issiter.next();
				u.setValue(oneInput.getURI().toString());

				inputList.getURI().add(u);
				groupIndex++;
			}
			index++;
		}
		tgwf.setInputs(inputList);
	}

	public String toString() {
		return workflowDescription;
	}

	public String getStatus() {
		return workflowStatus;
	}

	public Date getDate() {
		return workflowStartDate;
	}

	public String getWfID() {
		return workflowID;
	}

	public ArrayList<TextGridObject> getResultTGOs() {
		ArrayList<TextGridObject> result = new ArrayList<TextGridObject>();

		String resultWorkflow = WorkflowEngine.getInstance().getWorkflowString(
				workflowID);

		Workflow w = null;
		try {
			w = JdomString.string2workflow(resultWorkflow);
		} catch (WorkflowFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CapacityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Place p : w.getPlaces()) {
			if (p.getProperties().get("Output") != null) {
				// found an output place (could be more than one, but we throw
				// them all into one list)
				for (Token t : p.getTokens()) {
					Data d = t.getData();
					String xmlString = d.toXML();
//					System.out.println("ONE RESULT: " + xmlString);
					OMElement tgoMDom = null;
					try {
						tgoMDom = AXIOMUtil.stringToOM(xmlString);						
						// if that should not work, see WSCOMMONS-345.patch.txt
						
						tgoMDom = tgoMDom.getFirstElement().getFirstElement();
						// need to dive in for two levels: over gwdl:data/crud:tgObjectMetadata/
					} catch (XMLStreamException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					try {
						TextGridObject oneTGO = TextGridObject.getInstance(
								tgoMDom, true);
						result.add(oneTGO);
					} catch (CoreException e) {
						// TODO Auto-generated catch block
						// TODO here we could trigger BLOB returns
						e.printStackTrace();
					}
				}
			}
		}
		return result;
	}

	public int compareTo(WorkflowJob o) {
		if (workflowStartDateMS > o.workflowStartDateMS) {
			return -1;
		} else if (workflowStartDateMS == o.workflowStartDateMS) {
			return 0;
		} else {
			return 1;
		}
	}

	public boolean hasErrors() {
		return !"".equals(warnings);
	}

	public String getErrors() {
		return warnings;
	}

	/**
	 * @param stream
	 * @return
	 * @throws IOException
	 */
	public static String readFully(InputStream stream) throws IOException {
		return readFully(new InputStreamReader(stream));
	}

	public static String readFully(Reader reader) throws IOException {
		char[] arr = new char[8 * 1024]; // 8K at a time
		StringBuffer buf = new StringBuffer();
		int numChars;

		while ((numChars = reader.read(arr, 0, arr.length)) > 0) {
			buf.append(arr, 0, numChars);
		}
		return buf.toString();
	}
}
