package info.textgrid.lab.workflow.wizard;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.ChainEntry;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

public class ServiceOrderingPage extends WizardPage implements Listener {

	private Chain chain;
	private TableViewer orderViewer;

	public ServiceOrderingPage(String pageName) {
		super(pageName);
		setTitle(Messages.ServiceOrderingPage_pagetitle);
		setDescription(Messages.ServiceOrderingPage_pagedescription);
	}

	public enum MoveDirection {
		UP, DOWN, DUPLICATE, REMOVE;
	}

	class OrderServicesLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			if (obj instanceof ChainEntry) {
				ChainEntry ce = (ChainEntry) obj;
				return ce.getName();
			} else {
				return "unknown class: " + obj.getClass(); //$NON-NLS-1$
			}
		}

		public Image getColumnImage(Object obj, int index) {
			return null;// getImage(obj);
		}

		public Image getImage(Object obj) {
			// return PlatformUI.getWorkbench().
			// getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
			return null;
		}
	}

	@Override
	public void createControl(Composite parent) {

		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 400;
		parentGD.widthHint = 800;
		parentGD.verticalAlignment = GridData.FILL;

		parent.setLayoutData(parentGD);

		Composite composite = new Composite(parent, SWT.NONE);
		// create the desired layout for this wizard page

		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, true);
		composite.setLayoutData(gdstd);
		GridLayout glstd = new GridLayout(2, false);
		composite.setLayout(glstd);
		setControl(composite);

		orderViewer = new TableViewer(composite, SWT.SINGLE | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION);
		orderViewer.getTable().setHeaderVisible(false);
		orderViewer.setContentProvider(new ArrayContentProvider());
		orderViewer.setLabelProvider(new OrderServicesLabelProvider());
		GridData gd0 = new GridData(SWT.FILL, SWT.FILL, true, true);
		orderViewer.getControl().setLayoutData(gd0);

		chain = ((WorkflowWizard) getWizard()).getChain();
		refreshViewer();

		Composite arrowComposite = new Composite(composite, SWT.NONE);
		GridData gdstda = new GridData(GridData.FILL, GridData.FILL, true, true);
		arrowComposite.setLayoutData(gdstda);
		GridLayout glstda = new GridLayout(1, false);
		arrowComposite.setLayout(glstda);

		Button moveUpButton = new Button(arrowComposite, SWT.PUSH);
		moveUpButton.setText(Messages.ServiceOrderingPage_MoveUpButton);
		moveUpButton
				.setToolTipText(Messages.ServiceOrderingPage_MoveUpButtonTip);
		moveUpButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				reorderServices(MoveDirection.UP);
				refreshViewer();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		Button moveDownButton = new Button(arrowComposite, SWT.PUSH);
		moveDownButton.setText(Messages.ServiceOrderingPage_MoveDownButton);
		moveDownButton
				.setToolTipText(Messages.ServiceOrderingPage_MoveDownButtonDescription);
		moveDownButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				reorderServices(MoveDirection.DOWN);
				refreshViewer();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		Button duplicateButton = new Button(arrowComposite, SWT.PUSH);
		duplicateButton.setText(Messages.ServiceOrderingPage_DuplicateButton);
		duplicateButton
				.setToolTipText(Messages.ServiceOrderingPage_DuplicateButtonDescription);
		duplicateButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				reorderServices(MoveDirection.DUPLICATE);
				refreshViewer();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		Button removeButton = new Button(arrowComposite, SWT.PUSH);
		removeButton.setText(Messages.ServiceOrderingPage_RemoveButton);
		removeButton
				.setToolTipText(Messages.ServiceOrderingPage_RemoveButtonDescription);
		removeButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				reorderServices(MoveDirection.REMOVE);
				refreshViewer();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
	}

	void reorderServices(MoveDirection direction) {
		IStructuredSelection sel = (IStructuredSelection) orderViewer
				.getSelection();
		if (sel == null) {
			return;
		}
		Object elem = sel.getFirstElement();
		if (elem instanceof ChainEntry) {
			ChainEntry ce = (ChainEntry) elem;
			int oldpos = chain.getChain().indexOf(ce);

			if (direction.equals(MoveDirection.UP) && oldpos > 0) {
				chain.getChain().remove(oldpos);
				chain.getChain().add(oldpos - 1, ce);
			} else if (direction.equals(MoveDirection.DOWN)
					&& oldpos < chain.getChain().size() - 1) {
				chain.getChain().remove(oldpos);
				chain.getChain().add(oldpos + 1, ce);
			} else if (direction.equals(MoveDirection.DUPLICATE)) {
				TextGridObject TGOofce = null;
				try {
					TGOofce = TextGridObject.getInstance(ce.getTGOURI(), false);
				} catch (CrudServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				chain.addEntry(new ChainEntry(TGOofce));
			} else if (direction.equals(MoveDirection.REMOVE)) {
				chain.getChain().remove(oldpos);
			}
			chain.resetLinks();
			chain.connectEntriesMonotone();
		}
	}

	void refreshViewer() {
		orderViewer.setInput(chain.getChain());
		orderViewer.refresh();
	}

	@Override
	public void handleEvent(Event event) {
		// TODO Auto-generated method stub
		refreshViewer();
	}

	@Override
	public boolean canFlipToNextPage() {
		return true;
	}

	public boolean isPageComplete() {
		return true;
	}

	@Override
	public IWizardPage getPreviousPage() {
		// ((WorkflowWizardMain)
		// getWizard()).serviceSelection.placeSelections(chain);
		ServiceSelectionPage ss = ((WorkflowWizard) getWizard()).serviceSelection;
		return ss;
	}

	
	public IWizardPage getNextPage() {
		LinkInsertionPage  li = ((WorkflowWizard) getWizard()).linkInsertion;
//		System.out.println("to the links!"); //$NON-NLS-1$
		li.drawCEboxes();
		return li;
	}
	
	
	
}
