package info.textgrid.lab.workflow.wizard;

import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.ChainEntry;
import info.textgrid.lab.workflow.EntryLinkFromComparator;
import info.textgrid.lab.workflow.servicedescription.Configparameter;
import info.textgrid.lab.workflow.servicedescription.Examplevalue;
import info.textgrid.lab.workflow.views.ConfigParameterEditingDialogue;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.PlatformUI;

public class ServiceConfigurationPage extends WizardPage implements Listener {

	Chain chain;
	Composite composite;
	ScrolledComposite scrolledcomposite;

	public ServiceConfigurationPage(String pageName) {
		super(pageName);
		setTitle(Messages.ServiceConfigurationPage_pagetitle);
		setDescription(Messages.ServiceConfigurationPage_pagedescription);
	}

	class ConfigLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			Examplevalue ex;
			if (obj instanceof Examplevalue) {
				ex = (Examplevalue) obj;
				return ex.getName();
			} else {
				return "unknown class: " + obj.getClass(); //$NON-NLS-1$
			}
		}
	}

	@Override
	public void createControl(Composite parent) {

		chain = ((WorkflowWizard) getWizard()).getChain();

		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 400;
		parentGD.widthHint = 800;
		parentGD.verticalAlignment = GridData.FILL;

		parent.setLayoutData(parentGD);

		scrolledcomposite = new ScrolledComposite(parent, SWT.V_SCROLL
				| SWT.H_SCROLL);
		scrolledcomposite.setAlwaysShowScrollBars(true);

		composite = new Composite(scrolledcomposite, SWT.BORDER);
		GridLayout glstd = new GridLayout(1, false);
		composite.setLayout(glstd);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, true);
		// gdstd.minimumWidth = 800;
		// gdstd.minimumHeight= 300 * chain.getChain().size();
		composite.setLayoutData(gdstd);

		scrolledcomposite.setContent(composite);
		scrolledcomposite.setExpandVertical(true);
		scrolledcomposite.setExpandHorizontal(true);
		scrolledcomposite.setMinWidth(775);
		scrolledcomposite.setMinHeight(300 * chain.getChain().size());
		setControl(scrolledcomposite);

		drawCEboxes();

	}

	public void drawCEboxes() {
		for (Control c : composite.getChildren()) {
			c.dispose();
		}
		for (ChainEntry ce : chain.getChain()) {
			drawCEbox(ce);
		}
		scrolledcomposite.setMinHeight(200 * chain.getChain().size());
		scrolledcomposite.layout(true, true);
	}

	public void drawCEbox(ChainEntry ce) {
		Composite comp = new Composite(composite, SWT.BORDER);
		GridLayout glstd = new GridLayout(1, false);
		comp.setLayout(glstd);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, false);
		comp.setLayoutData(gdstd);

		drawName(ce, comp);

		drawConfigParams(ce, comp);

	}

	public void drawName(ChainEntry e, Composite comp) {
		Composite nameBar = new Composite(comp, SWT.NONE);
		GridLayout glstd = new GridLayout(2, false);
		glstd.horizontalSpacing = 20;
		nameBar.setLayout(glstd);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, false);
		nameBar.setLayoutData(gdstd);

		Label labelName = new Label(nameBar, SWT.SINGLE);
		labelName.setText(e.getName());
		GridData gd3aI = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		labelName.setLayoutData(gd3aI);

		if (e.getConfigParameters().size() == 0) {
			Label labelNoConfigParams = new Label(nameBar, SWT.SINGLE);
			labelNoConfigParams
					.setText(Messages.ServiceConfigurationPage_serviceHasNoParamsLabel);
			GridData gd3ancp = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			labelNoConfigParams.setLayoutData(gd3ancp);
		}

	}

	public void drawConfigParams(ChainEntry e, Composite comp) {
		Composite paramBar = new Composite(comp, SWT.NONE);
		GridLayout glstd = new GridLayout(1, false);
		paramBar.setLayout(glstd);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, true);
		paramBar.setLayoutData(gdstd);
		final ChainEntry currentChainEntry = e;
		
		for (Configparameter cp : e.getConfigParameters()) {
			Composite c = new Composite(paramBar, SWT.NONE);

			final String thisConfigParam = cp.getParam();
			final String thisConfigParamName = cp.getName();

			c.setLayout(new GridLayout(cp.isMultiple() ? 5 : 3, false));
			GridData gdc = new GridData(GridData.FILL, GridData.FILL, false,
					false);
			c.setLayoutData(gdc);

			ArrayList<Examplevalue> exValues = e
					.getChosenConfigParamExampleValues(cp.getParam());

			if (exValues == null || exValues.size() == 0) {
				// we need a possiblity to add param values even if no value is
				// set

				Label t = new Label(c, SWT.SINGLE);
				t.setText(cp.getName() + Messages.ServiceConfigurationPage_noValuesSetLabel);
				GridData gde = new GridData(GridData.FILL, GridData.FILL,
						false, false);
				t.setLayoutData(gde);

				Button addB = new Button(c, SWT.PUSH);
				GridData gdb = new GridData(GridData.FILL, GridData.FILL,
						false, false);
				addB.setLayoutData(gdb);
				addB.setText(Messages.ServiceConfigurationPage_AddMenu);
				addB.addSelectionListener(new SelectionListener() {
					public void widgetSelected(SelectionEvent event) {
						currentChainEntry
								.addDefaultExampleValue(thisConfigParam);
					}

					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);
					}
				});
				break;
			}

			for (int i = 0; i < exValues.size(); i++) {

				final int exValueCounter = i;

				if (i == 0) {
					Label t = new Label(c, SWT.SINGLE);
					t.setText(cp.getName() + ": "); //$NON-NLS-1$
					GridData gde = new GridData(GridData.FILL, GridData.FILL,
							false, false);
					t.setLayoutData(gde);
				} else {
					Label dummy = new Label(c, SWT.SINGLE);
					dummy.setText(""); //$NON-NLS-1$
					GridData gde = new GridData(GridData.FILL, GridData.FILL,
							false, false);
					dummy.setLayoutData(gde);
				}

				final ComboViewer cv = new ComboViewer(c, SWT.SINGLE);
				cv.setLabelProvider(new ConfigLabelProvider());
				cv.setContentProvider(new ArrayContentProvider());
				cv.setComparator(new EntryLinkFromComparator());
				cv.setInput(cp.getExamplevalue());
				GridData gdcv = new GridData(GridData.FILL, GridData.FILL,
						false, false);
				cv.getControl().setLayoutData(gdcv);

				// pre-set with default example value
				if (exValues.get(i).isDefault()) {
					cv.setSelection(new StructuredSelection(exValues.get(i)),
							true);
					currentChainEntry.selectExampleValue(thisConfigParam,
							exValues.get(i), i);
				}
				
				cv.addPostSelectionChangedListener(new ISelectionChangedListener() {
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						// TODO Auto-generated method stub
						IStructuredSelection spss = (IStructuredSelection) cv
								.getSelection();
						Examplevalue selectedID = null;
						if (spss.getFirstElement() instanceof Examplevalue) {
							selectedID = (Examplevalue) spss.getFirstElement();
							if (selectedID != null) {
								currentChainEntry.selectExampleValue(
										thisConfigParam, selectedID,
										exValueCounter);
							} else {
//								System.out.println("no example value selected"); //$NON-NLS-1$
							}
						} else {
//							System.out.println("no example value selected"); //$NON-NLS-1$
						}

					}
				});

				final Button editB = new Button(c, SWT.PUSH);
				editB.setText(Messages.ServiceConfigurationPage_editmenu);
				GridData gdeb = new GridData(GridData.FILL, GridData.FILL,
						false, false);
				editB.setLayoutData(gdeb);
				if (cp.isMultiple()) {
					Button addB = new Button(c, SWT.PUSH);
					GridData gdab = new GridData(GridData.FILL, GridData.FILL,
							false, false);
					addB.setLayoutData(gdab);
					addB.setText(Messages.ServiceConfigurationPage_addmenu);
					addB.addSelectionListener(new SelectionListener() {
						public void widgetSelected(SelectionEvent event) {
							currentChainEntry
									.addDefaultExampleValue(thisConfigParam);
							drawCEboxes();
						}

						public void widgetDefaultSelected(SelectionEvent e) {
							widgetSelected(e);
						}
					});
					Button delB = new Button(c, SWT.PUSH);
					GridData gddb = new GridData(GridData.FILL, GridData.FILL,
							false, false);
					delB.setLayoutData(gddb);
					delB.setText(Messages.ServiceConfigurationPage_deletemenu);
					delB.addSelectionListener(new SelectionListener() {
						public void widgetSelected(SelectionEvent event) {
							currentChainEntry.deleteExampleValue(
									thisConfigParam, exValueCounter);
							drawCEboxes();
						}

						public void widgetDefaultSelected(SelectionEvent e) {
							widgetSelected(e);
						}
					});

				}
				editB.addSelectionListener(new SelectionListener() {
					public void widgetSelected(SelectionEvent event) {

						// assume some value has already been set, however...
						Examplevalue selectedExValue = currentChainEntry
								.getChosenConfigParamExampleValues(
										thisConfigParam).get(exValueCounter);
						// ...look if the selection has been changed and update
						// on-the-fly
						IStructuredSelection spss = (IStructuredSelection) cv
								.getSelection();
						if (spss.getFirstElement() instanceof Examplevalue) {
							selectedExValue = (Examplevalue) spss
									.getFirstElement();
						}

						String orig = ChainEntry.anyToString(selectedExValue
								.getContent());
						ConfigParameterEditingDialogue cped = new ConfigParameterEditingDialogue(
								PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow().getShell());
						cped.setContents(thisConfigParamName, orig,
								currentChainEntry, thisConfigParam,
								selectedExValue.isInline(), exValueCounter);
						cped.setBlockOnOpen(true);
						cped.open();

						// ...on-the-fly! (on the way back only if save had been
						// pressed)
						cv.setInput(currentChainEntry
								.getAllAvailableExampleValues(thisConfigParam));
						cv.setSelection(
								new StructuredSelection(currentChainEntry
										.getChosenConfigParamExampleValues(
												thisConfigParam).get(
												exValueCounter)), true);
					}

					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);
					}
				});
			}
		}
		comp.layout(true, true);
	}

	@Override
	public void handleEvent(Event event) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canFlipToNextPage() {
		return true;
	}

	public boolean isPageComplete() {
		return true;
	}

	public void dumpAllConfigs () {
		System.out.println("DDDDDDDDDDDDDDDDDDDDumping:"); //$NON-NLS-1$
		for (ChainEntry e : chain.getChain()) {
			System.out.println(e.getName());
			for (Configparameter cp : e.getConfigParameters()) {
				System.out.println(cp.getName()+":"); //$NON-NLS-1$
				for (Examplevalue ex : e.getChosenConfigParamExampleValues(cp.getParam())) {
					System.out.println(ChainEntry.anyToString(ex.getContent()));
				}
			}
		}
	}
	
	public IWizardPage getNextPage() {
		// dumpAllConfigs();
		MetadataTransformationPage md = ((WorkflowWizard) getWizard()).metadataTransformation;
		md.refreshTransformerArea();
		return md;
	}
}
