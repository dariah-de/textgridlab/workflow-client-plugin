package info.textgrid.lab.workflow.wizard;

import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.ChainTGWF;
import info.textgrid.lab.workflow.tgwf.Tgwf;
import info.textgrid.lab.workflow.views.ListOfWorkflows;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXB;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class WorkflowWizard extends Wizard implements INewWizard {

	Chain chain;

	ServiceSelectionPage serviceSelection;
	ServiceOrderingPage serviceOrdering;
	LinkInsertionPage linkInsertion;
	ServiceConfigurationPage serviceConfiguration;
	MetadataTransformationPage metadataTransformation;
	FinalPage finalPage;
	protected IWorkbench workbench;
	protected IStructuredSelection selection;

	private TextGridObject originalTGO;
	private String originalProjectId;
	private String originalTitle;

	public WorkflowWizard() {
		super();
		chain = new Chain();
	}

	@Override
	public void addPages() {
		serviceSelection = new ServiceSelectionPage("XXX"); //$NON-NLS-1$
		addPage(serviceSelection);
		serviceOrdering = new ServiceOrderingPage("YYY"); //$NON-NLS-1$
		addPage(serviceOrdering);

		linkInsertion = new LinkInsertionPage("LLL"); //$NON-NLS-1$
		addPage(linkInsertion);

		serviceConfiguration = new ServiceConfigurationPage("CCC"); //$NON-NLS-1$
		addPage(serviceConfiguration);

		metadataTransformation = new MetadataTransformationPage("MMM"); //$NON-NLS-1$
		addPage(metadataTransformation);

		finalPage = new FinalPage("FFF"); //$NON-NLS-1$
		addPage(finalPage);
	};

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// TODO Auto-generated method stub
		this.workbench = workbench;
		this.selection = selection;
		if (selection != null && !selection.isEmpty()) {
			Object obj = selection.getFirstElement();
			if (obj instanceof TextGridObject) {
				originalTGO = (TextGridObject) obj;
				setChainFromTGWF(originalTGO);
			}
		}
	}

	public void setChainFromTGWF(TextGridObject tgwfTGO) {
		originalTGO = tgwfTGO;
		IFile tgwfFile = (IFile) tgwfTGO.getAdapter(IFile.class);
		Tgwf tgwf = null;
		try {
			tgwf = JAXB.unmarshal(tgwfFile.getContents(true), Tgwf.class);
			// TODO get Chain back
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ChainTGWF ct = new ChainTGWF(tgwf);
		this.chain = ct.getChain();
		try {
			originalTitle = tgwfTGO.getTitle();
			this.setWindowTitle(originalTitle);
			originalProjectId = tgwfTGO.getProject();
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public TextGridObject getOriginalTGO() {
		return originalTGO;
	}

	public String getOriginalProjectId() {
		return originalProjectId;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTGO(TextGridObject originalTGO) {
		this.originalTGO = originalTGO;
	}

	public void setOriginalProjectId(String originalProjectId) {
		this.originalProjectId = originalProjectId;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	Chain getChain() {
		return chain;
	}

	@Override
	public boolean canFinish() {
		if (this.getContainer().getCurrentPage() == finalPage
				&& ((originalTGO != null && finalPage.getUpdateOriginalTGO()) || (finalPage
						.getNameTextString() != null
						&& finalPage.getNameTextString().length() > 0 && finalPage
						.getSelectedProject() != null)))

			return true;
		else
			return false;
	}

	private void showMessage(String message) {
		MessageBox mb = new MessageBox(PlatformUI.getWorkbench().getDisplay()
				.getActiveShell(), SWT.OK | SWT.ICON_WARNING | SWT.MULTI);
		mb.setMessage(message);
		mb.open();
	}

	@Override
	public boolean performFinish() {

		chain.setDescription(finalPage.getDescriptionText().getText());
		String tgwf = chain.toString();

		String title = finalPage.getNameTextString();

		TextGridObject tgwfTGO;

		if (originalTGO != null && finalPage.getUpdateOriginalTGO()) {
			tgwfTGO = originalTGO;
		} else {

			if (finalPage.getSelectedProject() == null) {
				showMessage(Messages.WorkflowWizard_NoTargetProjectMessage);
				return false;
			}
			tgwfTGO = TextGridObject
					.getNewObjectInstance(finalPage.getSelectedProject(),
							TGContentType
									.getContentType("text/tg.workflow+xml"), //$NON-NLS-1$
							title);
		}

		IFile file = AdapterUtils.getAdapter(tgwfTGO, IFile.class);
		if (file == null)
			System.out.println("Cannot save " + title); //$NON-NLS-1$
		else {
			try {
				file.setContents(
						new ByteArrayInputStream(tgwf.getBytes("UTF-8")), true, //$NON-NLS-1$
						false, null);
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (CoreException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		return true;
	}

}
