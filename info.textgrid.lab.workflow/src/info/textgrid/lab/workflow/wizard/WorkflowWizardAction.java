package info.textgrid.lab.workflow.wizard;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class WorkflowWizardAction implements IObjectActionDelegate {
    IWorkbenchPart part;
    ISelection selection;


	@Override
	public void run(IAction action) {
		// TODO Auto-generated method stub
        // Instantiates and initializes the wizard
		WorkflowWizard wizard = new WorkflowWizard();

        if ((selection instanceof IStructuredSelection) || (selection == null))
        wizard.init(part.getSite().getWorkbenchWindow().getWorkbench(), 
                (IStructuredSelection)selection);
                
        // Instantiates the wizard container with the wizard and opens it
        WizardDialog dialog = new WizardDialog( part.getSite().getShell(), wizard);
        dialog.create();
        dialog.open();

	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
        this.selection = selection;

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
        this.part = part;

	}

}
