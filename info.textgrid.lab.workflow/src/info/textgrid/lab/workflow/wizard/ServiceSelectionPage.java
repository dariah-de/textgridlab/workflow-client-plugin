package info.textgrid.lab.workflow.wizard;

import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.OnlineStatus;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;
import info.textgrid.lab.workflow.Activator;
import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.ChainEntry;
import info.textgrid.lab.workflow.approvedlist.Approved;
import info.textgrid.lab.workflow.approvedlist.Service;

import java.net.URI;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXB;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PreferencesUtil;

public class ServiceSelectionPage extends WizardPage implements
		ISelectionChangedListener {

	private TextGridObjectTableViewer allViewer;
	private TableViewer approvedViewer;
	private boolean showSearchResults;
	private Button servicesLockedButton;
	private TabFolder folder;
	private TabItem approvedTab;
	private TabItem allTab;
	public final String APPROVED_LIST_KEY = "ApprovedServicesTextGridURI"; // ConfServ //$NON-NLS-1$
	private List<Service> approvedServices = new ArrayList<Service>();
	private boolean servicesLocked = false;
	private Chain chain;
	ArrayList<TextGridObject> newServices;

	public ServiceSelectionPage(String pageName) {
		super(pageName);
		setTitle(Messages.ServiceSelectionPage_pageTitle);
		setDescription(Messages.ServiceSelectionPage_PageDescription);
	}

	class ApprovedServicesLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			if (obj instanceof Service) {
				Service s = (Service) obj;
				return s.getName();
			} else {
				return "unknown class: " + obj.getClass(); //$NON-NLS-1$
			}
		}

		public Image getColumnImage(Object obj, int index) {
			return null;// getImage(obj);
		}

		public Image getImage(Object obj) {
			// return PlatformUI.getWorkbench().
			// getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
			return null;
		}
	}

	class NameSorter extends ViewerSorter {
	}

	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub

		chain = ((WorkflowWizard) getWizard()).getChain();

		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 400;
		parentGD.widthHint = 800;
		parentGD.verticalAlignment = GridData.FILL;

		parent.setLayoutData(parentGD);

		Composite composite = new Composite(parent, SWT.NONE);
		// create the desired layout for this wizard page
		GridLayout gl = new GridLayout();
		int ncol = 1;
		gl.numColumns = ncol;
		composite.setLayout(gl);
		setControl(composite);

		folder = new TabFolder(composite, SWT.TOP);
		GridData gf = new GridData(GridData.FILL, GridData.FILL, true, true);
		folder.setLayoutData(gf);
		GridLayout glf = new GridLayout(1, false);
		folder.setLayout(glf);

		Composite approvedComposite = new Composite(folder, SWT.NONE);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, true);
		approvedComposite.setLayoutData(gdstd);
		GridLayout glstd = new GridLayout(1, false);
		approvedComposite.setLayout(glstd);
		approvedTab = new TabItem(folder, SWT.NONE);
		approvedTab.setText(Messages.ServiceSelectionPage_ApprovedTabTitle);
		approvedTab.setControl(approvedComposite);

		Composite allServicesComposite = new Composite(folder, SWT.NONE);
		GridData gdexp = new GridData(GridData.FILL, GridData.FILL, true, true);
		allServicesComposite.setLayoutData(gdexp);
		GridLayout glexp = new GridLayout(1, false);
		allServicesComposite.setLayout(glexp);
		allTab = new TabItem(folder, SWT.NONE);
		allTab.setText(Messages.ServiceSelectionPage_AllTabTitle);
		allTab.setControl(allServicesComposite);

		approvedViewer = new TableViewer(approvedComposite, SWT.MULTI
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		approvedViewer.getTable().setHeaderVisible(false);
		approvedViewer.setContentProvider(new ArrayContentProvider());
		approvedViewer.setLabelProvider(new ApprovedServicesLabelProvider());
		approvedViewer.addSelectionChangedListener(this);

		GridData gd0 = new GridData(SWT.FILL, SWT.FILL, true, true);
		approvedViewer.getControl().setLayoutData(gd0);
		approvedViewer.setSorter(new NameSorter());
		boolean foundApproved = fillApprovedViewer();
		if (foundApproved) {

		} else {
			approvedComposite.dispose();
			approvedTab.dispose();
		}

		allViewer = new TextGridObjectTableViewer(allServicesComposite,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		allViewer.setVisibleColumns(EnumSet.of(Column.TITLE));
		allViewer.getTable().setHeaderVisible(false);

		GridData gd1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		allViewer.getControl().setLayoutData(gd1);
		allViewer.setSorter(new NameSorter());
		allViewer.addSelectionChangedListener(this);
		((DeferredListContentProvider) allViewer.getContentProvider())
				.addDoneListener(new DeferredListContentProvider.IDoneListener() {
					@Override
					public void loadDone(Viewer viewer) {
						placeSelections(chain);
					}
				});
		refreshViewer();

		// done in listener, once allViewer is filled: placeSelections(chain);

		newServices = new ArrayList<TextGridObject>();

		servicesLockedButton = new Button(composite, SWT.CHECK);
		GridData gd3a = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd3a.widthHint = 2000;
		servicesLockedButton.setLayoutData(gd3a);
		servicesLockedButton
				.setText(Messages.ServiceSelectionPage_KeepServicesButtonText);
		// wantSearchResults.setImage(InProjectState.SEARCHED.icon());
		servicesLockedButton
				.setToolTipText(Messages.ServiceSelectionPage_KeepServicesButtonDescription);
		servicesLockedButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				setLockedServices(servicesLockedButton.getSelection());
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		if (chain.getChain().size() > 0) {
			// warn the user not to overwrite
			setLockedServices(true);
		}

	}

	void setLockedServices(boolean locked) {
		servicesLocked = locked;
		if (!approvedViewer.getTable().isDisposed()) {
			approvedViewer.getTable().setEnabled(!locked);
		}
		allViewer.getTable().setEnabled(!locked);
		servicesLockedButton.setSelection(locked);
	}

	private void refreshViewer() {
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setAllProjects(true);
		searchRequest
				.setQueryMetadata("format:\"text/tg.servicedescription+xml\""); //$NON-NLS-1$
		allViewer.setInput(searchRequest);
	}

	private boolean fillApprovedViewer() {
		try {
			ConfClient confClient = ConfClient.getInstance();
			String approvedURIstring = confClient.getValue(APPROVED_LIST_KEY);
			if (approvedURIstring == null || approvedURIstring.equals("")) { //$NON-NLS-1$
				Activator
						.handleProblem(IStatus.WARNING, new Throwable(),
								"Workflow: Could not find TextGridObject holding the approved services list!"); //$NON-NLS-1$
				return false;
			}
			URI approvedURI = new URI(approvedURIstring);
			TextGridObject listtgo = TextGridObject.getInstance(approvedURI,
					false);
			IFile sf = (IFile) listtgo.getAdapter(IFile.class);
			Approved approvedlist = JAXB.unmarshal(sf.getContents(true),
					Approved.class);
			approvedServices = approvedlist.getService();
		} catch (OfflineException e) {
			OnlineStatus
					.netAccessFailed(
							"Workflow: Could not contact Confserver for Approved Services List location!", //$NON-NLS-1$
							e);
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		approvedViewer.setInput(approvedServices);
		return true;
	}

	void placeSelections(Chain c) {
		ArrayList<URI> apprUris = new ArrayList<URI>();
		for (Service s : approvedServices) {
			apprUris.add(URI.create(s.getUri()));
		}
		ArrayList<URI> chainUris = new ArrayList<URI>();
		boolean onlyApprovedURIS = true;
		for (ChainEntry ce : c.getChain()) {
			URI u = ce.getTGOURI();
			chainUris.add(u);
			boolean found = false;
			for (URI s : apprUris) {
				if (s.equals(u)) {
					found = true;
					//					System.out.println(u + " is approved"); //$NON-NLS-1$
				}
			}
			if (!found) {
				//				System.out.println(u + " is not approved"); //$NON-NLS-1$
				onlyApprovedURIS = false;
			}
		}
		if (onlyApprovedURIS) {
			folder.setSelection(approvedTab);
			ArrayList<Service> chainServices = new ArrayList<Service>();
			for (URI u : chainUris) {
				String ustring = u.toString();
				for (Service s : approvedServices) {
					if (s.getUri().equals(ustring)) {
						//						System.out.println("Added approved uri " + ustring); //$NON-NLS-1$
						chainServices.add(s);
					}
				}
			}
			approvedViewer.setSelection(new StructuredSelection(chainServices),
					true);
		} else {
			folder.setSelection(allTab);
			ArrayList<TextGridObject> chainTGOs = new ArrayList<TextGridObject>();
			for (URI u : chainUris) {
				TextGridObject tgo = null;
				try {
					tgo = TextGridObject.getInstance(u, false);
				} catch (CrudServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//				System.out.println("added non-approved uri " + u.toString()); //$NON-NLS-1$
				chainTGOs.add(tgo);
			}
			allViewer.setSelection(new StructuredSelection(chainTGOs), true);
		}
	}

	@Override
	public boolean canFlipToNextPage() {
		return isPageComplete();
	}

	public boolean isPageComplete() {
		if (!approvedViewer.getSelection().isEmpty()
				|| !allViewer.getSelection().isEmpty()) {
			if (!servicesLocked) {
				TabItem[] tiarr = folder.getSelection();
				//				System.out.println("Adding one service from the " //$NON-NLS-1$
				//						+ tiarr[0].getText() + " tab's list to the workflow"); //$NON-NLS-1$

				newServices = new ArrayList<TextGridObject>();

				if (tiarr[0].equals(approvedTab)) {
					IStructuredSelection wfss = (IStructuredSelection) approvedViewer
							.getSelection();
					for (Iterator<Service> it = wfss.iterator(); it.hasNext();) {
						TextGridObject serviceTGO = null;
						try {
							serviceTGO = TextGridObject.getInstance(
									URI.create(it.next().getUri()), false);
						} catch (CrudServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						newServices.add(serviceTGO);
					}

				} else {
					IStructuredSelection wfss = (IStructuredSelection) allViewer
							.getSelection();
					for (Iterator<TextGridObject> it = wfss.iterator(); it
							.hasNext();) {
						newServices.add(it.next());
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public IWizardPage getPreviousPage() {
		return null;
	}

	@Override
	public IWizardPage getNextPage() {
		if (newServices.size() > 0 && !servicesLocked) {
			chain.getChain().clear();
			chain.resetLinks();
		}
		for (TextGridObject service : newServices) {
			ChainEntry centry = new ChainEntry(service);
			if (centry != null && centry.getIncomingLinks() != null) {
				chain.addEntry(centry);
			} else {
				MessageBox mb = new MessageBox(PlatformUI.getWorkbench()
						.getDisplay().getActiveShell(), SWT.OK
						| SWT.ICON_WARNING | SWT.MULTI);
				try {
					mb.setMessage(MessageFormat
							.format(Messages.ServiceSelectionPage_NoValidSDWarning,
									service.getTitle()));
				} catch (CoreException e) {
					e.printStackTrace();
				}
				mb.open();
				return this;
			}
		}
		setLockedServices(true);
		newServices = new ArrayList<TextGridObject>();
		// System.out.println("going forward...");
		ServiceOrderingPage so = ((WorkflowWizard) getWizard()).serviceOrdering;
		so.refreshViewer();
		return so;
	}

	@Override
	public void selectionChanged(SelectionChangedEvent e) {
		boolean anyServiceIsSelected = false;
		if (e.getSource() == approvedViewer
				&& !approvedViewer.getSelection().isEmpty()) {
			anyServiceIsSelected = true;
		} else if (e.getSource() == allViewer
				&& !allViewer.getSelection().isEmpty()) {
			anyServiceIsSelected = true;
		}
		if (anyServiceIsSelected) {
			setPageComplete(isPageComplete());
			if (getWizard().getContainer().getCurrentPage() != null) {
				getWizard().getContainer().updateButtons();
			}
		}
	}

}
