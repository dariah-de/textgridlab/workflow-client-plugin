package info.textgrid.lab.workflow.wizard;

import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.ChainEntry;
import info.textgrid.lab.workflow.EntryLink;
import info.textgrid.lab.workflow.EntryLinkFromComparator;
import info.textgrid.lab.workflow.servicedescription.Input;
import info.textgrid.lab.workflow.servicedescription.Output;

import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

public class LinkInsertionPage extends WizardPage implements Listener {

	Chain chain;
	Composite composite;
	ScrolledComposite scrolledcomposite;
	boolean chainIsValid = false;

	public LinkInsertionPage(String pageName) {
		super(pageName);
		setTitle(Messages.LinkInsertionPage_pagetitle);
		setDescription(Messages.LinkInsertionPage_pagedescription);
	}

	class LinkLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			EntryLink l;
			if (obj instanceof EntryLink) {
				l = (EntryLink) obj;
				if (l.getFrom() == null || l.getFromPort() == null) {
					return Messages.LinkInsertionPage_LinkFromWorkflowInputs;
				} else {
					return l.getFrom().getName() + ", " //$NON-NLS-1$
							+ l.getFromPort().getName();
				}
			} else {
				return "unknown class: " + obj.getClass(); //$NON-NLS-1$
			}
		}
	}

	class LinkOutLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			EntryLink l;
			if (obj instanceof EntryLink) {
				l = (EntryLink) obj;
				if (l.getTo() == null || l.getToPort() == null) {
					return Messages.LinkInsertionPage_LinkToWorkflowOutputs;
				} else {
					return l.getTo().getName() + ", " + l.getToPort().getName(); //$NON-NLS-1$
				}
			} else {
				return "unknown class: " + obj.getClass(); //$NON-NLS-1$
			}
		}
	}

	@Override
	public void createControl(Composite parent) {

		chain = ((WorkflowWizard) getWizard()).getChain();

		GridData parentGD = new GridData();
		parentGD.horizontalAlignment = GridData.FILL;
		parentGD.grabExcessVerticalSpace = true;
		parentGD.grabExcessHorizontalSpace = true;
		parentGD.heightHint = 400;
		parentGD.widthHint = 800;
		parentGD.verticalAlignment = GridData.FILL;

		parent.setLayoutData(parentGD);

		scrolledcomposite = new ScrolledComposite(parent, SWT.V_SCROLL
				| SWT.H_SCROLL);
		scrolledcomposite.setAlwaysShowScrollBars(true);

		composite = new Composite(scrolledcomposite, SWT.BORDER);
		GridLayout glstd = new GridLayout(1, false);
		composite.setLayout(glstd);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, true);
		// gdstd.minimumWidth = 800;
		// gdstd.minimumHeight= 300 * chain.getChain().size();
		composite.setLayoutData(gdstd);

		scrolledcomposite.setContent(composite);
		scrolledcomposite.setExpandVertical(true);
		scrolledcomposite.setExpandHorizontal(true);
		scrolledcomposite.setMinWidth(775);
		scrolledcomposite.setMinHeight(300 * chain.getChain().size());
		setControl(scrolledcomposite);

		drawCEboxes();
		chainIsValid = chain.validateLinks();
	}

	public void drawCEboxes() {
		for (Control c : composite.getChildren()) {
			c.dispose();
		}
		for (ChainEntry ce : chain.getChain()) {
			drawCEbox(ce);
		}
		appendClearButton();
		scrolledcomposite.setMinHeight(200 * chain.getChain().size());
		scrolledcomposite.layout(true, true);
	}

	public void appendClearButton() {
		Button clearButton = new Button(composite, SWT.PUSH);
		clearButton.setText(Messages.LinkInsertionPage_ClearLinksButtonText);
		clearButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				chain.clearLinks();
				chain.connectEntriesMonotone();
				drawCEboxes();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
	}

	public void drawCEbox(ChainEntry ce) {
		Composite comp = new Composite(composite, SWT.BORDER);
		GridLayout glstd = new GridLayout(1, false);
		comp.setLayout(glstd);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, false);
		comp.setLayoutData(gdstd);

		drawInputLinks(ce, comp);

		drawName(ce, comp);

		drawOutputLinks(ce, comp);

	}

	public void drawInputLinks(ChainEntry e, Composite comp) {
		int noOfInputLInks = e.getInputs().size();
		Composite paramBar = new Composite(comp, SWT.NONE);
		GridLayout glstd = new GridLayout(noOfInputLInks + 1, false);
		glstd.horizontalSpacing = 15;
		paramBar.setLayout(glstd);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, true);
		paramBar.setLayoutData(gdstd);

		Label labelFrom = new Label(paramBar, SWT.SINGLE);
		labelFrom.setText(Messages.LinkInsertionPage_FromLabel);
		GridData gd3aF = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		labelFrom.setLayoutData(gd3aF);

		for (Input i : e.getInputs()) {
			List<EntryLink> linksToHere = chain.findLinksToHere(e, i);

			final LinkLabelProvider clp = new LinkLabelProvider();
			// check if there is only one possibility. If so, add link at
			// once and do not show the list/button
			if (linksToHere.size() == 1) {
				EntryLink solitaire = linksToHere.get(0);
				Label fromReal = new Label(paramBar, SWT.SINGLE);
				GridData gd3a = new GridData(SWT.LEFT, SWT.CENTER, true, false);
				fromReal.setLayoutData(gd3a);
				fromReal.setText(clp.getText(solitaire));
				if (solitaire.isUndecided()) { // only add link if it had'nt
												// been added before yet
					chain.addLink(solitaire);
					checkLinksComplete();
				}
			} else {
				final ComboViewer cv = new ComboViewer(paramBar, SWT.SINGLE);
				GridData gd0 = new GridData(SWT.FILL, SWT.FILL, false, false);
				cv.getControl().setLayoutData(gd0);
				cv.setLabelProvider(clp);
				cv.setContentProvider(new ArrayContentProvider());
				cv.setComparator(new EntryLinkFromComparator());
				cv.setInput(linksToHere);
				cv.addPostSelectionChangedListener(new ISelectionChangedListener() {
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						// TODO Auto-generated method stub
						IStructuredSelection sel = (IStructuredSelection) cv
								.getSelection();
						if (sel == null) {
							return;
						}
						Object elem = sel.getFirstElement();
						if (elem instanceof EntryLink) {
							EntryLink selectedLink = (EntryLink) elem;
							chain.addLink(selectedLink);
							drawCEboxes(); // TODO brute force
							checkLinksComplete();
						}
					}
				});
			}
		}

		Label labelInput = new Label(paramBar, SWT.SINGLE);
		labelInput.setText(Messages.LinkInsertionPage_InputLabel);
		GridData gd3aI = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		labelInput.setLayoutData(gd3aI);

		for (Input i : e.getInputs()) {
			Label nextInputName = new Label(paramBar, SWT.SINGLE);
			GridData gd3N = new GridData(SWT.LEFT, SWT.CENTER, true, false);
			nextInputName.setLayoutData(gd3N);
			nextInputName.setText(i.getName());
		}

	}

	public void drawName(ChainEntry e, Composite comp) {
		Composite nameBar = new Composite(comp, SWT.NONE);
		GridLayout glstd = new GridLayout(2, false);
		glstd.horizontalSpacing = 20;
		nameBar.setLayout(glstd);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, false);
		nameBar.setLayoutData(gdstd);

		Label labelName = new Label(nameBar, SWT.SINGLE);
		labelName.setText(e.getName());
		GridData gd3aI = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		labelName.setLayoutData(gd3aI);

		Label descriptionText = new Label(nameBar, SWT.WRAP);
		GridData gdll = new GridData(GridData.FILL, GridData.FILL, true, false);
		descriptionText.setLayoutData(gdll);
		descriptionText.setText(e.getDescription());
		descriptionText.setBounds(nameBar.getBounds());

	}

	public void drawOutputLinks(ChainEntry e, Composite comp) {
		int noOfOutputLInks = e.getOutputs().size();
		Composite paramBar = new Composite(comp, SWT.NONE);
		GridLayout glstd = new GridLayout(noOfOutputLInks + 1, false);
		glstd.horizontalSpacing = 15;
		paramBar.setLayout(glstd);
		GridData gdstd = new GridData(GridData.FILL, GridData.FILL, true, true);
		paramBar.setLayoutData(gdstd);

		Label labelOutput = new Label(paramBar, SWT.SINGLE);
		labelOutput.setText(Messages.LinkInsertionPage_OutputLabel);
		GridData gd3aI = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		labelOutput.setLayoutData(gd3aI);

		for (Output o : e.getOutputs()) {
			Label nextOutputName = new Label(paramBar, SWT.SINGLE);
			GridData gd3N = new GridData(SWT.LEFT, SWT.CENTER, true, false);
			nextOutputName.setLayoutData(gd3N);
			nextOutputName.setText(o.getName());
		}

		Label labelTo = new Label(paramBar, SWT.SINGLE);
		labelTo.setText(Messages.LinkInsertionPage_ToLabel);
		GridData gd3aF = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		labelTo.setLayoutData(gd3aF);

		for (Output o : e.getOutputs()) {
			List<EntryLink> linksFromHere = chain.findLinksFromHere(e, o);

			final LinkOutLabelProvider clp = new LinkOutLabelProvider();
			// check if there is only one possibility. If so, add link at
			// once and do not show the list/button
			if (linksFromHere.size() == 1) {
				EntryLink solitaire = linksFromHere.get(0);
				Label toReal = new Label(paramBar, SWT.SINGLE);
				GridData gd3a = new GridData(SWT.LEFT, SWT.CENTER, true, false);
				toReal.setLayoutData(gd3a);
				toReal.setText(clp.getText(solitaire));
				if (solitaire.isUndecided()) { // only add link if it had'nt
												// been added before yet
					chain.addLink(solitaire);
					checkLinksComplete();
				}
			} else {
				final ComboViewer cv = new ComboViewer(paramBar, SWT.SINGLE);
				GridData gd0 = new GridData(SWT.FILL, SWT.FILL, false, false);
				cv.getControl().setLayoutData(gd0);
				cv.setLabelProvider(clp);
				cv.setContentProvider(new ArrayContentProvider());
				// cv.setComparator(new EntryLinkFromComparator());
				cv.setInput(linksFromHere);
				cv.addPostSelectionChangedListener(new ISelectionChangedListener() {
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						// TODO Auto-generated method stub
						IStructuredSelection sel = (IStructuredSelection) cv
								.getSelection();
						if (sel == null) {
							return;
						}
						Object elem = sel.getFirstElement();
						if (elem instanceof EntryLink) {
							EntryLink selectedLink = (EntryLink) elem;
							chain.addLink(selectedLink);
							drawCEboxes();
							checkLinksComplete();
						}
					}
				});
			}
		}
	}

	public void checkLinksComplete() {
//		System.out.println("Checking Links..............."); //$NON-NLS-1$
		chainIsValid = chain.validateLinks();
		setPageComplete(chainIsValid);
	}

	@Override
	public void handleEvent(Event event) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canFlipToNextPage() {
		return chainIsValid;
	}

	public IWizardPage getNextPage() {
		ServiceConfigurationPage sc = ((WorkflowWizard) getWizard()).serviceConfiguration;
		sc.drawCEboxes();
		return sc;
	}
}
