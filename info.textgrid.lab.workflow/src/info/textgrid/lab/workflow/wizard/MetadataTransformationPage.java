package info.textgrid.lab.workflow.wizard;

import info.textgrid.lab.workflow.Chain;
import info.textgrid.lab.workflow.EntryLink;
import info.textgrid.lab.workflow.MetadataTransformer;
import info.textgrid.lab.workflow.views.MetadataTransformerDialogue;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.PlatformUI;

public class MetadataTransformationPage extends WizardPage implements Listener {
	Chain chain;
	private ArrayList<EntryLink> outputs;
	private ArrayList<EntryLink> inputs;
	private Composite composite;
	private int numberOfOutputs = 0;

	public MetadataTransformationPage(String pageName) {
		super(pageName);
		setTitle(Messages.MetadataTransformationPage_pageTitle);
		setDescription(Messages.MetadataTransformationPage_pageDescription);
	}

	class mdtLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			EntryLink l;
			if (obj instanceof EntryLink) {
				l = (EntryLink) obj;
				return l.getTo().getName() + "/" + l.getToPort().getName(); //$NON-NLS-1$
			} else {
				return "unknown class: " + obj.getClass(); //$NON-NLS-1$
			}
		}
	}

	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		composite = new Composite(parent, SWT.NONE);
		// create the desired layout for this wizard page
		GridLayout gl = new GridLayout();
		int ncol = 4;
		gl.numColumns = ncol;
		composite.setLayout(gl);
		setControl(composite);

		chain = ((WorkflowWizard) getWizard()).getChain();

		refreshTransformerArea();

	}

	private void readLinks() {
		outputs = new ArrayList<EntryLink>();
		inputs = new ArrayList<EntryLink>();
		for (EntryLink l : chain.getLinks()) {
			if (l.isUndecided() || l.isDeleted()) {
				continue;
			}
			if (l.getFrom() == null && l.getFromPort() == null) {
				inputs.add(l);
			}
			if (l.getTo() == null && l.getToPort() == null) {
				outputs.add(l);
			}
		}
		numberOfOutputs = outputs.size();
	}

	public void refreshTransformerArea() {
//		System.out.println("Current Number of Transformers: " //$NON-NLS-1$
//				+ chain.getTransformers().size());
		for (Control c : composite.getChildren()) {
			c.dispose();
		}
		readLinks();
		for (EntryLink l : outputs) {
			final EntryLink outputLink = l;
			Label outputName = new Label(composite, SWT.SINGLE);
			outputName.setText(l.getFrom().getName() + " / " //$NON-NLS-1$
					+ l.getFromPort().getName());

			Label fromLabel = new Label(composite, SWT.SINGLE);
			fromLabel.setText(Messages.MetadataTransformationPage_GenerateFromLabel);

			MetadataTransformer myMDT = null;
			for (MetadataTransformer mdt : chain.getTransformers()) {
				// we can re-use an existing Transformer if there has been set
				// one for this output and the input it referred to is still
				// existing
				if (mdt.getOutput().equals(l)
						&& inputs.contains(mdt.getFromInput())) {
					myMDT = mdt;
					//System.out.println("re-using a previously used one"); //$NON-NLS-1$
				}
			}
			if (myMDT == null) {
				myMDT = new MetadataTransformer();
				myMDT.setOutput(l);
				myMDT.setDefaultParams();
				chain.getTransformers().add(myMDT);
			}
			final int myMDTindex = chain.getTransformers().indexOf(myMDT);

			final ComboViewer cv = new ComboViewer(composite, SWT.SINGLE);
			cv.setLabelProvider(new mdtLabelProvider());
			cv.setContentProvider(new ArrayContentProvider());
			cv.setInput(inputs);
			cv.addPostSelectionChangedListener(new ISelectionChangedListener() {
				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					IStructuredSelection spss = (IStructuredSelection) cv
							.getSelection();
					EntryLink inputLink = null;
					if (spss.getFirstElement() instanceof EntryLink) {
						inputLink = (EntryLink) spss.getFirstElement();
						if (inputLink != null) {
							for (MetadataTransformer mdt : chain
									.getTransformers()) {
								if (mdt.getOutput().equals(outputLink)) {
									mdt.setFromInput(inputLink);
//									System.out.println("Metadata for " //$NON-NLS-1$
//											+ outputLink.getFromPort()
//													.getName()
//											+ " will be generated from " //$NON-NLS-1$
//											+ inputLink.getToPort().getName());
									setPageComplete(checkPageComplete());
								}
							}
						}
					}
				}
			});

			// preselect a previously read input still existing in the chain
			// (otherwise would have been deleted if services were reordered)
			if (inputs.contains(myMDT.getFromInput())) {
				cv.setSelection(new StructuredSelection(myMDT.getFromInput()),
						true);
			} else if (inputs.size() == 1) { // set default MDT if only one
												// input (only for new chains)
				cv.setSelection(new StructuredSelection(inputs.get(0)), true);
				myMDT.setFromInput(inputs.get(0));
//				System.out
//						.println("Metadata Transformation trivial, only one input."); //$NON-NLS-1$
				setPageComplete(checkPageComplete());
			}

			Button configureButton = new Button(composite, SWT.PUSH);
			configureButton.setText(Messages.MetadataTransformationPage_ConfigureButton);
			configureButton.addSelectionListener(new SelectionListener() {
				public void widgetSelected(SelectionEvent event) {

					MetadataTransformerDialogue mdtd = new MetadataTransformerDialogue(
							PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getShell());
					mdtd.setContents(chain.getTransformers().get(myMDTindex));
					mdtd.setBlockOnOpen(true);
					mdtd.open();
				}

				public void widgetDefaultSelected(SelectionEvent e) {
					widgetSelected(e);
				}
			});
		}
		composite.layout(true, true);
	}

	@Override
	public void handleEvent(Event event) {
		// TODO Auto-generated method stub

	}

	public boolean checkPageComplete() {
		int outputsLeft = numberOfOutputs;
		for (MetadataTransformer mdt : chain.getTransformers()) {
			if (mdt.getFromInput() != null) {
				outputsLeft--;
			}
		}
		boolean complete = outputsLeft == 0;
		return complete;
	}

	@Override
	public boolean canFlipToNextPage() {
		return checkPageComplete();
	}

	public boolean isPageComplete() {
		return true;
	}
	public IWizardPage getNextPage() {
		FinalPage fp = ((WorkflowWizard) getWizard()).finalPage;
		fp.drawQuestions();
		return fp;
	}
}
