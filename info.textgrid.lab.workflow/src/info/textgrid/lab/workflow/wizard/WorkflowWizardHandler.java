package info.textgrid.lab.workflow.wizard;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.handlers.HandlerUtil;

public class WorkflowWizardHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		WorkflowWizard wizard = new WorkflowWizard();

		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if ((selection instanceof IStructuredSelection) || (selection == null))
			wizard.init(HandlerUtil.getActivePart(event).getSite()
					.getWorkbenchWindow().getWorkbench(),
					(IStructuredSelection) selection);

		WizardDialog dialog = new WizardDialog(
				HandlerUtil.getActiveShell(event), wizard);
		dialog.setMinimumPageSize(800, 400);
		dialog.setTitle(Messages.WorkflowWizardHandler_DialogTitle);
		dialog.create();
		dialog.open();

		return null;
	}

}
