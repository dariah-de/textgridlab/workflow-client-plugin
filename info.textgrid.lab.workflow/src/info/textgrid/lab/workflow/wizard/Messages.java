package info.textgrid.lab.workflow.wizard;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.workflow.wizard.messages"; //$NON-NLS-1$
	public static String FinalPage_andForL10WFDescription;
	public static String FinalPage_CreateRadioButton;
	public static String FinalPage_PageDescription;
	public static String FinalPage_pageTitle;
	public static String FinalPage_SaveLabel;
	public static String FinalPage_UpdateRadioBUtton;
	public static String FinalPage_WFDescriptionLabel;
	public static String LinkInsertionPage_ClearLinksButtonText;
	public static String LinkInsertionPage_FromLabel;
	public static String LinkInsertionPage_InputLabel;
	public static String LinkInsertionPage_LinkFromWorkflowInputs;
	public static String LinkInsertionPage_LinkToWorkflowOutputs;
	public static String LinkInsertionPage_OutputLabel;
	public static String LinkInsertionPage_pagedescription;
	public static String LinkInsertionPage_pagetitle;
	public static String LinkInsertionPage_ToLabel;
	public static String MetadataTransformationPage_ConfigureButton;
	public static String MetadataTransformationPage_GenerateFromLabel;
	public static String MetadataTransformationPage_pageDescription;
	public static String MetadataTransformationPage_pageTitle;
	public static String ServiceConfigurationPage_addmenu;
	public static String ServiceConfigurationPage_AddMenu;
	public static String ServiceConfigurationPage_deletemenu;
	public static String ServiceConfigurationPage_editmenu;
	public static String ServiceConfigurationPage_noValuesSetLabel;
	public static String ServiceConfigurationPage_pagedescription;
	public static String ServiceConfigurationPage_pagetitle;
	public static String ServiceConfigurationPage_serviceHasNoParamsLabel;
	public static String ServiceOrderingPage_DuplicateButton;
	public static String ServiceOrderingPage_DuplicateButtonDescription;
	public static String ServiceOrderingPage_MoveDownButton;
	public static String ServiceOrderingPage_MoveDownButtonDescription;
	public static String ServiceOrderingPage_MoveUpButton;
	public static String ServiceOrderingPage_MoveUpButtonTip;
	public static String ServiceOrderingPage_pagedescription;
	public static String ServiceOrderingPage_pagetitle;
	public static String ServiceOrderingPage_RemoveButton;
	public static String ServiceOrderingPage_RemoveButtonDescription;
	public static String ServiceSelectionPage_AllTabTitle;
	public static String ServiceSelectionPage_ApprovedTabTitle;
	public static String ServiceSelectionPage_KeepServicesButtonDescription;
	public static String ServiceSelectionPage_KeepServicesButtonText;
	public static String ServiceSelectionPage_NoValidSDWarning;
	public static String ServiceSelectionPage_PageDescription;
	public static String ServiceSelectionPage_pageTitle;
	public static String WorkflowWizard_NoTargetProjectMessage;
	public static String WorkflowWizardHandler_DialogTitle;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
