package info.textgrid.lab.workflow.wizard;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.ProjectDoesNotExistException;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider.IDoneListener;
import info.textgrid.lab.core.swtutils.PendingLabelProvider;
import info.textgrid.lab.ui.core.utils.UpdatingDeferredListContentProvider;
import info.textgrid.lab.workflow.ChainEntry;

import java.rmi.RemoteException;

import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

public class FinalPage extends WizardPage implements Listener {

	WorkflowWizard wizard;
	private Boolean updateOriginalTGO = null;
	private Text nameText;
	private ComboViewer saveProjectViewer;
	private final UpdatingDeferredListContentProvider sPcontentProvider = new UpdatingDeferredListContentProvider();
	Composite composite;
	private Text descriptionText;
	private String descriptionTextString;
	private TextGridProject selectedProject;
	private String nameTextString;

	public FinalPage(String pageName) {
		super(pageName);
		setTitle(Messages.FinalPage_pageTitle);
		setDescription(Messages.FinalPage_PageDescription);
	}

	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		composite = new Composite(parent, SWT.NONE);
		// create the desired layout for this wizard page
		GridLayout gl = new GridLayout();
		int ncol = 1;
		gl.numColumns = ncol;
		composite.setLayout(gl);
		setControl(composite);

		wizard = (WorkflowWizard) getWizard();

		// draw dynamically upon leaving pre-last page such that initial login
		// question will be delayed
		// drawQuestions();
	}

	public void drawQuestions() {
		for (Control c : composite.getChildren()) {
			c.dispose();
		}
		Composite descriptionComposite = new Composite(composite, SWT.NONE);
		GridLayout gld = new GridLayout();
		gld.numColumns = 1;
		descriptionComposite.setLayout(gld);

		Label textualDescriptionLabel = new Label(descriptionComposite,
				SWT.NONE);
		textualDescriptionLabel.setText(Messages.FinalPage_WFDescriptionLabel);

		// fill in dummy space to line up nicely with the other indents below
		Composite descriptionComposite2 = new Composite(composite, SWT.NONE);
		GridLayout gld2 = new GridLayout();
		gld2.numColumns = 2;
		descriptionComposite2.setLayout(gld2);

		Label dummy3 = new Label(descriptionComposite2, SWT.SINGLE
				| SWT.READ_ONLY);
		dummy3.setText("       "); //$NON-NLS-1$

		descriptionText = new Text(descriptionComposite2, SWT.MULTI
				| SWT.BORDER);
		GridData dtgd = new GridData(SWT.FILL, SWT.CENTER, true, true);
		dtgd.widthHint = 2000;
		dtgd.heightHint = 75;
		descriptionText.setLayoutData(dtgd);
		descriptionText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				descriptionTextString = descriptionText.getText();
			}
		});

		if (descriptionTextString != null) {
			descriptionText.setText(descriptionTextString);
		} else if (wizard.getChain().getDescription().length() > 0) {
			descriptionText.setText(wizard.getChain().getDescription());
		} else {
			StringBuffer automaticDescription = new StringBuffer();
			for (ChainEntry ce : wizard.getChain().getChain()) {
				automaticDescription.append(ce.getName()).append(Messages.FinalPage_andForL10WFDescription);
			}
			descriptionText.setText(automaticDescription.substring(0,
					automaticDescription.length() - 5));
		}

		if (wizard.getOriginalTGO() == null) {
			Label l = new Label(composite, SWT.SINGLE);
			l.setText(Messages.FinalPage_SaveLabel);

			Composite dummyComposite3 = new Composite(composite, SWT.NONE);
			GridLayout gldu3 = new GridLayout();
			gldu3.numColumns = 2;
			dummyComposite3.setLayout(gldu3);

			Label dummy4 = new Label(dummyComposite3, SWT.SINGLE
					| SWT.READ_ONLY);
			dummy4.setText("       "); //$NON-NLS-1$

			Composite newMetadataComposite4 = new Composite(dummyComposite3,
					SWT.NONE);
			GridLayout gl4 = new GridLayout();
			gl4.numColumns = 1;
			newMetadataComposite4.setLayout(gl4);

			drawNameProject(newMetadataComposite4, "", ""); //$NON-NLS-1$ //$NON-NLS-2$
			updateOriginalTGO = false;
		} else {
			Button keepOriginalMetadata = new Button(composite, SWT.RADIO);
			keepOriginalMetadata
					.setText(Messages.FinalPage_UpdateRadioBUtton);
			keepOriginalMetadata.addSelectionListener(new SelectionListener() {
				public void widgetSelected(SelectionEvent e) {
					enableNewMetadata(false);
					wizard.getContainer().updateButtons();
				}

				public void widgetDefaultSelected(SelectionEvent e) {
					widgetSelected(e);
				}
			});

			Composite dummyComposite2 = new Composite(composite, SWT.NONE);
			GridLayout gldu2 = new GridLayout();
			gldu2.numColumns = 2;
			dummyComposite2.setLayout(gldu2);

			Label dummy2 = new Label(dummyComposite2, SWT.SINGLE
					| SWT.READ_ONLY);
			dummy2.setText("       "); //$NON-NLS-1$

			Composite newMetadataComposite2 = new Composite(dummyComposite2,
					SWT.NONE);
			GridLayout gl2 = new GridLayout();
			gl2.numColumns = 1;
			newMetadataComposite2.setLayout(gl2);

			Text readOnlyNameText = new Text(newMetadataComposite2, SWT.SINGLE
					| SWT.READ_ONLY);
			readOnlyNameText.setText(wizard.getOriginalTitle());
			Text readOnlyProjectText = new Text(newMetadataComposite2,
					SWT.SINGLE | SWT.READ_ONLY);
			try {
				readOnlyProjectText.setText(TextGridProject.getProjectInstance(
						wizard.getOriginalProjectId()).getName());
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (CrudServiceException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ProjectDoesNotExistException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			Button saveWithNewMetadata = new Button(composite, SWT.RADIO);
			saveWithNewMetadata
					.setText(Messages.FinalPage_CreateRadioButton);
			saveWithNewMetadata.addSelectionListener(new SelectionListener() {
				public void widgetSelected(SelectionEvent e) {
					enableNewMetadata(true);
					wizard.getContainer().updateButtons();
				}

				public void widgetDefaultSelected(SelectionEvent e) {
					widgetSelected(e);
				}
			});

			Composite dummyComposite = new Composite(composite, SWT.NONE);
			GridLayout gldu = new GridLayout();
			gldu.numColumns = 2;
			dummyComposite.setLayout(gldu);

			Label dummy = new Label(dummyComposite, SWT.SINGLE | SWT.READ_ONLY);
			dummy.setText("       "); //$NON-NLS-1$

			Composite newMetadataComposite = new Composite(dummyComposite,
					SWT.NONE);
			GridLayout gl = new GridLayout();
			gl.numColumns = 1;
			newMetadataComposite.setLayout(gl);

			drawNameProject(newMetadataComposite, wizard.getOriginalTitle(),
					wizard.getOriginalProjectId());

			enableNewMetadata(updateOriginalTGO == null ? false
					: !updateOriginalTGO);
			if (updateOriginalTGO) {
				keepOriginalMetadata.setSelection(true);
			} else {
				saveWithNewMetadata.setSelection(true);
			}

		}
		composite.layout(true, true);
	}

	public Text getDescriptionText() {
		return descriptionText;
	}

	public void enableNewMetadata(boolean enabled) {
		nameText.setEnabled(enabled);
		saveProjectViewer.getControl().setEnabled(enabled);
		composite.layout();
		updateOriginalTGO = !enabled;
	}

	public void drawNameProject(Composite comp, String name,
			final String project) {

		nameText = new Text(comp, SWT.SINGLE | SWT.BORDER);
		if (nameTextString != null) {
			nameText.setText(nameTextString);
		} else {
			nameText.setText(name == null ? "" : name); //$NON-NLS-1$
		}
		GridData gd6t = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd6t.widthHint = 2000;
		nameText.setLayoutData(gd6t);
		nameText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				nameTextString = nameText.getText();
				wizard.getContainer().updateButtons();
			}
		});

		saveProjectViewer = new ComboViewer(comp, SWT.SINGLE);
		GridData gd6b = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		gd6b.widthHint = 250;
		saveProjectViewer.getControl().setLayoutData(gd6b);

		saveProjectViewer.setContentProvider(sPcontentProvider);
		saveProjectViewer.setLabelProvider(new PendingLabelProvider());
		saveProjectViewer.setInput(TextGridProjectRoot
				.getInstance(TextGridProjectRoot.LEVELS.EDITOR));
		saveProjectViewer
				.addSelectionChangedListener(new ISelectionChangedListener() {
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						// TODO Auto-generated method stub
						selectedProject = (TextGridProject) ((IStructuredSelection) saveProjectViewer
								.getSelection()).getFirstElement();
						wizard.getContainer().updateButtons();
					}
				});
		IDoneListener doneListener = new IDoneListener() {

			@Override
			public void loadDone(Viewer viewer) {
				// TODO Auto-generated method stub
				try {
					if (selectedProject != null) {
						saveProjectViewer.setSelection(new StructuredSelection(
								selectedProject));
					} else if (project != "") { //$NON-NLS-1$
						saveProjectViewer.setSelection(new StructuredSelection(
								TextGridProject.getProjectInstance(project)),
								true);
					}
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (CrudServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ProjectDoesNotExistException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		};
		sPcontentProvider.addDoneListener(doneListener);
	}

	public String getDescriptionTextString() {
		return descriptionTextString;
	}

	public TextGridProject getSelectedProject() {
		return selectedProject;
	}

	public String getNameTextString() {
		return nameTextString;
	}

	public void setDescriptionTextString(String descriptionTextString) {
		this.descriptionTextString = descriptionTextString;
	}

	public void setSelectedProject(TextGridProject selectedProject) {
		this.selectedProject = selectedProject;
	}

	public void setNameTextString(String nameTextString) {
		this.nameTextString = nameTextString;
	}

	public Boolean getUpdateOriginalTGO() {
		return updateOriginalTGO;
	}

	@Override
	public void handleEvent(Event event) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean canFlipToNextPage() {
		return false;
	}

	public boolean isPageComplete() {
		return true;
	}

}
