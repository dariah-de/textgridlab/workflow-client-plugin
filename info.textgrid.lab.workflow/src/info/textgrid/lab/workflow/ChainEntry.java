package info.textgrid.lab.workflow;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.workflow.servicedescription.Configparameter;
import info.textgrid.lab.workflow.servicedescription.Examplevalue;
import info.textgrid.lab.workflow.servicedescription.Input;
import info.textgrid.lab.workflow.servicedescription.Output;
import info.textgrid.lab.workflow.servicedescription.Service;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXB;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.w3c.dom.Element;

public class ChainEntry {
	private Service service;
	private HashMap<String, ArrayList<Examplevalue>> selectedValues; // Param ->
																		// ArrayList<ExampleValueId>
	private HashMap<String, Integer> numberOfInstances;
	private URI myURI;
	private int myID;
	private HashMap<Input, IncomingLink> incomingLinks;
	private HashMap<Output, OutgoingLink> outgoingLinks;

	public static final String NEW_FROM_STRING_ID = "new_from_String";
	public static final String NEW_FROM_URI_ID = "new_from_URI";
	private static final String NEW_FROM_STRING_DISPLAY = "New from String...";
	private static final String NEW_FROM_URI_DISPLAY = "New from TextGrid URI...";

	public ChainEntry(TextGridObject serviceTGO) {

		myURI = serviceTGO.getURI();

		IFile sf = (IFile) serviceTGO.getAdapter(IFile.class);
		try {
			service = JAXB.unmarshal(sf.getContents(true), Service.class);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			try {
				Activator.handleProblem(IStatus.ERROR, e, "Object {0} is no valid service description", serviceTGO.getTitle());
			} catch (CoreException e1) {
			}
			return;
		}
		selectedValues = new HashMap<String, ArrayList<Examplevalue>>();
		numberOfInstances = new HashMap<String, Integer>();
		List<Configparameter> cpList = service.getConfigparameters()
				.getConfigparameter();

		// add dummy entries for users wishing to create their new value from
		// scratch
		for (Configparameter cp : cpList) {
			numberOfInstances.put(cp.getParam(), 1);
			Examplevalue ex1 = new Examplevalue();
			ex1.setDefault(false);
			ex1.setId(NEW_FROM_STRING_ID);
			ex1.setInline(true);
			ex1.setName(NEW_FROM_STRING_DISPLAY);
			ex1.getContent().add(
					"Possible Value for " + cp.getName() + " (the Parameter '"
							+ cp.getParam() + "')");
			cp.getExamplevalue().add(ex1);

			Examplevalue ex2 = new Examplevalue();
			ex2.setDefault(false);
			ex2.setId(NEW_FROM_URI_ID);
			ex2.setInline(false);
			ex2.setName(NEW_FROM_URI_DISPLAY);
			ex2.getContent().add("THIS:IS:A:TEXTGRID:URI");
			cp.getExamplevalue().add(ex2);
		}

		clearLinks(); // initialize with empty maps
	}

	/**
	 * Set default IDs for configuration parameters. Do not get the real values
	 * yet, as this could mean separate threads to be accessed. This is called
	 * only during construction of a workflow in the GUI.
	 */
	public void setDefaultParams() {
		List<Configparameter> cpList = service.getConfigparameters()
				.getConfigparameter();
		for (Configparameter cp : cpList) {
			for (Examplevalue ex : cp.getExamplevalue()) {
				if (ex.isDefault()) {
					ArrayList<Examplevalue> val = new ArrayList<Examplevalue>();
					val.add(ex);
					selectedValues.put(cp.getParam(), val);
					break;
				}
			}
		}
	}

	public void deleteExampleValue(String param, int position) {
		for (Configparameter cp : service.getConfigparameters()
				.getConfigparameter()) {
			if (cp.getParam().equals(param)) {
				selectedValues.get(param).remove(position);
				numberOfInstances.put(param, numberOfInstances.get(param) - 1);
			}
		}
	}

	public void addDefaultExampleValue(String param) {
		for (Configparameter cp : service.getConfigparameters()
				.getConfigparameter()) {
			if (cp.getParam().equals(param)) {
				for (Examplevalue ex : cp.getExamplevalue()) {
					if (ex.isDefault()) {
						selectedValues.get(param).add(ex);
						numberOfInstances.put(param,
								numberOfInstances.get(param) + 1);
						break;
					}
				}
				break;
			}
		}
	}

	public void selectExampleValue(String param, Examplevalue selectEx,
			int position) {
		for (Configparameter cp : service.getConfigparameters()
				.getConfigparameter()) {
			if (cp.getParam().equals(param)) {
				selectedValues.get(param).set(position, selectEx);
			}
		}
	}

	public List<Examplevalue> getAllAvailableExampleValues(String param) {
		for (Configparameter cp : service.getConfigparameters()
				.getConfigparameter()) {
			if (cp.getParam().equals(param)) {
				return cp.getExamplevalue();
			}
		}
		return null;
	}

	// this is only called during reading from TGWF Workflow
	public boolean setConfigParam(String configParam, String valueID) {
		for (Configparameter cp : service.getConfigparameters()
				.getConfigparameter()) {
			if (cp.getParam().equals(configParam)) {
				if (valueID.equals(NEW_FROM_STRING_ID)
						|| valueID.equals(NEW_FROM_URI_ID)) {
//					System.out
//							.println("Error! Trying to set Non-predeclared Value using the wrong function");
					return false;

				}
				if (!selectedValues.containsKey(configParam)) {
					selectedValues.put(configParam,
							new ArrayList<Examplevalue>());
					numberOfInstances.put(configParam, 1);
				}
				for (Examplevalue ex : cp.getExamplevalue()) {
					if (valueID.equals(ex.getId())) {
						selectedValues.get(configParam).add(ex);
						numberOfInstances.put(configParam,
								numberOfInstances.get(configParam) + 1);
						return true;
					}
				}
				break;
			}
		}
		return false;
	}

	public boolean needsB64encoding(String configParam) {
		for (Configparameter cp : service.getConfigparameters()
				.getConfigparameter()) {
			if (cp.getParam().equals(configParam)) {
				return cp.isNeedsB64Encoding();
			}
		}
		return false; // or what should I do if there was no such configParam?
	}

	public ArrayList<Examplevalue> getChosenConfigParamExampleValues(
			String configParam) {
		return selectedValues.get(configParam);
	}

	public void setNewConfigValue(String param, boolean inline, int position,
			String contents) {
		for (Configparameter cp : getConfigParameters()) {
			if (cp.getParam().equals(param)) {
				Examplevalue previous = selectedValues.get(param).get(position);
				if (previous.getId().equals(NEW_FROM_STRING_ID)
						|| previous.getId().equals(NEW_FROM_URI_ID)) {
					// We already have one NEW_FROM_XXX specified
					// we only set the existing Examplevalue's contents
					previous.getContent().clear();
					previous.getContent().add(contents);
				} else {
					// the old value was one of the Examplevalues having an
					// ordinary ID. create a new NEW_FROM_XXX Value
					if (inline) {
						Examplevalue ex1 = new Examplevalue();
						ex1.setDefault(false);
						ex1.setId(NEW_FROM_STRING_ID);
						ex1.setInline(true);
						ex1.setName(NEW_FROM_STRING_DISPLAY + position);
						ex1.getContent().add(contents);
						cp.getExamplevalue().add(ex1);
						selectedValues.get(param).set(position, ex1);
					} else {
						Examplevalue ex2 = new Examplevalue();
						ex2.setDefault(false);
						ex2.setId(NEW_FROM_URI_ID);
						ex2.setInline(false);
						ex2.setName(NEW_FROM_URI_DISPLAY + position);
						ex2.getContent().add(contents);
						cp.getExamplevalue().add(ex2);
						selectedValues.get(param).set(position, ex2);
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param param
	 * @param inline
	 * @param contents
	 */
	// this is only called during reading from TGWF Workflow
	public void appendNewConfigValue(String param, boolean inline,
			String content) {
		int counter;
		if (!selectedValues.containsKey(param)) {
			selectedValues.put(param, new ArrayList<Examplevalue>());
			counter = 1;
		} else {
			counter = numberOfInstances.get(param) + 1;
		}
		numberOfInstances.put(param, counter);

		for (Configparameter cp : getConfigParameters()) {
			if (cp.getParam().equals(param)) {
				if (inline) {
					Examplevalue ex1 = new Examplevalue();
					ex1.setDefault(false);
					ex1.setId(NEW_FROM_STRING_ID);
					ex1.setInline(true);
					ex1.setName(NEW_FROM_STRING_DISPLAY + counter);
					ex1.getContent().add(content);
					cp.getExamplevalue().add(ex1);
					selectedValues.get(param).add(ex1);
				} else {
					Examplevalue ex2 = new Examplevalue();
					ex2.setDefault(false);
					ex2.setId(NEW_FROM_URI_ID);
					ex2.setInline(false);
					ex2.setName(NEW_FROM_URI_DISPLAY + counter);
					ex2.getContent().add(content);
					cp.getExamplevalue().add(ex2);
					selectedValues.get(param).add(ex2);
				}
			}
		}
	}

	public URI getTGOURI() {
		return myURI;
	}

	public void setID(int id) {
		myID = id;
	}

	public int getID() {
		return myID;
	}

	public String getName() {
		return service.getDescriptivedata().getName();
	}

	public String getOperationName() {
		return service.getTechnicaldata().getOperation();
	}

	public String getURL() {
		return service.getTechnicaldata().getDescriptionlocation().getUri();
	}

	public String toString() {
		return getName();
	}

	public List<Input> getInputs() {
		return service.getInputs().getInput();
	}

	public List<Output> getOutputs() {
		return service.getOutputs().getOutput();
	}

	public List<Configparameter> getConfigParameters() {
		return service.getConfigparameters().getConfigparameter();
	}

	public boolean existsIncomingLink(Input i) {
		return incomingLinks.containsKey(i);
	}

	public boolean existsOutgoingLink(Output o) {
		return outgoingLinks.containsKey(o);
	}

	public boolean addIncomingLink(EntryLink el, String iD) {
		if (this.equals(el.getTo())) {
			if (incomingLinks.containsKey(el.getToPort())) {
				EntryLink tmplink = new EntryLink(incomingLinks.get(
						el.getToPort()).getFrom(), incomingLinks.get(
						el.getToPort()).getFromPort(), this, el.getToPort());
				if (el.equals(tmplink)) {
					return true;
				} else {
					return false;
				}
			} else {
				incomingLinks.put(el.getToPort(), new IncomingLink(
						el.getFrom(), el.getFromPort(), iD));
				return true;
			}
		} else {
			return false;
		}
	}

	public HashMap<Input, IncomingLink> getIncomingLinks() {
		return incomingLinks;
	}

	public boolean addOutgoingLink(EntryLink el, String iD) {
		if (this.equals(el.getFrom())) {
			if (outgoingLinks.containsKey(el.getFromPort())) {
				EntryLink tmplink = new EntryLink(this, el.getFromPort(),
						outgoingLinks.get(el.getFromPort()).getTo(),
						outgoingLinks.get(el.getFromPort()).getToPort());
				if (el.equals(tmplink)) {
					return true;
				} else {
					return false;
				}
			} else {
				outgoingLinks.put(el.getFromPort(), new OutgoingLink(
						el.getTo(), el.getToPort(), iD));
				return true;
			}
		} else {
			return false;
		}
	}

	public HashMap<Output, OutgoingLink> getOutgoingLinks() {
		return outgoingLinks;
	}

	public void clearLinks() {
		incomingLinks = new HashMap<Input, IncomingLink>();
		outgoingLinks = new HashMap<Output, OutgoingLink>();
	}

	public static String anyToString(List<Object> contents) {
		// JAXB binding for xs:any allows both String and XML -- and we want
		// String for sure.
		StringBuilder sb = new StringBuilder();
		for (Iterator<Object> i = contents.iterator(); i.hasNext();) {
			Object o = i.next();
			if (o instanceof String) {
				sb.append((String) o);
			} else if (o instanceof Element) {
				StringWriter sw = new StringWriter();
				OutputFormat format = new OutputFormat();
				format.setOmitXMLDeclaration(true);
				XMLSerializer serial = new XMLSerializer(sw, format);
				try {
					serial.serialize((Element) o);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				sb.append(sw);
			} else {
//				System.out
//						.println("Warning [anyToString], dunno what to do with a "
//								+ o.getClass());
				sb.append("[" + o.getClass() + "]" + o.toString() + "\n");
			}
		}
		return sb.toString();
	}

	public String getTns() {
		if (!service.getTechnicaldata().getTargetnamespace().isUsetns()) {
			return null;
		} else {
			return service.getTechnicaldata().getTargetnamespace().getUri();
		}
	}

	public int getNumberOfParamInstances(String param) {
		return numberOfInstances.get(param);
	}

	public String getType() {
		return service.getTechnicaldata().getType();
	}

	public String getDescription() {
		return service.getDescriptivedata().getDescription();
	}

}
