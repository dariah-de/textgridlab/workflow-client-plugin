/**
 * 
 */
package info.textgrid.lab.workflow;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.workflow.views.WorkflowInputs;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * adds all selected TGOs to the list of input documents held in
 * WORKFLOW_PREP_VIEW_ID
 * 
 * @author martin, based on tv's CopyURIHandler
 */
public class AddtoWorkflowHandler extends AbstractHandler implements IHandler {
	public static final String WORKFLOW_INPUTS_VIEW_ID = "info.textgrid.lab.workflow.views.WorkflowInputs";
	public static final String WORKFLOW_PERSPECTIVE_ID = "info.textgrid.lab.workflow.WorkflowPerspective";

	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection sel = HandlerUtil.getCurrentSelectionChecked(event);

		if (sel instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) sel;
			Iterator<?> iterator = selection.iterator();
			ArrayList<TextGridObject> tgos = new ArrayList<TextGridObject>();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof IAdaptable) {
					TextGridObject object = (TextGridObject) ((IAdaptable) next)
							.getAdapter(TextGridObject.class);
					if (object == null)
						continue;
					tgos.add(object);
				}

			}
			try {
				PlatformUI.getWorkbench().showPerspective(
						WORKFLOW_PERSPECTIVE_ID,
						PlatformUI.getWorkbench().getActiveWorkbenchWindow());
			} catch (WorkbenchException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						"Could not open workflow perspective", e);
				Activator.getDefault().getLog().log(status);
			}

			IViewPart wfView = null;
			// try {
			// only findView as we open the perspective in a moment.
			// should be showView (with the catch) if we decide not to.
			wfView = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage().findView(WORKFLOW_INPUTS_VIEW_ID);
			// } catch (PartInitException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			if (wfView != null && wfView instanceof WorkflowInputs) {
				WorkflowInputs wiInstance = (WorkflowInputs) wfView;
				wiInstance.addtoInputs(tgos, 0); // TODO is that reasonable? Or
													// rather disable the
													// handler, not knowing how
													// many input boxes will be
													// there?
			}

		}

		return null;
		// return text.toString();
	}
}
